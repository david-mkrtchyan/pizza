from django.contrib import admin
from .models import Category
from suit.admin import SortableModelAdmin
from mptt.admin import MPTTModelAdmin


@admin.register(Category)
class CategoryAdmin(MPTTModelAdmin, SortableModelAdmin):
    list_filter = ('name_en', 'name_hy', 'name_ru', 'slug', 'is_active',)
    # radio_fields = {"category": admin.VERTICAL}
    search_fields = ['name_en', 'name_hy', 'name_ru']

    mptt_level_indent = 20
    list_display = ('name_en', 'name_hy', 'name_ru', 'slug', 'is_active')
    list_editable = ('is_active',)

    # Specify name of sortable property
    sortable = 'order'
