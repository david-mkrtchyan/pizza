from django.shortcuts import render, HttpResponse
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from bs4 import BeautifulSoup
from .models import Category
from blog.functions import uploadImageFromUrl
import os
import requests as httprequest
from post.models import Post
from django.contrib.auth.models import User
from decimal import Decimal
from blog.models import Images
from django.utils.translation import gettext as _
from blog.models import District


def categories(request):
    categories = Category.objects.filter(is_active=True).all()
    return render(request, 'category/index.html', {
        'categories': categories,
        'title': _('Կատեգորիաներ'),
        'breadcrumb': _('Կատեգորիաներ'),
    })


@login_required
def insert_districts(requests):
    return HttpResponse('Already inserted')
    lists = [
        {'en': 'Ajapnyak', 'hy': 'Աջափնյակ', 'ru': 'Ачапняк'},
        {'en': 'Arabkir', 'hy': 'Արաբկիր', 'ru': 'Арабкир'},
        {'en': 'Avan', 'hy': 'Ավան', 'ru': 'Аван'},
        {'en': 'Davtashen', 'hy': 'Դավթաշեն', 'ru': 'Давташен'},
        {'en': 'Erebuni', 'hy': 'Էրեբունի', 'ru': 'Эребуни'},
        {'en': 'Kanaker-Zeytun', 'hy': 'Քանաքեռ-Զեյթուն', 'ru': 'Канакер-Зейтун'},
        {'en': 'Kentron', 'hy': 'Կենտրոն', 'ru': 'Центр'},
        {'en': 'Malatia-Sebastia', 'hy': 'Մալաթիա-Սեբաստիա', 'ru': 'Малатия-Себастия'},
        {'en': 'Nork-Marash', 'hy': 'Նորք-Մարաշ', 'ru': 'Норк-Мараш'},
        {'en': 'Nor Nork', 'hy': 'Նոր Նորք', 'ru': 'Нор Норк'},
        {'en': 'Nubarashen', 'hy': 'Նուբարաշեն', 'ru': 'Нубарашен'},
        {'en': 'Shengavit', 'hy': 'Շենգավիթ', 'ru': 'Шенгавит'},
    ]

    for key, value in enumerate(lists):
        district = District(name_en=value['en'], name_ru=value['ru'], name_hy=value['hy'])
        district.save()


@login_required
def parse_category_from_url(requests):
    return HttpResponse('Already downloaded')
    en = True
    ru = False
    hy = False
    site_url = 'https://www.karas.am'
    source = httprequest.get('https://www.karas.am/hy/').text
    soup = BeautifulSoup(source, 'lxml')
    # print(soup.prettify())
    menu = soup.find_all('div', class_='app-singleMenu')
    for key, value in enumerate(menu):
        try:
            name = value.a.span.text
            slug = name.replace(" ", "-").lower()
            img = value.a.img['src']
            url = value.a['href']
            path = site_url + img
            directory = f'media/category/'
            if en == True:
                category = Category.objects.filter(name_en=name).first()
                if category == None:
                    path = uploadImageFromUrl(path, directory)
                    path = os.path.join('/', path)
                    category = Category(name_en=name, slug=slug, image=path, is_active=True, order=key)
                    category.save()
                productText = httprequest.get(site_url + url).text
                soupPro = BeautifulSoup(productText, 'lxml')
                products = soupPro.find_all('div', class_='categories-menu')
                for key, value in enumerate(products):
                    hrefPro = value.a['href']
                    slug = hrefPro[12:]
                    productViewText = httprequest.get(site_url + hrefPro).text
                    soupViewPro = BeautifulSoup(productViewText, 'lxml')
                    productsView = soupViewPro.find('div', class_='product-body')
                    imgView = productsView.a.img['src']
                    titleView = productsView.find('p', class_='item-title').text
                    descView = productsView.find('p', class_='item-desc').text
                    price = productsView.find('span', class_='price1').text
                    post = Post.objects.filter(title_en=titleView).first()
                    if post == None and category and titleView and slug and price:
                        order = int(category.id) + int(key)
                        post = Post.objects.create(order=order, price=Decimal(price), category=category,
                                                   user=requests.user, title_en=str(titleView),
                                                   content_en=str(descView), slug=slug, status=True)
                        path = uploadImageFromUrl(site_url + imgView, directory)
                        path = os.path.join('/', path)
                        Images.objects.create(path=path, object_id=post.id, type='post')
            else:
                category = Category.objects.filter(order=key).first()
                if ru == True:
                    category.name_ru = name
                else:
                    category.name_hy = name
                category.save()

                productText = httprequest.get(site_url + url).text
                soupPro = BeautifulSoup(productText, 'lxml')
                products = soupPro.find_all('div', class_='categories-menu')
                for key, value in enumerate(products):
                    hrefPro = value.a['href']
                    slug = hrefPro[12:]
                    productViewText = httprequest.get(site_url + hrefPro).text
                    soupViewPro = BeautifulSoup(productViewText, 'lxml')
                    productsView = soupViewPro.find('div', class_='product-body')
                    imgView = productsView.a.img['src']
                    titleView = productsView.find('p', class_='item-title').text
                    descView = productsView.find('p', class_='item-desc').text
                    order = int(category.id) + int(key)
                    post = Post.objects.filter(slug=slug).first()
                    if ru == True:
                        if post and titleView:
                            post.title_ru = titleView
                            post.content_ru = descView
                            post.save()
                    else:
                        if post and titleView:
                            post.title_hy = titleView
                            post.content_hy = descView
                            post.save()
        except Exception   as e:
            raise
    return HttpResponse(222)
# print(parse_from_html())
