from django.db import models
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel
from django.utils.translation import to_locale, get_language

class Category(MPTTModel,models.Model):
    name_en = models.CharField(max_length=200,null=True)
    name_ru = models.CharField(max_length=200,null=True)
    name_hy = models.CharField(max_length=200,null=True)
    slug = models.SlugField(unique=True)
    parent = TreeForeignKey('self', null=True, blank=True,on_delete=True ,
                            related_name='children')
    is_active = models.BooleanField(default=True)
    image = models.ImageField(max_length=255, null=True,upload_to='category', blank=True)
    order = models.PositiveIntegerField(null=True)
    lft = models.PositiveIntegerField(editable=False, db_index=True,null=True)
    rght = models.PositiveIntegerField(editable=False, db_index=True,null=True)
    tree_id = models.PositiveIntegerField(editable=False, db_index=True,null=True)
    level = models.PositiveIntegerField(editable=False, db_index=True,null=True)

    class MPTTMeta:
         order_insertion_by = ['order']

    def save(self, *args, **kwargs):
        super(Category, self).save(*args, **kwargs)
        Category.objects.rebuild()

    class Meta:
        unique_together = ('slug', 'parent',)    #enforcing that there can not be two
        verbose_name_plural = "categories"       #categories under a parent with same 
        db_table = "categoryies"                         

    def __unicode__(self):
        return self.name

    def name(self):
        lang  = to_locale(get_language())
        return getattr(self, 'name_%s'%lang)

    def __str__(self):
        lang  = to_locale(get_language())
        self.name = getattr(self, 'name_%s'%lang)
        return self.name

