from django.urls import path
from . import views

urlpatterns = [
    #path('insert-districts', views.insert_districts, name='insert-districts'),
    #path('category-parse', views.parse_category_from_url, name='category-parse'),
    path('categories', views.categories, name='categories'),
]
