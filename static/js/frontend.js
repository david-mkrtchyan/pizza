$(document).ready(function () {

    new Vue({
        el: '#language-list',
        methods: {
            set_lang: function (e) {
                e.preventDefault()
                let name = event.target.getAttribute('data-name')
                let url = '/i18n/setlang/'
                let csrftoken = $('input[name="csrfmiddlewaretoken"]').val();

                const options = {
                    method: 'post',
                    params: {
                        language: name,
                        csrfmiddlewaretoken: csrftoken,
                    },
                    responseType: 'json',
                };

                $.post(url, {language: name, csrfmiddlewaretoken: csrftoken}, function (data) {
                    location.reload();
                });

            }
        }
    });

    removeProduct()
    mouseleave_cart()

    function removeProduct() {
        $('.trash-remove-product').click(function (e) {
            e.preventDefault();
            var id = $(this).data('id')
            app.removeFromCart(id)
        })
    }

    function mouseleave_cart() {
        $('.cart-area').mouseover(function () {
            $('.cart-data').find('ul').css({'opacity': 1, 'visibility': 'visible'})
        })
            .mouseout(function () {
                $('.cart-data').find('ul').css({'opacity': 0, 'visibility': 'hidden'})
            });
    }

    const app = new Vue({
        el: '#add_cart',
        data: {
            errors: [],
            quantity: 1,
        },
        methods: {
            addCartForm: function (e) {
                e.preventDefault();

                var productId = document.getElementById('productId').value;

                if (!this.quantity || !productId) {
                    return false;
                }

                let url = '/add-to-cart'

                this.sendAjax(url, productId)
            },
            removeFromCart: function (id) {
                let url = '/remove-product'
                this.sendAjax(url, id, 'del')
            },
            sendAjax: function (url, productId, action = 'add') {
                let csrftoken = $('input[name="csrfmiddlewaretoken"]').val();

                $('.alertify-notifier.ajs-bottom').html('');

                $.post(url, {
                    quantity: this.quantity,
                    productId: productId,
                    csrfmiddlewaretoken: csrftoken,
                }, function (data) {
                    if (data.success == true) {
                        var cartData = $('.cart-data')
                        if (cartData.length) {
                            cartData.html(data.data)

                            if (action == 'del') {
                                mouseleave_cart()
                                cartData.find('.cart-area').trigger('mouseenter')
                                //cartData.find('ul').css({'opacity': 1, 'visibility': 'visible'})
                            }

                            removeProduct()
                        }

                        // if(e.responseText){
                        //     var jsonData = JSON.parse(e.responseText);
                        //     $.each(jsonData.errors, function(index, items){
                        //         $.each(items, function(i, item){
                        //             alertifyError(item)
                        //         });
                        //     });
                        // }

                        if (data.message) {
                            alertifySuccess(data.message)
                        }
                    }
                });
            },
            calculate: function () {
                var obj = []
                let csrftoken = $('input[name="csrfmiddlewaretoken"]').val();

                $('#quantity-holder').find('tr').each(function () {
                    var quantityEach = $(this).find('.quantity').find('.quantity-input')
                    var q = parseInt(quantityEach.val(), 10);
                    var p = parseInt(quantityEach.data('productid'), 10);
                    var data = {'id': p, 'quantity': q};
                    obj.push(data)
                });

                if (obj) {
                    $.post('/update-cart', {
                        'data': JSON.stringify(obj),
                        'csrfmiddlewaretoken': csrftoken,
                    }, function (data) {
                        if (data.success == true) {
                            var cartData = $('.cart-data')
                            if (data.message) {
                                alertifySuccess(data.message)
                            }
                        }
                    });
                }
            },
        }
    });

    $('#quantity-holder').on('change', '.quantity-input', function () {
        var $holder = $(this).parents('.quantity-holder');
        var $target = $holder.find('input.quantity-input');
        var $quantity = parseInt($target.val(), 10);

        if ($.isNumeric($quantity) && $quantity > 0) {
            calucalatPlusMinus($target, $holder, $quantity)
        } else if ($quantity == 0) {
            alertifyError(gettext('Number must be greater than 0'))
        } else {
            alertifyError(gettext('You must use number only'))
        }
    })

    $('#quantity-holder').on('click', '.quantity-plus', function () {
        var $holder = $(this).parents('.quantity-holder');
        var $target = $holder.find('input.quantity-input');
        var $quantity = parseInt($target.val(), 10);

        if ($.isNumeric($quantity) && $quantity > 0) {
            $quantity = $quantity + 1;
            $target.val($quantity);
        } else {
            $target.val($quantity);
        }

        calucalatPlusMinus($target, $holder, $quantity)

    }).on('click', '.quantity-minus', function () {
        var $holder = $(this).parents('.quantity-holder');
        var $target = $holder.find('input.quantity-input');
        var $quantity = parseInt($target.val(), 10);
        if ($.isNumeric($quantity) && $quantity >= 2) {
            $quantity = $quantity - 1;
            $target.val($quantity);
        } else {
            $target.val(1);
        }

        calucalatPlusMinus($target, $holder, $quantity)
    });

    function calucalatPlusMinus($target, $holder, $quantity) {
        var $price = parseInt($target.data('price'), 10);
        var $amount = $holder.closest('tr').find('.amount span');
        $amount.html($quantity * $price)

        var resultTotalQuantity = 0
        var resultTotal = 0
        $('#quantity-holder').find('tr').each(function () {
            var quantityEach = $(this).find('.quantity').find('.quantity-input')
            var q = parseInt(quantityEach.val(), 10);
            var p = parseInt(quantityEach.data('price'), 10);
            resultTotalQuantity += q
            resultTotal += q * p
        })
        var totaldatacontent = $('#total-data-content');
        totaldatacontent.find('.totaldata-quantity').html(resultTotalQuantity)
        totaldatacontent.find('.totaldata-price').html(resultTotal)

    }


    function alertifySuccess(message, modal = null, delay = 10) {
        var msg = alertify.success('Default message');
        msg.delay(10).setContent(message);

        if (modal) {
            $('#' + modal).modal('hide');
        }
    }

    function alertifyError(message, modal = null, delay = 10) {
        var msg = alertify.warning('Default message');
        msg.delay(10).setContent(message);

        if (modal) {
            $('#' + modal).modal('hide');
        }
    }


});


function alertifyTogether(message, className, delay = 20) {
    if (className == 'success' || className == 'info') {
        var msg = alertify.success('Default message');
        msg.delay(10).setContent(message);
    } else {
        var msg = alertify.warning('Default message');
        msg.delay(10).setContent(message);
    }

}