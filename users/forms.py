from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile, Billing
from django.utils.translation import gettext as _
from phonenumber_field.modelfields import PhoneNumberField
from blog.models import District
from django.forms import widgets


class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


class UserUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username']


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['image']


class BillingUpdateForm(forms.ModelForm):
    email = forms.EmailField(label=_('Email'))
    phone = PhoneNumberField(help_text='Contact phone number')
    first_name = forms.CharField(label=_('First Name'), max_length=255)
    last_name = forms.CharField(label=_('Last Name'), max_length=255)
    district = forms.ChoiceField(choices=[(doc.id, doc.name) for doc in District.objects.order_by('-order').all()])

    # district = forms.Select(label=_('District'), max_length=255)
    address = forms.CharField(label=_('Address'), max_length=255)

    class Meta:
        model = Billing
        fields = ['first_name', 'last_name', 'email', 'phone', 'district', 'address']
