from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm, BillingUpdateForm
from django.utils.translation import gettext as _
from django.contrib.auth import login
from django.http import HttpResponseRedirect
from django.contrib.auth import views as auth_views
from django.views.generic import CreateView
from django.contrib.auth.models import User
from .models import Billing

class Login(auth_views.LoginView):

    def form_valid(self, form):
        login(self.request, form.get_user())
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'], = str(_('Sign In')),
        context['breadcrumb'], = str(_('Sign In')),

        return context


def register(request):

    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, _('Your account has been created! now you are able to log in'))
            return redirect('login')
    else:
        form = UserRegisterForm()

    return render(request, 'users/register.html', {
        'form': form,
        'title': _('Sign Up'),
        'breadcrumb': _('Sign Up'),
    })


@login_required
def profile(request):

    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)
        b_form = BillingUpdateForm(request.POST, instance=request.user.billing)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, _('Profile info updated successfully'))
            return redirect('profile')

        if b_form.is_valid():
            # if hasattr(request.user, 'billing'):
            #     b_form.save()
            # else:
            #     billing = b_form.save(commit=False)
            #     billing.user = request.user
            #     billing.save()
            b_form.save()
            messages.success(request, _('Billing details updated successfully'))
            return redirect('profile')
    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)
        b_form = BillingUpdateForm(instance=request.user.billing)

    context = {
        'p_form': p_form,
        'u_form': u_form,
        'b_form': b_form,
        'title': _('My Profile'),
        'breadcrumb': _('My Profile'),
    }
    return render(request, 'users/profile.html', context)
