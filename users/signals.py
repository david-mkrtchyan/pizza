from django.db.models.signals import post_save
from django.contrib.auth.models import User
from .models import Profile,Billing
from django.dispatch import receiver


@receiver(post_save, sender=User)
def after_save_create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def before_save_save_profile(sender, instance, **kwargs):
    instance.profile.save()


@receiver(post_save, sender=User)
def after_save_create_billing(sender, instance, created, **kwargs):
    if created:
        Billing.objects.create(user=instance)


@receiver(post_save, sender=User)
def before_save_save_billing(sender, instance, **kwargs):
    instance.billing.save()
