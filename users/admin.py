from django.contrib import admin
from .models import Profile, Billing
from .forms import BillingUpdateForm
from django import forms
from blog.models import District


@admin.register(Billing)
class BillingAdmin(admin.ModelAdmin):
    form = BillingUpdateForm
    list_display = ('user', 'first_name', 'last_name', 'email', 'phone', 'district', 'address')
    list_filter = ('district',)
