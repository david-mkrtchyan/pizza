from django.db import models
from django.contrib.auth.models import User
from PIL import Image
from django.urls import reverse
from phonenumber_field.modelfields import PhoneNumberField
from blog.models import District


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')

    def __str__(self):
        return f'{self.user.username}'

    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'pk': self.pk})

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        img = Image.open(self.image.path)
        if img.width > 200 or img.height > 200:
            output_size = (150, 150)
            img.thumbnail(output_size)
            img.save(self.image.path)


class Billing(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=255, blank=True, null=True, )
    last_name = models.CharField(max_length=255, blank=True, null=True, )
    email = models.EmailField()
    phone = PhoneNumberField(blank=True, null=True)
    district = models.OneToOneField(District, on_delete=models.CASCADE, blank=True, null=True)
    address = models.CharField(blank=True, null=True, max_length=255)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.user.username}'

    # def get_absolute_url(self):
    #     return reverse('post-detail', kwargs={'pk': self.pk})

    # def save(self, *args, **kwargs):
    #     super().save(*args, **kwargs)
