"""pizza URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from users import views as users_views
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns

admin.autodiscover()
from django.utils.translation import gettext as _

urlpatterns = [

    path('admin/', admin.site.urls),
    path('register/', users_views.register, name='register'),
    path('profile/', users_views.profile, name='profile'),


    path('login/',
         auth_views.LoginView.as_view(extra_context={'title': str(_('Sign In')), 'breadcrumb': str(_('Sign In'))},
                                      template_name='users/login.html'),
         name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),
    path('password-reset/', auth_views.PasswordResetView.as_view(
        extra_context={'title': str(_('Reset password')), 'breadcrumb': str(_('Reset password'))},
        template_name='users/password_reset.html'), name='password_reset'),
    path('password-reset/done/', auth_views.PasswordResetDoneView.as_view(
        extra_context={'title': str(_('Reset password')), 'breadcrumb': str(_('Reset password'))},
        template_name='users/password_reset_done.html'), name='password_reset_done'),
    path('password-reset-confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(
        extra_context={'title': str(_('Reset password')), 'breadcrumb': str(_('Reset password'))},
        template_name='users/password_reset_confirm.html'), name='password_reset_confirm'),
    path('password-reset-complete/', auth_views.PasswordResetCompleteView.as_view(
        extra_context={'title': str(_('Reset password')), 'breadcrumb': str(_('Reset password'))},
        template_name='users/password_reset_complete.html'), name='password_reset_complete'),

    path('i18n/', include('django.conf.urls.i18n')),
    path('', include('blog.urls')),
    path('accounts/', include('admin.urls')),
    path('tinymce/', include('tinymce.urls')),
]

# urlpatterns += i18n_patterns(
#     path('admin/', admin.site.urls),
#     path('', include('blog.urls')),
#     path('accounts/', include('admin.urls')),
#     path('tinymce/', include('tinymce.urls')),
#     prefix_default_language = False,
# )


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
