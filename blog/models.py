from django.db import models
from django.utils import timezone
from django_mysql.models import EnumField
import secrets
from pizza import settings
import os
from django.contrib.auth.models import User
from tinymce.models import HTMLField
from django.utils.translation import to_locale, get_language


class Images(models.Model):
    path = models.ImageField()
    object_id = models.IntegerField()
    type = EnumField(choices=['post', 'category', 'about'], default=('post'))
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(timezone.now, default=timezone.now)
    rank = models.IntegerField(default=0, null=True)

    class Meta:
        db_table = 'images'
    # fields =
    # ['path','object_id','type','created_at'.,'updated_at','rank']


class Contact(models.Model):
    name = models.CharField(null=False, max_length=100)
    subject = models.CharField(null=True, max_length=255, blank=False)
    description = models.TextField()
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    image = models.ImageField(null=True)
    email = models.EmailField(null=True, blank=False)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(timezone.now, default=timezone.now)
    order = models.PositiveIntegerField(null=True)

    def __str__(self):
        if self.user:
            return self.name + '(' + self.user + ')'
        else:
            return self.name

    class Meta:
        db_table = 'contacts'


class Slider(models.Model):
    image = models.ImageField(null=True, upload_to='slider')
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(timezone.now, default=timezone.now)
    order = models.PositiveIntegerField(null=True)
    status = models.BooleanField(null=True, default=False)

    def image_tag(self):
        from django.utils.safestring import mark_safe
        return mark_safe('<img src="/media/%s" width="150" height="150" />' % (self.image))

    image_tag.short_description = 'Image'
    image_tag.allow_tags = True

    def __str__(self):
        return str(self.image)

    class Meta:
        db_table = 'sliders'


class District(models.Model):
    image = models.ImageField(null=True, upload_to='slider', blank=True)
    order = models.PositiveIntegerField(null=True)
    status = models.BooleanField(null=True, default=False)
    name_en = models.CharField(max_length=100)
    name_ru = models.CharField(max_length=100)
    name_hy = models.CharField(max_length=100)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(timezone.now, default=timezone.now)

    class Meta:
        db_table = 'districts'

    def name(self):
        lang = to_locale(get_language())
        return getattr(self, 'name_%s' % lang)

    def __str__(self):
        lang = to_locale(get_language())
        return getattr(self, 'name_%s' % lang)


# def get_absolute_url(self):
#    return reverse('home')


class About(models.Model):
    title_en = models.CharField(max_length=100)
    title_ru = models.CharField(max_length=100)
    title_hy = models.CharField(max_length=100)

    content_en = HTMLField()
    content_ru = HTMLField()
    content_hy = HTMLField()

    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(timezone.now, default=timezone.now)

    def title(self):
        lang = to_locale(get_language())
        return getattr(self, 'title_%s' % lang)

    def content(self):
        lang = to_locale(get_language())
        return getattr(self, 'content_%s' % lang)

    def shortContent(self, count=270):
        content = self.content()
        if len(content) > count:
            return content[0:count] + '...'

        return content[0:count]

    class Meta:
        db_table = 'about'

    def __str__(self):
        return self.title_hy

# class Variable(models.Model):
# 	name = models.CharField(max_length=100)
# 	key = models.SlugField()
# 	description = models.TextField()
# 	type = models.TextField()
# 	created_at = models.DateField(auto_now_add=True)
# 	updated_at = models.DateField(timezone.now, default=timezone.now)
# 	order = models.PositiveIntegerField(null=True)

# 	class Meta:
# 		db_table = 'Variables'
