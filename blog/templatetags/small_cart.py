from django import template
from cart.views import session_name
import itertools
from django.utils.translation import to_locale, get_language
from cart.classes.cart import Cart
register = template.Library()

@register.inclusion_tag('cart/template/small_cart.html', takes_context=True)
def show_cart_modal(context):
    basket = context.request.session.get(session_name)
    return Cart.getSessionObjectForView(basket)
