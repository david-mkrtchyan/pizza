from django import template
from category.models import Category

register = template.Library()

@register.simple_tag
def total_count():
    #menu = Category.objects.filter(is_active=True).all()
    return {'menu' : menu }


@register.inclusion_tag('blog/templatetags/category_menu.html')
def show_menu():
    menu = Category.objects.filter(is_active=True).all()
    return {'all_menu' : menu }
