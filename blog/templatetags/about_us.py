from django import template
from blog.models import About

register = template.Library()

@register.simple_tag
def total_count():
    #menu = Category.objects.filter(is_active=True).all()
    return {'menu' : menu }


@register.inclusion_tag('blog/templatetags/about.html')
def about():
    about = About.objects.first()
    return {'about' : about }
