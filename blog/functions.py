import secrets
from pizza import settings
import os
from PIL import Image
import secrets
from urllib.request import urlopen

def HandleUploadedFile(f,direct):
    pathPost = settings.MEDIA_URL+direct+'/'
    
    os.makedirs(pathPost, exist_ok=True)

    try:
        random_hex = secrets.token_hex(8)
        _, f_ext = os.path.splitext(f.name)
        picture_fn = random_hex + f_ext

        destination = open(pathPost[1:]+'%s' % picture_fn, 'wb+')
        for chunk in f.chunks():
            destination.write(chunk)
    except Exception as e:
        raise
    else:
        destination.close()
        return  os.path.join(pathPost,picture_fn)
     
def uploadImageFromUrl(url,direct):
    try:
        img = Image.open(urlopen(url))
        key = secrets.token_hex(16)
        path = f'{direct}{key}.jpg'
        img.save(path)
        return path
    except Exception as e:
        raise

def file_exists(filepath,default = 'post_default.jpg'):
    try:
        image = Image.open(filepath)
    except FileNotFoundError as e:
        return settings.MEDIA_URL+default 
    except Exception as e:
        return False
    else:
        return filepath     