from django.contrib import admin
from blog.models import Contact, About, Slider, District
from django.forms import ModelForm, TextInput
from suit.widgets import HTML5Input, SuitDateWidget, SuitTimeWidget, SuitSplitDateTimeWidget
from blog.forms import ContactForm
from suit.admin import SortableModelAdmin
from .functions import HandleUploadedFile
from .models import Images
from post.templatetags.file_exists import file_exists
from django.urls import reverse


@admin.register(Contact)
class ContactAdmin(SortableModelAdmin, admin.ModelAdmin):
    change_form_template = 'admin/blog/contact/change_form.html'
    sortable = 'order'
    list_display = ('name', 'subject', 'email')
    form = ContactForm

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}

        return super().change_view(
            request, object_id, form_url, extra_context=extra_context,
        )


@admin.register(About)
class AboutAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {
            'classes': ('suit-tab', 'suit-tab-ruLang',),
            'fields': ['title_ru', 'content_ru']
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-enLang',),
            'fields': ['title_en', 'content_en']
        }),

        (None, {
            'classes': ('suit-tab', 'suit-tab-hyLang',),
            'fields': ['title_hy', 'content_hy']
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-images',),
            'fields': []}),
    ]

    change_form_template = 'admin/blog/about/change_form.html'

    suit_form_tabs = (('ruLang', 'Ru'), ('enLang', 'En'), ('hyLang', 'Hy'), ('images', 'Images'))

    suit_form_includes = (
        ('admin/blog/about/images.html', '', 'images'),
    )

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()

        for afile in request.FILES.getlist('photos'):
            path = HandleUploadedFile(afile, 'about')
            if path:
                postImage = Images(path=path, object_id=obj.id,
                                   type='about', rank=0)
                postImage.save()

    def __str__(self):
        return self.name_hy

    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js',
            'https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js',
            '/media/admin/js/admin.js',
            '/media/node_modules/bootstrap-fileinput/js/plugins/piexif.js',
            '/media/node_modules/bootstrap-fileinput/js/plugins/sortable.js',
            '/media/node_modules/bootstrap-fileinput/js/plugins/purify.js',
            '/media/node_modules/bootstrap-fileinput/js/fileinput.js',
        )
        css = {
            'all': (
                '/media/admin/css/style.css',
                '/media/node_modules/bootstrap-fileinput/css/fileinput.min.css',
                # 'https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css',
            )
        }

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}

        imagesObject = Images.objects.filter(object_id=object_id, type='about').order_by('rank')
        images = imagesConfig = ''

        if imagesObject:
            for image in imagesObject:
                images += '"' + str(file_exists(image.path)) + '",'
                imagesConfig += "{'url': '" + reverse('image-delete') + "', 'key':'" + str(
                    image.id) + "', 'extra' : {'key':'" + str(image.id) + "','type': 'about'} },"

        extra_context['images'] = str(images)
        extra_context['imagesConfig'] = imagesConfig

        return super().change_view(
            request, object_id, form_url, extra_context=extra_context,
        )


class AboutAdmin(admin.ModelAdmin):
    sortable = 'order'


@admin.register(Slider)
class SliderAdmin(admin.ModelAdmin):
    sortable = 'order'
    list_filter = ('order', 'status', 'updated_at')

    fields = ['image_tag', 'image', 'order', 'status', 'updated_at']
    readonly_fields = ['image_tag', ]
    list_display = ('image_tag','order', 'status', 'updated_at',)

    # radio_fields = {"category": admin.VERTICAL}
    # autocomplete_fields = ['title_en','title_ru']'title_hy',
    # raw_id_fields = ("category",)


@admin.register(District)
class DistrictAdmin(admin.ModelAdmin):
    sortable = 'order'
    list_filter = ('order', 'name_en', 'name_ru', 'name_hy', 'status', 'updated_at')
    # radio_fields = {"category": admin.VERTICAL}
    # autocomplete_fields = ['title_en','title_ru']'title_hy',
    # raw_id_fields = ("category",)
