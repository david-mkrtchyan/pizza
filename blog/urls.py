from django.urls import path,include
from . import views
from .views import ContactForm
from django.conf.urls import url
from django.views.i18n import JavaScriptCatalog

urlpatterns = [
    path('jsi18n/', JavaScriptCatalog.as_view(), name='javascript-catalog'),
    path('', views.home, name='home'),
    path('about', views.about, name='about'),
    path('contact', ContactForm.as_view(), name='contact'),
    path('food', views.food, name='food'),
    path('', include('category.urls')),
    path('', include('post.urls')),
    path('', include('cart.urls')),
    path('', include('users.urls')),

]



