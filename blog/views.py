from django.shortcuts import render
from django.utils.translation import ugettext, ungettext, gettext as _
import locale
from django.utils import translation
from .models import About, Contact, Images, Slider
from post.models import Post
from category.models import Category
from django.utils.translation import to_locale, get_language
from .forms import ContactForm
from django.views.generic import CreateView
from django.http import JsonResponse
from .classes import AjaxableResponseMixin
from django.urls import reverse_lazy
from django.db.models import Q


def home(request):
    sliders = Slider.objects.filter(status=True).order_by('-order').all()
    menuFirst = Post.objects.filter(status=True).order_by('-order', 'id').all()[0:6]
    menuSecond = Post.objects.filter(status=True).order_by('-order', 'id').all()[6:12]
    drink = Category.objects.filter(slug='alcoholic-drinks').first()
    specialDishes = Post.objects.filter(~Q(category=drink), status=True).order_by('-order', 'id').all()[12:21]

    return render(request, 'blog/home.html', {
        'title': _('Home'),
        'sliders': sliders,
        'menuFirst': menuFirst,
        'menuSecond': menuSecond,
        'specialDishes': specialDishes,
    })


def about(request):
    content = About.objects.first()
    images = Images.objects.filter(type='about', object_id=content.id).all()
    return render(request, 'blog/about.html', {
        'title': _('About'),
        'breadcrumb': _('About Us'),
        'content': content,
        'images': images,
    })


class ContactForm(AjaxableResponseMixin, CreateView):
    model = Contact
    template_name = 'blog/contact.html'
    fields = ['name', 'description', 'email', 'subject']
    success_url = reverse_lazy('home')

    def get_context_data(self, **kwargs):
        context = super(ContactForm, self).get_context_data(**kwargs)
        context['title'] = _('Contact Us')
        context['breadcrumb'] = _('Contact')
        return context


def food(request):
    posts = Post.objects.filter(status=True).all()
    categories = Category.objects.filter(is_active=True).order_by('order').all()
    lang = to_locale(get_language())
    return render(request, 'blog/food.html', {
        'title': _('Menu'),
        'breadcrumb': _('Menu'),
        'posts': posts,
        'categories': categories,
        'lang': to_locale(get_language()),
    })
