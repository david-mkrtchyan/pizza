from django.forms import ModelForm,ImageField,Textarea
from blog.models import Contact
from suit.widgets import EnclosedInput,HTML5Input,SuitDateWidget, SuitTimeWidget, SuitSplitDateTimeWidget,AutosizedTextarea,LinkedSelect

class ContactForm(ModelForm):
    class Meta:
        model = Contact
        fields = ['name','description','email','subject']
        widgets = {
            'title': EnclosedInput(prepend='icon-pencil'),
            'subject': EnclosedInput(),
            'email': EnclosedInput(),
            'description': AutosizedTextarea,
            'description': AutosizedTextarea(attrs={'rows': 3, 'class': 'input-xlarge'}),
            'user': LinkedSelect,
            'updated_at': SuitDateWidget,
        }