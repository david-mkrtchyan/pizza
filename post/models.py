from django.db import models
from django.contrib.auth.models import User
from tinymce.models import HTMLField
from category.models import Category
from blog.models import Images
from django.utils.translation import to_locale, get_language


class Post(models.Model):
    user = models.ForeignKey(User, on_delete=True, default=1)
    title_en = models.CharField(max_length=250, null=True, blank=False)
    title_ru = models.CharField(max_length=250, null=True, )
    title_hy = models.CharField(max_length=250, null=True, )
    content_en = HTMLField('Content', null=True)
    content_ru = HTMLField('Content', null=True)
    content_hy = HTMLField('Content', null=True)
    order = models.PositiveIntegerField(null=True)
    category = models.ForeignKey(Category, on_delete=True, blank=True)
    publish = models.DateField(auto_now=False, auto_now_add=True, )
    status = models.BooleanField(default=True)
    price = models.DecimalField(max_digits=12, decimal_places=2, null=True)
    slug = models.SlugField(unique=True)
    popular = models.BooleanField(null=True, default=False)

    class Meta:
        db_table = "posts"

        # def images(self):

    #     return Images.object.filter(type='post',object_id=self.id).order_by('-rank').all()

    def title(self):
        lang = to_locale(get_language())
        return getattr(self, 'title_%s' % lang)

    def getPrice(self):
        return int(self.price)

    def content(self):
        lang = to_locale(get_language())
        return getattr(self, 'content_%s' % lang)

    def shortContent(self):
        content = self.content()
        return content[0:100]

    def shortTitle(self, count=18):
        title = self.title()
        if len(title) > count:
            return title[0:count] + '...'

        return title[0:count]

    def get_cat_list(self):  # for now ignore this instance method,
        k = self.category
        breadcrumb = ["dummy"]
        while k is not None:
            breadcrumb.append(k.slug)
            k = k.parent

        for i in range(len(breadcrumb) - 1):
            breadcrumb[i] = '/'.join(breadcrumb[-1:i - 1:-1])
        return breadcrumb[-1:0:-1]

    def image(self):  # for now ignore this instance method,
        return Images.objects.filter(type='post', object_id=self.id).order_by('-rank').first()

    def images(self):
        return Images.objects.filter(type='post', object_id=self.id).order_by('-rank').all()
