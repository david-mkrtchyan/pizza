from django.urls import path
from . import views

urlpatterns = [
    path('product/<str:slug>', views.product, name='product'),
    path('category/<str:slug>', views.category, name='category-post'),
    path('search', views.search, name='search'),
]
