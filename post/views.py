from django.shortcuts import render, get_object_or_404
from .models import Post
from django.utils.translation import gettext as _
from category.models import Category
from django.db.models import Q
from django.core.paginator import Paginator
from django.utils.translation import to_locale, get_language


def product(request, slug):
    product = get_object_or_404(Post, slug=slug)
    images = product.images
    similars = Post.objects.filter(category=product.category).exclude(slug=slug).all()
    categories = Category.objects.order_by('?').all()[:7]
    popular = Post.objects.filter(popular=True).exclude(slug=slug).order_by('-id').all()[:4]

    return render(request, 'product/post.html', {
        'title': product.title,
        'breadcrumb': product.title,
        'product': product,
        'popular': popular,
        'images': images,
        'similars': similars,
        'categories': categories,
    })


def category(request, slug):
    category = get_object_or_404(Category, slug=slug)
    posts = Post.objects.filter(category=category).all()
    paginator = Paginator(posts, 20)
    page = request.GET.get('page')
    posts = paginator.get_page(page)

    return render(request, 'product/category-post.html', {
        'title': category.name,
        'breadcrumb': category.name,
        'category': category,
        'posts': posts
    })


def search(request):
    lang = to_locale(get_language())
    getSearch = request.GET.get('q', '')
    search = getSearch + '%'
    posts = Post.objects.raw(
        'SELECT * FROM posts WHERE title_' + str(lang) + ' like %s OR content_' + str(lang) + ' like %s',
        tuple([search, search]))
    paginator = Paginator(posts, 20)
    page = request.GET.get('page')
    posts = paginator.get_page(page)

    return render(request, 'product/category-post.html', {
        'title': str(getSearch),
        'breadcrumb': _('Search'),
        'posts': posts
    })
