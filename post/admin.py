from django.contrib import admin
from .models import Post
from blog.models import Images
from blog.functions import HandleUploadedFile
from PIL import Image
import secrets
import os
from django.urls import path
from django.template.response import TemplateResponse
from django import template
from django.core.files.storage import default_storage
from django import template
from post.templatetags.file_exists import file_exists
from django.urls import reverse
from import_export import resources
from import_export.admin import ImportExportModelAdmin


class PostResource(resources.ModelResource):
    class Meta:
        model = Post
        import_id_fields = ('isbn',)
        fields = (
        'id', 'title_en', 'title_ru', 'title_hy', 'category__name_en', 'content_en', 'content_ru', 'content_hy',
        'status', 'slug')
        export_order = (
        'id', 'category__name_en', 'title_en', 'title_ru', 'title_hy', 'content_en', 'content_ru', 'content_hy',
        'status', 'slug')
# exclude = ('imported', )


class PostAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    resource_class = PostResource

    list_filter = ('category', 'title_en', 'title_ru', 'title_hy', 'status', 'popular')
    # radio_fields = {"category": admin.VERTICAL}
    search_fields = ['category', 'title_en', 'title_ru', 'title_hy'],
    # autocomplete_fields = ['title_en','title_ru']'title_hy',
    # raw_id_fields = ("category",)
    list_display = ('title_en', 'title_ru', 'title_hy', 'category', 'user', 'slug', 'status', 'popular')

    change_form_template = 'admin/post/post/change_form.html'

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}

        imagesObject = Images.objects.filter(object_id=object_id, type='post').order_by('rank')
        images = imagesConfig = ''

        if imagesObject:
            for image in imagesObject:
                images += '"' + str(file_exists(image.path)) + '",'
                imagesConfig += "{'url': '" + reverse('image-delete') + "', 'key':'" + str(
                    image.id) + "', 'extra' : {'key':'" + str(image.id) + "','type': 'posts'} },"

        extra_context['images'] = str(images)
        extra_context['imagesConfig'] = imagesConfig

        return super().change_view(
            request, object_id, form_url, extra_context=extra_context,
        )

    fieldsets = [
        (None, {
            'classes': ('suit-tab', 'suit-tab-general',),
            'fields': ['user', 'order', 'category', 'status', 'price', 'slug', 'popular']
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-ruLang',),
            'fields': ['title_ru', 'content_ru']
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-enLang',),
            'fields': ['title_en', 'content_en']
        }),

        (None, {
            'classes': ('suit-tab', 'suit-tab-hyLang',),
            'fields': ['title_hy', 'content_hy']
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-images',),
            'fields': []}),
    ]

    suit_form_tabs = (
    ('general', 'General'), ('ruLang', 'Ru'), ('enLang', 'En'), ('hyLang', 'Hy'), ('images', 'Images'))

    suit_form_includes = (
        ('admin/post/post/images.html', '', 'images'),
    )

    def suit_cell_attributes(self, obj, column):
        if column == 'status':
            return {'class': 'text-center', 'data-id': obj.status}
        return {'class': 'text-center'}

    # fieldsets = (
    # 	(None, {
    # 		'fields': (('user', 'status'), 'category', 'content', 'title_en','title_ru')'title_hy',
    # 	}),
    # 	('Advanced options', {
    # 		'classes': ('wide', 'extrapretty', 'collapse',),
    # 		'fields': ('publish', 'slug'),
    # 	}),
    # )

    # Or bit more advanced example
    def suit_row_attributes(self, obj, request):
        css_class = {
            1: 'success',
            0: 'warning',
            -1: 'error',
        }.get(obj.status)
        if css_class:
            return {'class': css_class, 'data': obj.status}

    def get_ordering(self, request):
        if request.user.is_superuser:
            return ['-id']
        else:
            return ['-id']

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()

        for afile in request.FILES.getlist('photos'):
            path = HandleUploadedFile(afile, 'posts')
            if path:
                postImage = Images(path=path, object_id=obj.id,
                                   type='post', rank=0)
                postImage.save()

    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js',
            'https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js',
            '/media/admin/js/admin.js',
            '/media/node_modules/bootstrap-fileinput/js/plugins/piexif.js',
            '/media/node_modules/bootstrap-fileinput/js/plugins/sortable.js',
            '/media/node_modules/bootstrap-fileinput/js/plugins/purify.js',
            '/media/node_modules/bootstrap-fileinput/js/fileinput.js',
        )
        css = {
            'all': (
                '/media/admin/css/style.css',
                '/media/node_modules/bootstrap-fileinput/css/fileinput.min.css',
                # 'https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css',
            )
        }


admin.site.register(Post, PostAdmin)
