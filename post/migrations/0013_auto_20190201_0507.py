# Generated by Django 2.1.5 on 2019-02-01 13:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('post', '0012_auto_20190201_0504'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='popular',
            field=models.BooleanField(default=False, null=True),
        ),
    ]
