from django import template
from django.core.files.storage import default_storage
from django.core.files import File
import sys
from pizza import settings
from pathlib import Path
from PIL import Image

register = template.Library()

@register.filter(name='file_exists')
def file_exists(filepath):
    try:
        path = bytes(sys.path[0]+str(filepath),"utf-8")
        image = Image.open(path)
    except FileNotFoundError as e:
        return settings.MEDIA_URL+'post_default.jpg'    
    except Exception as e:
        return False
    else:
        return filepath

register.filter(file_exists)       