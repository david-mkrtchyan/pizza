-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 02, 2019 at 05:06 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.1.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizza`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `title_en` varchar(100) NOT NULL,
  `title_ru` varchar(100) NOT NULL,
  `title_hy` varchar(100) NOT NULL,
  `content_en` longtext NOT NULL,
  `content_ru` longtext NOT NULL,
  `content_hy` longtext NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `title_en`, `title_ru`, `title_hy`, `content_en`, `content_ru`, `content_hy`, `created_at`, `updated_at`) VALUES
(9, 'OUR HISTORY', 'OUR HISTORY', 'OUR HISTORY', '<p>1947&nbsp;ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.ex ea commodo consequat.</p>\r\n<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos quised quia non numquam eius modi tempora incidunt.cia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnisaque ipsquae.</p>', '<p>1947&nbsp;ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.ex ea commodo consequat.</p>\r\n<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos quised quia non numquam eius modi tempora incidunt.cia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnisaque ipsquae.</p>', '<p>1947&nbsp;ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.ex ea commodo consequat.</p>\r\n<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos quised quia non numquam eius modi tempora incidunt.cia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnisaque ipsquae.</p>', '2019-01-30', '2019-01-30');

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add contact', 1, 'add_contact'),
(2, 'Can change contact', 1, 'change_contact'),
(3, 'Can delete contact', 1, 'delete_contact'),
(4, 'Can view contact', 1, 'view_contact'),
(5, 'Can add images', 2, 'add_images'),
(6, 'Can change images', 2, 'change_images'),
(7, 'Can delete images', 2, 'delete_images'),
(8, 'Can view images', 2, 'view_images'),
(9, 'Can add category', 3, 'add_category'),
(10, 'Can change category', 3, 'change_category'),
(11, 'Can delete category', 3, 'delete_category'),
(12, 'Can view category', 3, 'view_category'),
(13, 'Can add post', 4, 'add_post'),
(14, 'Can change post', 4, 'change_post'),
(15, 'Can delete post', 4, 'delete_post'),
(16, 'Can view post', 4, 'view_post'),
(17, 'Can add profile', 5, 'add_profile'),
(18, 'Can change profile', 5, 'change_profile'),
(19, 'Can delete profile', 5, 'delete_profile'),
(20, 'Can view profile', 5, 'view_profile'),
(21, 'Can add log entry', 6, 'add_logentry'),
(22, 'Can change log entry', 6, 'change_logentry'),
(23, 'Can delete log entry', 6, 'delete_logentry'),
(24, 'Can view log entry', 6, 'view_logentry'),
(25, 'Can add permission', 7, 'add_permission'),
(26, 'Can change permission', 7, 'change_permission'),
(27, 'Can delete permission', 7, 'delete_permission'),
(28, 'Can view permission', 7, 'view_permission'),
(29, 'Can add group', 8, 'add_group'),
(30, 'Can change group', 8, 'change_group'),
(31, 'Can delete group', 8, 'delete_group'),
(32, 'Can view group', 8, 'view_group'),
(33, 'Can add user', 9, 'add_user'),
(34, 'Can change user', 9, 'change_user'),
(35, 'Can delete user', 9, 'delete_user'),
(36, 'Can view user', 9, 'view_user'),
(37, 'Can add content type', 10, 'add_contenttype'),
(38, 'Can change content type', 10, 'change_contenttype'),
(39, 'Can delete content type', 10, 'delete_contenttype'),
(40, 'Can view content type', 10, 'view_contenttype'),
(41, 'Can add session', 11, 'add_session'),
(42, 'Can change session', 11, 'change_session'),
(43, 'Can delete session', 11, 'delete_session'),
(44, 'Can view session', 11, 'view_session'),
(45, 'Can add about', 12, 'add_about'),
(46, 'Can change about', 12, 'change_about'),
(47, 'Can delete about', 12, 'delete_about'),
(48, 'Can view about', 12, 'view_about'),
(49, 'Can add slider', 13, 'add_slider'),
(50, 'Can change slider', 13, 'change_slider'),
(51, 'Can delete slider', 13, 'delete_slider'),
(52, 'Can view slider', 13, 'view_slider'),
(53, 'Can add billing', 14, 'add_billing'),
(54, 'Can change billing', 14, 'change_billing'),
(55, 'Can delete billing', 14, 'delete_billing'),
(56, 'Can view billing', 14, 'view_billing'),
(57, 'Can add districts', 15, 'add_districts'),
(58, 'Can change districts', 15, 'change_districts'),
(59, 'Can delete districts', 15, 'delete_districts'),
(60, 'Can view districts', 15, 'view_districts'),
(61, 'Can add district', 15, 'add_district'),
(62, 'Can change district', 15, 'change_district'),
(63, 'Can delete district', 15, 'delete_district'),
(64, 'Can view district', 15, 'view_district'),
(65, 'Can add order', 16, 'add_order'),
(66, 'Can change order', 16, 'change_order'),
(67, 'Can delete order', 16, 'delete_order'),
(68, 'Can view order', 16, 'view_order'),
(69, 'Can add order billing', 17, 'add_orderbilling'),
(70, 'Can change order billing', 17, 'change_orderbilling'),
(71, 'Can delete order billing', 17, 'delete_orderbilling'),
(72, 'Can view order billing', 17, 'view_orderbilling'),
(73, 'Can add order product', 18, 'add_orderproduct'),
(74, 'Can change order product', 18, 'change_orderproduct'),
(75, 'Can delete order product', 18, 'delete_orderproduct'),
(76, 'Can view order product', 18, 'view_orderproduct'),
(77, 'Can add post', 19, 'add_post'),
(78, 'Can change post', 19, 'change_post'),
(79, 'Can delete post', 19, 'delete_post'),
(80, 'Can view post', 19, 'view_post');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$120000$LlpwXciVhlUK$bpD22v2r6QK389hiHw3qv/0ZUCAaR9fh2+QAdFVoMek=', '2019-05-02 13:34:47.794138', 1, 'admin', 'David2', 'dav2', 'davidmkrtchyan1992@mail.ru', 1, 1, '2019-01-29 11:07:33.365666'),
(2, 'pbkdf2_sha256$120000$SVFxznIxXm73$Wbhq1Wux3A/AJRifQgA5lvWOpKPF3y7q5rT7Cy60YRM=', NULL, 0, 'test', '', '', 'test@mail.ru', 0, 1, '2019-04-03 12:27:53.101332');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categoryies`
--

CREATE TABLE `categoryies` (
  `id` int(11) NOT NULL,
  `name_en` varchar(200) DEFAULT NULL,
  `name_ru` varchar(200) DEFAULT NULL,
  `name_hy` varchar(200) DEFAULT NULL,
  `slug` varchar(50) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `order` int(10) UNSIGNED DEFAULT NULL,
  `lft` int(10) UNSIGNED DEFAULT NULL,
  `rght` int(10) UNSIGNED DEFAULT NULL,
  `tree_id` int(10) UNSIGNED DEFAULT NULL,
  `level` int(10) UNSIGNED DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categoryies`
--

INSERT INTO `categoryies` (`id`, `name_en`, `name_ru`, `name_hy`, `slug`, `is_active`, `image`, `order`, `lft`, `rght`, `tree_id`, `level`, `parent_id`) VALUES
(34, 'Barbecue', 'Шашлык', 'Խորոված', 'barbecue', 1, '/media/category/5b1caa555b7c38c1949009c5e9fcaf0a.jpg', 0, 1, 2, 1, 0, NULL),
(35, 'Beverages', 'Освежающие напитки', 'Զովացուցիչ ըմպելիք', 'beverages', 1, '/media/category/e4d4ceb98243b16ea061e6c584d53f0f.jpg', 1, 1, 2, 2, 0, NULL),
(36, 'Alcoholic drinks', 'Алкогольные напитки', 'Ալկոհոլային ըմպելիք', 'alcoholic-drinks', 1, '/media/category/839dab8b4c45560806f1d76592e7d4cf.jpg', 2, 1, 2, 3, 0, NULL),
(37, 'Complex lunch', 'Комплекс ланч', 'Կոմպլեքս լանչ', 'complex-lunch', 1, '/media/category/022097f225347d41c9ff9ceb577c3654.jpg', 3, 1, 2, 4, 0, NULL),
(38, 'Pizza', 'Пицца', 'Պիցցա', 'pizza', 1, '/media/category/d448a3caa293388643231003cd2825ca.jpg', 4, 1, 2, 5, 0, NULL),
(39, 'Khachapuri, pies', 'Хачапури, пирожки', 'Խաչապուրի, կարկանդակ', 'khachapur-pies', 1, '/media/category/82afa51c53a901bae734ec711d5d5c0a.jpg', 5, 1, 2, 6, 0, NULL),
(40, 'Salads', 'Салаты', 'Աղցաններ', 'salads', 1, '/media/category/15776a3e6370bb136e5c31294123510d.jpg', 6, 1, 2, 7, 0, NULL),
(41, 'Soups', 'Супы', 'Ապուրներ', 'soups', 1, '/media/category/22fc2a9bd4de35ce604e7aca8451fd11.jpg', 7, 1, 2, 8, 0, NULL),
(42, 'Hot dishes', 'Горячие блюда', 'Տաք ուտեստներ', 'hot-dishes', 1, '/media/category/f2a17853d428c129e25f3f8271677198.jpg', 8, 1, 2, 9, 0, NULL),
(43, 'Side dishes', 'Гарниры', 'Խավարտ', 'side-dishes', 1, '/media/category/99a744988be6b9c04ffe6668db681e30.jpg', 9, 1, 2, 10, 0, NULL),
(44, 'Snacks', 'Закуски', 'Նախուտեստներ', 'snacks', 1, '/media/category/3b39aad0619d33f199d4020dfa678ad4.jpg', 10, 1, 2, 11, 0, NULL),
(45, 'Fast food', 'Фаст фуд', 'Արագ սնունդ', 'fast-food', 1, '/media/category/3fa7ecaec58b4d82af27bd89086d40a5.jpg', 11, 1, 2, 12, 0, NULL),
(46, 'Child food', 'Детское питание', 'Մանկական սնունդ', 'child-food', 1, '/media/category/14b3e1aa94cf89304b09f2067cb95ded.jpg', 12, 1, 2, 13, 0, NULL),
(47, 'Dessert', 'Десерт', 'Աղանդեր', 'dessert', 1, '/media/category/db89d0a7f056e86bc0be0752ad975f37.jpg', 13, 1, 2, 14, 0, NULL),
(48, 'Sauces', 'Соусы', 'Սոուսներ', 'sauces', 1, '/media/category/40bf24880d306080995c9da91461ab6e.jpg', 14, 1, 2, 15, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `order` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(254) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `description`, `image`, `created_at`, `updated_at`, `order`, `user_id`, `email`, `subject`) VALUES
(11, 'asd', 'dasdsa', '', '2019-01-31', '2019-01-31', NULL, NULL, 'vazgen.mkrtchyan001@gmail.com', 'sad'),
(12, 'asd', 'dasdsa', '', '2019-01-31', '2019-01-31', NULL, NULL, 'vazgen.mkrtchyan001@gmail.com', 'sad'),
(13, 'tort', 'asdsa', '', '2019-01-31', '2019-01-31', NULL, NULL, 'vazgen.mkrtchyan001@gmail.com', 'asdsa'),
(14, 'tort', 'asdsa', '', '2019-01-31', '2019-01-31', NULL, NULL, 'vazgen.mkrtchyan001@gmail.com', 'asdsa'),
(15, 'tort', 'asdsa', '', '2019-01-31', '2019-01-31', NULL, NULL, 'vazgen.mkrtchyan001@gmail.com', 'asdsa'),
(16, 'sa', 'asda', '', '2019-01-31', '2019-01-31', NULL, NULL, 'vazgen.mkrtchyan001@gmail.com', 'asd'),
(17, 'sad', 'sad', '', '2019-01-31', '2019-01-31', NULL, NULL, 'vazgen.mkrtchyan001@gmail.com', 'asd'),
(18, 'asd', 'sad', '', '2019-01-31', '2019-01-31', NULL, NULL, 'vazgen.mkrtchyan001@gmail.com', 'sad'),
(19, 'tort', 'dsfsd', '', '2019-01-31', '2019-01-31', NULL, NULL, 'vazgen.mkrtchyan001@gmail.com', 'asd'),
(20, 'asd', 'ddsad', '', '2019-02-08', '2019-02-08', NULL, NULL, 'vazgen.mkrtchyan001@gmail.com', 'sad'),
(21, 'asd', 'asdasd', '', '2019-02-08', '2019-02-08', NULL, NULL, 'vazgen.mkrtchyan001@gmail.com', 'sad'),
(22, 'test', 'david mkrtchyan', '', '2019-02-08', '2019-02-08', NULL, NULL, 'david.mkrtchyan1992@mail.ru', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` int(11) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `order` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `name_en` varchar(100) NOT NULL,
  `name_ru` varchar(100) NOT NULL,
  `name_hy` varchar(100) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `image`, `order`, `status`, `name_en`, `name_ru`, `name_hy`, `created_at`, `updated_at`) VALUES
(1, '', NULL, 1, 'Ajapnyak', 'Ачапняк', 'Աջափնյակ', '2019-03-25', '2019-03-25'),
(2, '', NULL, 1, 'Arabkir', 'Арабкир', 'Արաբկիր', '2019-03-25', '2019-03-25'),
(3, '', NULL, 1, 'Avan', 'Аван', 'Ավան', '2019-03-25', '2019-03-25'),
(4, '', NULL, 1, 'Davtashen', 'Давташен', 'Դավթաշեն', '2019-03-25', '2019-03-25'),
(5, '', NULL, 1, 'Erebuni', 'Эребуни', 'Էրեբունի', '2019-03-25', '2019-03-25'),
(6, '', NULL, 1, 'Kanaker-Zeytun', 'Канакер-Зейтун', 'Քանաքեռ-Զեյթուն', '2019-03-25', '2019-03-25'),
(7, '', NULL, 1, 'Kentron', 'Центр', 'Կենտրոն', '2019-03-25', '2019-03-25'),
(8, '', NULL, 1, 'Malatia-Sebastia', 'Малатия-Себастия', 'Մալաթիա-Սեբաստիա', '2019-03-25', '2019-03-25'),
(9, '', NULL, 1, 'Nork-Marash', 'Норк-Мараш', 'Նորք-Մարաշ', '2019-03-25', '2019-03-25'),
(10, '', NULL, 1, 'Nor Nork', 'Нор Норк', 'Նոր Նորք', '2019-03-25', '2019-03-25'),
(11, '', NULL, 1, 'Nubarashen', 'Нубарашен', 'Նուբարաշեն', '2019-03-25', '2019-03-25'),
(12, '', NULL, 1, 'Shengavit', 'Шенгавит', 'Շենգավիթ', '2019-03-25', '2019-03-25');

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2019-01-30 11:40:50.627478', '8', 'About object (8)', 1, '[{\"added\": {}}]', 12, 1),
(2, '2019-01-30 12:24:40.350503', '8', 'About object (8)', 2, '[]', 12, 1),
(3, '2019-01-30 12:25:21.815680', '8', 'About object (8)', 2, '[]', 12, 1),
(4, '2019-01-30 12:34:16.856655', '8', 'About object (8)', 2, '[{\"changed\": {\"fields\": [\"title_ru\", \"content_ru\", \"title_en\", \"content_en\", \"title_hy\", \"content_hy\"]}}]', 12, 1),
(5, '2019-01-30 12:35:29.834089', '8', 'OUR HISTORY', 2, '[]', 12, 1),
(6, '2019-01-30 12:35:43.848296', '8', 'About object (8)', 2, '[]', 12, 1),
(7, '2019-01-30 12:37:16.508517', '8', 'About object (8)', 2, '[]', 12, 1),
(8, '2019-01-30 12:38:01.615617', '8', 'OUR HISTORY', 2, '[]', 12, 1),
(9, '2019-01-30 12:39:12.874837', '8', 'OUR HISTORY', 2, '[]', 12, 1),
(10, '2019-01-30 12:45:31.271273', '9', 'OUR HISTORY', 1, '[{\"added\": {}}]', 12, 1),
(11, '2019-01-30 12:51:07.192166', '8', 'OUR HISTORY', 3, '', 12, 1),
(12, '2019-01-30 14:05:26.802599', '55', 'Խաչապուրի, կարկանդակ', 2, '[{\"changed\": {\"fields\": [\"slug\"]}}]', 3, 1),
(13, '2019-02-01 13:05:18.968720', '437', 'Post object (437)', 2, '[{\"changed\": {\"fields\": [\"popular\", \"content_ru\", \"content_en\", \"content_hy\"]}}]', 4, 1),
(14, '2019-02-01 13:05:24.410823', '435', 'Post object (435)', 2, '[{\"changed\": {\"fields\": [\"popular\", \"content_ru\", \"content_en\", \"content_hy\"]}}]', 4, 1),
(15, '2019-02-01 13:05:28.915712', '433', 'Post object (433)', 2, '[{\"changed\": {\"fields\": [\"popular\", \"content_ru\", \"content_en\", \"content_hy\"]}}]', 4, 1),
(16, '2019-02-01 13:05:33.200070', '432', 'Post object (432)', 2, '[{\"changed\": {\"fields\": [\"popular\", \"content_ru\", \"content_en\", \"content_hy\"]}}]', 4, 1),
(17, '2019-02-01 13:09:22.718342', '436', 'Post object (436)', 2, '[{\"changed\": {\"fields\": [\"popular\", \"content_ru\", \"content_en\", \"content_hy\"]}}]', 4, 1),
(18, '2019-02-01 13:10:04.586903', '506', 'Post object (506)', 2, '[{\"changed\": {\"fields\": [\"popular\", \"title_ru\", \"content_ru\", \"content_en\", \"title_hy\", \"content_hy\"]}}]', 4, 1),
(19, '2019-02-01 13:10:18.729893', '510', 'Post object (510)', 2, '[{\"changed\": {\"fields\": [\"title_ru\", \"content_ru\", \"content_en\", \"title_hy\", \"content_hy\"]}}]', 4, 1),
(20, '2019-02-01 20:48:11.003574', '562', 'Post object (562)', 2, '[{\"changed\": {\"fields\": [\"popular\", \"content_ru\", \"content_en\", \"content_hy\"]}}]', 4, 1),
(21, '2019-02-01 20:48:15.513493', '559', 'Post object (559)', 2, '[{\"changed\": {\"fields\": [\"popular\", \"content_ru\", \"content_en\", \"content_hy\"]}}]', 4, 1),
(22, '2019-02-01 20:48:20.386953', '557', 'Post object (557)', 2, '[{\"changed\": {\"fields\": [\"popular\", \"content_ru\", \"content_en\", \"content_hy\"]}}]', 4, 1),
(23, '2019-02-01 20:48:44.496612', '632', 'Post object (632)', 2, '[{\"changed\": {\"fields\": [\"popular\", \"content_ru\", \"content_en\", \"content_hy\"]}}]', 4, 1),
(24, '2019-02-01 20:48:47.741500', '628', 'Post object (628)', 2, '[{\"changed\": {\"fields\": [\"popular\", \"content_ru\", \"content_en\", \"content_hy\"]}}]', 4, 1),
(25, '2019-02-01 20:48:51.264897', '626', 'Post object (626)', 2, '[{\"changed\": {\"fields\": [\"popular\", \"content_ru\", \"content_en\", \"content_hy\"]}}]', 4, 1),
(26, '2019-02-01 20:49:41.917184', '626', 'Post object (626)', 2, '[{\"changed\": {\"fields\": [\"popular\"]}}]', 4, 1),
(27, '2019-02-03 17:55:53.492302', '9', 'OUR HISTORY', 2, '[]', 12, 1),
(28, '2019-02-03 18:09:36.789589', '9', 'OUR HISTORY', 2, '[]', 12, 1),
(29, '2019-02-07 17:53:11.417168', '1', 'Slider object (1)', 1, '[{\"added\": {}}]', 13, 1),
(30, '2019-02-07 17:53:17.539581', '2', 'Slider object (2)', 1, '[{\"added\": {}}]', 13, 1),
(31, '2019-02-07 17:53:22.796039', '3', 'Slider object (3)', 1, '[{\"added\": {}}]', 13, 1),
(32, '2019-04-03 15:16:45.961814', '33', 'Order object (33)', 2, '[{\"changed\": {\"fields\": [\"show\"]}}]', 16, 1),
(33, '2019-05-02 14:37:31.573521', '4', 'Slider object (4)', 1, '[{\"added\": {}}]', 13, 1),
(34, '2019-05-02 14:46:45.439767', '4', 'slider/56242811_2317444135170209_1787197146689175552_n.jpg', 3, '', 13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(6, 'admin', 'logentry'),
(8, 'auth', 'group'),
(7, 'auth', 'permission'),
(9, 'auth', 'user'),
(12, 'blog', 'about'),
(1, 'blog', 'contact'),
(15, 'blog', 'district'),
(2, 'blog', 'images'),
(13, 'blog', 'slider'),
(16, 'cart', 'order'),
(17, 'cart', 'orderbilling'),
(18, 'cart', 'orderproduct'),
(3, 'category', 'category'),
(10, 'contenttypes', 'contenttype'),
(19, 'personal', 'post'),
(4, 'post', 'post'),
(11, 'sessions', 'session'),
(14, 'users', 'billing'),
(5, 'users', 'profile');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2019-01-29 11:06:10.104235'),
(2, 'auth', '0001_initial', '2019-01-29 11:06:18.893325'),
(3, 'admin', '0001_initial', '2019-01-29 11:06:21.091160'),
(4, 'admin', '0002_logentry_remove_auto_add', '2019-01-29 11:06:21.137787'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2019-01-29 11:06:21.195641'),
(6, 'contenttypes', '0002_remove_content_type_name', '2019-01-29 11:06:22.247177'),
(7, 'auth', '0002_alter_permission_name_max_length', '2019-01-29 11:06:23.181946'),
(8, 'auth', '0003_alter_user_email_max_length', '2019-01-29 11:06:24.448755'),
(9, 'auth', '0004_alter_user_username_opts', '2019-01-29 11:06:24.515829'),
(10, 'auth', '0005_alter_user_last_login_null', '2019-01-29 11:06:25.083134'),
(11, 'auth', '0006_require_contenttypes_0002', '2019-01-29 11:06:25.149412'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2019-01-29 11:06:25.236391'),
(13, 'auth', '0008_alter_user_username_max_length', '2019-01-29 11:06:26.625640'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2019-01-29 11:06:27.536358'),
(15, 'blog', '0001_initial', '2019-01-29 11:06:28.938206'),
(16, 'category', '0001_initial', '2019-01-29 11:06:33.483194'),
(17, 'post', '0001_initial', '2019-01-29 11:06:35.459823'),
(18, 'sessions', '0001_initial', '2019-01-29 11:06:36.207260'),
(19, 'users', '0001_initial', '2019-01-29 11:06:37.415664'),
(20, 'post', '0002_auto_20190129_0427', '2019-01-29 12:28:11.329653'),
(21, 'post', '0003_auto_20190129_0429', '2019-01-29 12:29:55.838355'),
(22, 'post', '0004_auto_20190129_0456', '2019-01-29 12:56:33.083846'),
(23, 'post', '0005_auto_20190129_0458', '2019-01-29 12:58:36.431305'),
(24, 'post', '0006_auto_20190129_0505', '2019-01-29 13:05:51.233395'),
(25, 'post', '0007_auto_20190129_0508', '2019-01-29 13:08:42.600576'),
(26, 'post', '0008_post_price', '2019-01-29 13:31:24.306168'),
(27, 'post', '0009_auto_20190129_0542', '2019-01-29 13:42:28.284775'),
(28, 'post', '0010_post_order', '2019-01-29 13:42:29.107572'),
(29, 'blog', '0002_about', '2019-01-30 11:25:00.805334'),
(30, 'blog', '0003_auto_20190130_0326', '2019-01-30 11:26:35.848975'),
(31, 'blog', '0004_auto_20190130_0433', '2019-01-30 12:33:33.896086'),
(32, 'blog', '0005_auto_20190130_1226', '2019-01-30 20:27:21.787045'),
(33, 'post', '0011_post_popular', '2019-02-01 13:01:04.992341'),
(34, 'post', '0012_auto_20190201_0504', '2019-02-01 13:04:33.915375'),
(35, 'post', '0013_auto_20190201_0507', '2019-02-01 13:07:52.961088'),
(36, 'blog', '0006_slider', '2019-02-07 17:48:51.680039'),
(37, 'blog', '0007_auto_20190207_1019', '2019-02-07 18:20:02.545999'),
(38, 'users', '0002_billing', '2019-03-23 08:45:25.775752'),
(39, 'users', '0003_auto_20190323_1300', '2019-03-23 09:00:17.336955'),
(40, 'users', '0004_auto_20190323_2344', '2019-03-23 19:44:49.176177'),
(41, 'blog', '0008_districts', '2019-03-24 12:01:15.583163'),
(42, 'blog', '0009_auto_20190324_1601', '2019-03-24 12:01:56.088198'),
(43, 'users', '0005_auto_20190325_2235', '2019-03-25 18:35:44.260312'),
(44, 'cart', '0001_initial', '2019-03-26 18:19:39.929379'),
(45, 'users', '0006_auto_20190326_2216', '2019-03-26 18:19:39.957809'),
(46, 'cart', '0002_auto_20190326_2307', '2019-03-26 19:08:02.654432'),
(47, 'auth', '0010_auto_20190328_1743', '2019-03-28 13:46:04.843162'),
(48, 'personal', '0001_initial', '2019-03-28 13:46:04.885191'),
(49, 'auth', '0011_auto_20190328_1748', '2019-03-30 10:32:43.231245'),
(50, 'auth', '0012_auto_20190330_1430', '2019-03-30 10:32:43.367244'),
(51, 'users', '0007_auto_20190330_1430', '2019-03-30 10:32:43.500325'),
(52, 'auth', '0013_auto_20190401_1051', '2019-04-06 10:38:37.082258'),
(53, 'auth', '0014_auto_20190401_1220', '2019-04-06 10:38:37.218421'),
(54, 'cart', '0003_auto_20190406_1438', '2019-04-06 10:47:19.116996'),
(55, 'cart', '0004_auto_20190406_1438', '2019-04-06 10:47:45.450610'),
(56, 'cart', '0005_auto_20190406_1446', '2019-04-06 10:47:45.525177'),
(57, 'cart', '0006_auto_20190406_1446', '2019-04-06 10:47:45.614713'),
(58, 'users', '0008_auto_20190406_1438', '2019-04-06 10:47:45.780107'),
(59, 'cart', '0007_auto_20190406_2311', '2019-04-06 19:11:14.884961');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('1jkexdz0ks81b7pi9zqiwrbciv4t19uw', 'OGY5ZWU5ZmZiY2MyM2ZjYzczN2ZkZDkwMzVmM2RhZTU4NTVlZGFjODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YWU2Y2ZiOGRlODhkNDkwYTc2MWUyYjM0YTMxMWUxZDAwNGM4MzY5In0=', '2019-04-17 06:46:10.943931'),
('2z7m01aj1rwnjb1ddhooxsp85b7gb52w', 'NTg4NGYyNDhjYTQwMTYwMDU2MDcwOWEzZjlmYjgwZDg0ZjhhN2ZlYzp7ImRhdGFiYXNrZXRzZXNzaW9uIjp7IjY4OSI6W3sicXVhbnRpdHkiOjIsInByb2R1Y3RJZCI6Njg5LCJwcmljZSI6NDgwMCwibmFtZSI6eyJoeSI6Ilx1MDU0Zlx1MDU2MVx1MDU3ZVx1MDU2MVx1MDU4MFx1MDU2YiBcdTA1N2ZcdTA1NjFcdTA1N2FcdTA1NjFcdTA1NmZcdTA1NjFcdTA1NmUgXHUwNTdhXHUwNTc4XHUwNTc5IiwicnUiOiJcdTA0MTZcdTA0MzBcdTA0NDBcdTA0MzVcdTA0M2RcdTA0NGJcdTA0MzkgXHUwNDMzXHUwNDNlXHUwNDMyXHUwNDRmXHUwNDM2XHUwNDM4XHUwNDM5IFx1MDQ0NVx1MDQzZVx1MDQzMlx1MDQ0MVx1MDQ0MiIsImVuIjoiRnJpZWQgYmVlZiB0YWlsIn0sImltYWdlIjoiL21lZGlhL2NhdGVnb3J5L2U0NTFjNDllYjQyYjA0OTQ5NGU5ZTc5MmRmYmU3OGZhLmpwZyIsInNsdWciOiJmcmllZC1iZWVmLXRhaWwifV0sImRhdGEiOlt7InF1YW50aXR5IjoyLCJwcmljZSI6NDgwMH1dfX0=', '2019-03-03 19:51:15.726017'),
('3p8e70mzjt9zv3nnyqsgflp0m3cyrlw3', 'NTc2MDNhMTlmY2E3NzA0Njc5NTdkMmNmODhjMGZiNjcwOTczODJmYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YWU2Y2ZiOGRlODhkNDkwYTc2MWUyYjM0YTMxMWUxZDAwNGM4MzY5IiwiX2xhbmd1YWdlIjoiaHkifQ==', '2019-02-17 18:13:08.266730'),
('52zyc2ufowkzl7o4eoxqx66rjnki0i4o', 'OGY5ZWU5ZmZiY2MyM2ZjYzczN2ZkZDkwMzVmM2RhZTU4NTVlZGFjODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YWU2Y2ZiOGRlODhkNDkwYTc2MWUyYjM0YTMxMWUxZDAwNGM4MzY5In0=', '2019-02-24 13:18:47.005509'),
('6cm5vt90ipou0mygum2qud04cmdm811h', 'ODMzM2JjZTE1ODE0NTg5NDE5ZjZiMDZhNWY0ZTQzZGIzMTA1YTAyMzp7ImRhdGFiYXNrZXRzZXNzaW9uIjp7IjcxMyI6W3sicXVhbnRpdHkiOjIsInByb2R1Y3RJZCI6NzEzLCJwcmljZSI6MTcwMCwibmFtZSI6eyJoeSI6Ilx1MDU0MVx1MDU3ZVx1MDU2MVx1MDU2ZVx1MDU2NVx1MDU3MiBcdTA1NmRcdTA1NzhcdTA1NjZcdTA1NjFcdTA1N2FcdTA1NzhcdTA1ODJcdTA1NmRcdTA1N2ZcdTA1NzhcdTA1N2UiLCJydSI6Ilx1MDQyZlx1MDQzOFx1MDQ0N1x1MDQzZFx1MDQzOFx1MDQ0Nlx1MDQzMCBcdTA0NDEgXHUwNDMyXHUwNDM1XHUwNDQyXHUwNDQ3XHUwNDM4XHUwNDNkXHUwNDNlXHUwNDM5IiwiZW4iOiJGcmllZCBFZ2dzIHdpdGggaGFtIn0sImltYWdlIjoiL21lZGlhL2NhdGVnb3J5LzdmOTg1YmUwNDk2YWYxMTZmOTU1N2E4Yzg5OTJiMGQzLmpwZyIsInNsdWciOiJmcmllZC1lZ2dzLXdpdGgtaGFtIn1dLCJkYXRhIjpbeyJxdWFudGl0eSI6MjcsInByaWNlIjo2MTcwMH1dLCI2ODkiOlt7InF1YW50aXR5IjoyNSwicHJvZHVjdElkIjo2ODksInByaWNlIjo2MDAwMCwibmFtZSI6eyJoeSI6Ilx1MDU0Zlx1MDU2MVx1MDU3ZVx1MDU2MVx1MDU4MFx1MDU2YiBcdTA1N2ZcdTA1NjFcdTA1N2FcdTA1NjFcdTA1NmZcdTA1NjFcdTA1NmUgXHUwNTdhXHUwNTc4XHUwNTc5IiwicnUiOiJcdTA0MTZcdTA0MzBcdTA0NDBcdTA0MzVcdTA0M2RcdTA0NGJcdTA0MzkgXHUwNDMzXHUwNDNlXHUwNDMyXHUwNDRmXHUwNDM2XHUwNDM4XHUwNDM5IFx1MDQ0NVx1MDQzZVx1MDQzMlx1MDQ0MVx1MDQ0MiIsImVuIjoiRnJpZWQgYmVlZiB0YWlsIn0sImltYWdlIjoiL21lZGlhL2NhdGVnb3J5L2U0NTFjNDllYjQyYjA0OTQ5NGU5ZTc5MmRmYmU3OGZhLmpwZyIsInNsdWciOiJmcmllZC1iZWVmLXRhaWwifV19fQ==', '2019-03-03 19:50:41.529794'),
('6xu99zvkbajpdhuj8i4vgyevoxwj09qk', 'Y2IzNzk1MmY3NDJlOTAxNTFhNWNjYTBjMTlhYmQ3NTA4M2FhY2E2Nzp7ImJhc2tldCI6eyI2MTUiOlt7InF1YW50aXR5Ijo3OCwicHJvZHVjdElkIjo2MTUsInByaWNlIjo1MzgyMDB9XSwiNzEwIjpbeyJxdWFudGl0eSI6MTU0LCJwcm9kdWN0SWQiOjcxMCwicHJpY2UiOjEyMzIwMH1dLCIyIjpbeyJxdWFudGl0eSI6NjE1LCJwcm9kdWN0SWQiOjIsInByaWNlIjo0MjQzNTAwfV0sIjMiOlt7InF1YW50aXR5Ijo2MTUsInByb2R1Y3RJZCI6MywicHJpY2UiOjQyNDM1MDB9XX0sImJhc2tldDMiOnsiNjE1IjpbeyJxdWFudGl0eSI6MiwicHJvZHVjdElkIjo2MTUsInByaWNlIjoxMzgwMH1dLCI3MTAiOlt7InF1YW50aXR5Ijo0LCJwcm9kdWN0SWQiOjcxMCwicHJpY2UiOjMyMDB9XX0sImJhc2tldDQiOnsiNzEwIjpbeyJxdWFudGl0eSI6NywicHJvZHVjdElkIjo3MTAsInByaWNlIjo1NjAwfV0sImRhdGEiOlt7InF1YW50aXR5IjozLCJwcmljZSI6ODAwfV19LCJiYXNrZXQ1Ijp7IjcxMCI6W3sicXVhbnRpdHkiOjUsInByb2R1Y3RJZCI6NzEwLCJwcmljZSI6NDAwMH1dLCJkYXRhIjpbeyJxdWFudGl0eSI6MiwicHJpY2UiOjE2MDB9XX0sImJhc2tldDYiOnsiNzEwIjpbeyJxdWFudGl0eSI6MywicHJvZHVjdElkIjo3MTAsInByaWNlIjoyNDAwfV0sImRhdGEiOlt7InF1YW50aXR5Ijo0LCJwcmljZSI6OTMwMH1dLCI2MTUiOlt7InF1YW50aXR5IjozLCJwcm9kdWN0SWQiOjYxNSwicHJpY2UiOjIwNzAwfV19LCJiYXNrZXQ4Ijp7IjYxNSI6W3sicXVhbnRpdHkiOjEzLCJwcm9kdWN0SWQiOjYxNSwicHJpY2UiOjg5NzAwfV0sImRhdGEiOlt7InF1YW50aXR5IjoyNiwicHJpY2UiOjEwODUwMH1dLCI3MTAiOlt7InF1YW50aXR5Ijo5LCJwcm9kdWN0SWQiOjcxMCwicHJpY2UiOjcyMDB9XSwiNjIzIjpbeyJxdWFudGl0eSI6NCwicHJvZHVjdElkIjo2MjMsInByaWNlIjoxMTYwMH1dfSwiYmFza2V0X2RhdGEiOnsiNjIzIjpbeyJxdWFudGl0eSI6NDQsInByb2R1Y3RJZCI6NjIzLCJwcmljZSI6MTI3NjAwfV0sImRhdGEiOlt7InF1YW50aXR5Ijo0NCwicHJpY2UiOjEyNzYwMH1dfSwiYmFza2V0X2RhdGE5Ijp7Ijc2MiI6W3sicXVhbnRpdHkiOjMsInByb2R1Y3RJZCI6NzYyLCJwcmljZSI6NjAwMH1dLCJkYXRhIjpbeyJxdWFudGl0eSI6MywicHJpY2UiOjYwMDB9XX0sImJhc2tldF9kYXRhMTEiOnsiNzYyIjpbeyJxdWFudGl0eSI6MiwicHJvZHVjdElkIjo3NjIsInByaWNlIjo0MDAwLCJuYW1lIjoiPGJvdW5kIG1ldGhvZCBQb3N0LnRpdGxlIG9mIDxQb3N0OiBQb3N0IG9iamVjdCAoNzYyKT4+In1dLCJkYXRhIjpbeyJxdWFudGl0eSI6MiwicHJpY2UiOjQwMDB9XX0sImJhc2tldF9kYXRhMTI2Ijp7IjYyMyI6W3sicXVhbnRpdHkiOjI2LCJwcm9kdWN0SWQiOjYyMywicHJpY2UiOjc1NDAwLCJuYW1lIjoiPGJvdW5kIG1ldGhvZCBQb3N0LnRpdGxlIG9mIDxQb3N0OiBQb3N0IG9iamVjdCAoNjIzKT4+In1dLCJkYXRhIjpbeyJxdWFudGl0eSI6MjYsInByaWNlIjo3NTQwMH1dfSwiYmFza2V0X2RhdGExMjI2MjIiOnsiNjIzIjpbeyJxdWFudGl0eSI6ODgsInByb2R1Y3RJZCI6NjIzLCJwcmljZSI6MjU1MjAwLCJuYW1lIjoiPGJvdW5kIG1ldGhvZCBQb3N0LnRpdGxlIG9mIDxQb3N0OiBQb3N0IG9iamVjdCAoNjIzKT4+IiwiaW1hZ2UiOiIvbWVkaWEvY2F0ZWdvcnkvYjkwNjhlMDVkOTNlYzk0MzFiYjZlOTg0Yjc1MTUxZWIuanBnIn1dLCJkYXRhIjpbeyJxdWFudGl0eSI6ODgsInByaWNlIjoyNTUyMDB9XX0sImJhc2tldF9kYXRhMTIyNjIyMyI6eyI2MjMiOlt7InF1YW50aXR5IjoyLCJwcm9kdWN0SWQiOjYyMywicHJpY2UiOjU4MDAsIm5hbWUiOiI8Ym91bmQgbWV0aG9kIFBvc3QudGl0bGUgb2YgPFBvc3Q6IFBvc3Qgb2JqZWN0ICg2MjMpPj4ifV0sImRhdGEiOlt7InF1YW50aXR5IjoyLCJwcmljZSI6NTgwMH1dfSwiYmFza2V0X2RhdGExMjI2MjIzMyI6eyI2MjMiOlt7InF1YW50aXR5IjozLCJwcm9kdWN0SWQiOjYyMywicHJpY2UiOjg3MDAsIm5hbWUiOiI8Ym91bmQgbWV0aG9kIFBvc3QudGl0bGUgb2YgPFBvc3Q6IFBvc3Qgb2JqZWN0ICg2MjMpPj4iLCJpbWFnZSI6Ii9tZWRpYS9jYXRlZ29yeS9iOTA2OGUwNWQ5M2VjOTQzMWJiNmU5ODRiNzUxNTFlYi5qcGcifV0sImRhdGEiOlt7InF1YW50aXR5IjozLCJwcmljZSI6ODcwMH1dfSwiYmFza2V0X2RhdGExMjI2MjIyIjp7IjYyMyI6W3sicXVhbnRpdHkiOjMzLCJwcm9kdWN0SWQiOjYyMywicHJpY2UiOjk1NzAwLCJuYW1lIjoiPGJvdW5kIG1ldGhvZCBQb3N0LnRpdGxlIG9mIDxQb3N0OiBQb3N0IG9iamVjdCAoNjIzKT4+IiwiaW1hZ2UiOiIvbWVkaWEvY2F0ZWdvcnkvYjkwNjhlMDVkOTNlYzk0MzFiYjZlOTg0Yjc1MTUxZWIuanBnIn1dLCJkYXRhIjpbeyJxdWFudGl0eSI6MzYsInByaWNlIjoxMDE3MDB9XSwiNzYyIjpbeyJxdWFudGl0eSI6MywicHJvZHVjdElkIjo3NjIsInByaWNlIjo2MDAwLCJuYW1lIjoiPGJvdW5kIG1ldGhvZCBQb3N0LnRpdGxlIG9mIDxQb3N0OiBQb3N0IG9iamVjdCAoNzYyKT4+IiwiaW1hZ2UiOiIvbWVkaWEvY2F0ZWdvcnkvY2QwZThmNThlMDhmMmQyYjAyMWU1N2I1Y2U5MmY0OWUuanBnIn1dfSwiZGF0YV9iYXNrZXQiOnsiNzYyIjpbeyJxdWFudGl0eSI6MzMsInByb2R1Y3RJZCI6NzYyLCJwcmljZSI6NjYwMDAsIm5hbWUiOiI8Ym91bmQgbWV0aG9kIFBvc3QudGl0bGUgb2YgPFBvc3Q6IFBvc3Qgb2JqZWN0ICg3NjIpPj4iLCJpbWFnZSI6Ii9tZWRpYS9jYXRlZ29yeS9jZDBlOGY1OGUwOGYyZDJiMDIxZTU3YjVjZTkyZjQ5ZS5qcGcifV0sImRhdGEiOlt7InF1YW50aXR5Ijo0MCwicHJpY2UiOjg0NjUwfV0sIjYyMyI6W3sicXVhbnRpdHkiOjQsInByb2R1Y3RJZCI6NjIzLCJwcmljZSI6MTE2MDAsIm5hbWUiOiI8Ym91bmQgbWV0aG9kIFBvc3QudGl0bGUgb2YgPFBvc3Q6IFBvc3Qgb2JqZWN0ICg2MjMpPj4iLCJpbWFnZSI6Ii9tZWRpYS9jYXRlZ29yeS9iOTA2OGUwNWQ5M2VjOTQzMWJiNmU5ODRiNzUxNTFlYi5qcGcifV0sIjYyNSI6W3sicXVhbnRpdHkiOjMsInByb2R1Y3RJZCI6NjI1LCJwcmljZSI6NzA1MCwibmFtZSI6IlBpenphIEFzc29ydGksIDMwY20iLCJpbWFnZSI6Ii9tZWRpYS9jYXRlZ29yeS9iOGE1NjBiZGJiN2JkNzBhY2IyMWUwZDcxNTVmMGYyYi5qcGcifV19LCJkYXRhYmFza2V0Ijp7IjcxMiI6W3sicXVhbnRpdHkiOjIsInByb2R1Y3RJZCI6NzEyLCJwcmljZSI6MTkwMCwibmFtZSI6IkZyaWVkIEVnZ3Mgd2l0aCBiYXN0dXJtYSIsImltYWdlIjoiL21lZGlhL2NhdGVnb3J5LzkxMzM5M2VhYmRkZDQyYTM4YmIxMmI1ODAxYTQ3NDllLmpwZyJ9XSwiZGF0YSI6W3sicXVhbnRpdHkiOjcsInByaWNlIjoxMDEwMH1dLCI3MDkiOlt7InF1YW50aXR5IjoyLCJwcm9kdWN0SWQiOjcwOSwicHJpY2UiOjEwMDAsIm5hbWUiOiJGcmllZCBFZ2dzIiwiaW1hZ2UiOiIvbWVkaWEvY2F0ZWdvcnkvOWQ2M2MxNzIzYTJlMjk2OGI3N2ExZjkyMjc2N2ExZGEuanBnIn1dLCI2ODkiOlt7InF1YW50aXR5IjozLCJwcm9kdWN0SWQiOjY4OSwicHJpY2UiOjcyMDAsIm5hbWUiOiJcdTA1NGZcdTA1NjFcdTA1N2VcdTA1NjFcdTA1ODBcdTA1NmIgXHUwNTdmXHUwNTYxXHUwNTdhXHUwNTYxXHUwNTZmXHUwNTYxXHUwNTZlIFx1MDU3YVx1MDU3OFx1MDU3OSIsImltYWdlIjoiL21lZGlhL2NhdGVnb3J5L2U0NTFjNDllYjQyYjA0OTQ5NGU5ZTc5MmRmYmU3OGZhLmpwZyJ9XX0sIl9sYW5ndWFnZSI6ImVuIiwiZGF0YV9iYXNrZXRfIjp7IjY4OSI6W3sicXVhbnRpdHkiOjIsInByb2R1Y3RJZCI6Njg5LCJwcmljZSI6NDgwMCwibmFtZSI6eyJoeSI6Ilx1MDU0Zlx1MDU2MVx1MDU3ZVx1MDU2MVx1MDU4MFx1MDU2YiBcdTA1N2ZcdTA1NjFcdTA1N2FcdTA1NjFcdTA1NmZcdTA1NjFcdTA1NmUgXHUwNTdhXHUwNTc4XHUwNTc5IiwicnUiOiJcdTA0MTZcdTA0MzBcdTA0NDBcdTA0MzVcdTA0M2RcdTA0NGJcdTA0MzkgXHUwNDMzXHUwNDNlXHUwNDMyXHUwNDRmXHUwNDM2XHUwNDM4XHUwNDM5IFx1MDQ0NVx1MDQzZVx1MDQzMlx1MDQ0MVx1MDQ0MiIsImVuIjoiRnJpZWQgYmVlZiB0YWlsIn0sImltYWdlIjoiL21lZGlhL2NhdGVnb3J5L2U0NTFjNDllYjQyYjA0OTQ5NGU5ZTc5MmRmYmU3OGZhLmpwZyJ9XSwiZGF0YSI6W3sicXVhbnRpdHkiOjgsInByaWNlIjoxNDAwMH1dLCI2OTIiOlt7InF1YW50aXR5IjoyLCJwcm9kdWN0SWQiOjY5MiwicHJpY2UiOjQ4MDAsIm5hbWUiOnsiaHkiOiJcdTA1MzNcdTA1NjFcdTA1N2NcdTA1NjFcdTA1NzYgXHUwNTZkXHUwNTYxXHUwNTc3XHUwNTZjXHUwNTYxXHUwNTc0XHUwNTYxIiwicnUiOiJcdTA0MjVcdTA0MzBcdTA0NDhcdTA0M2JcdTA0MzBcdTA0M2NcdTA0MzAgXHUwNDM4XHUwNDM3IFx1MDQzMVx1MDQzMFx1MDQ0MFx1MDQzMFx1MDQzZFx1MDQzOFx1MDQzZFx1MDQ0YiIsImVuIjoiTGFtYiBraGFzaGxhbWEifSwiaW1hZ2UiOiIvbWVkaWEvY2F0ZWdvcnkvMmJkZTlhYmE3YWRhYWE5MmUwZmU0YTFkYWE5NzI5MTAuanBnIn1dLCI2OTAiOlt7InF1YW50aXR5Ijo0LCJwcm9kdWN0SWQiOjY5MCwicHJpY2UiOjQ0MDAsIm5hbWUiOnsiaHkiOiJcdTA1NGZcdTA1NzhcdTA1NmNcdTA1NzRcdTA1NjEgXHUwNTY5XHUwNTgzXHUwNTc4XHUwNTdlIiwicnUiOiJcdTA0MTRcdTA0M2VcdTA0M2JcdTA0M2NcdTA0MzAgXHUwNDM4XHUwNDM3IFx1MDQzMlx1MDQzOFx1MDQzZFx1MDQzZVx1MDQzM1x1MDQ0MFx1MDQzMFx1MDQzNFx1MDQzZFx1MDQ0Ylx1MDQ0NSBcdTA0M2JcdTA0MzhcdTA0NDFcdTA0NDJcdTA0NGNcdTA0MzVcdTA0MzIiLCJlbiI6IkRvbG1hIHdpdGggZ3JhcGUgbGVhdmVzIn0sImltYWdlIjoiL21lZGlhL2NhdGVnb3J5L2IxNGJkYTEzMDMxYzYyMDlmZWUyMTBkNzY4MzlkZDc3LmpwZyJ9XX0sImRhdGFiYXNrZXRzZXNzaW9uIjp7IjY5MCI6W3sicXVhbnRpdHkiOjIsInByb2R1Y3RJZCI6NjkwLCJwcmljZSI6MjIwMCwibmFtZSI6eyJoeSI6Ilx1MDU0Zlx1MDU3OFx1MDU2Y1x1MDU3NFx1MDU2MSBcdTA1NjlcdTA1ODNcdTA1NzhcdTA1N2UiLCJydSI6Ilx1MDQxNFx1MDQzZVx1MDQzYlx1MDQzY1x1MDQzMCBcdTA0MzhcdTA0MzcgXHUwNDMyXHUwNDM4XHUwNDNkXHUwNDNlXHUwNDMzXHUwNDQwXHUwNDMwXHUwNDM0XHUwNDNkXHUwNDRiXHUwNDQ1IFx1MDQzYlx1MDQzOFx1MDQ0MVx1MDQ0Mlx1MDQ0Y1x1MDQzNVx1MDQzMiIsImVuIjoiRG9sbWEgd2l0aCBncmFwZSBsZWF2ZXMifSwiaW1hZ2UiOiIvbWVkaWEvY2F0ZWdvcnkvYjE0YmRhMTMwMzFjNjIwOWZlZTIxMGQ3NjgzOWRkNzcuanBnIiwic2x1ZyI6ImRvbG1hLXdpdGgtZ3JhcGUtbGVhdmVzIn1dLCI2MzIiOlt7InF1YW50aXR5IjozLCJwcm9kdWN0SWQiOjYzMiwicHJpY2UiOjUyNTAsIm5hbWUiOnsiaHkiOiJcdTA1NGFcdTA1NmJcdTA1ODFcdTA1ODFcdTA1NjEgXHUwNTQ5XHUwNTZiXHUwNTZmXHUwNTc2IFx1MDU0NFx1MDU2MVx1MDU3Y1x1MDU2Y1x1MDU2NVx1MDU3NSwgMjRcdTA1N2RcdTA1NzQiLCJydSI6Ilx1MDQxZlx1MDQzOFx1MDQ0Nlx1MDQ0Nlx1MDQzMCBcdTA0MjdcdTA0MzhcdTA0M2FcdTA0M2QgXHUwNDFjXHUwNDMwXHUwNDQwXHUwNDNiXHUwNDM1XHUwNDM5LCAyNFx1MDQ0MVx1MDQzYyIsImVuIjoiUGl6emEgQ2hpY2tlbiBNYXJsZXksIDI0Y20ifSwiaW1hZ2UiOiIvbWVkaWEvY2F0ZWdvcnkvZjFmZWQ4YjA4MWM0MmM1MmMyNWFlZWMwM2M3NDQ3NmEuanBnIiwic2x1ZyI6InBpenphLWNoaWNrZW4tbWFybGV5LTI0Y20ifV0sIjU1OSI6W3sicXVhbnRpdHkiOjQsInByb2R1Y3RJZCI6NTU5LCJwcmljZSI6MzYwMCwibmFtZSI6eyJoeSI6Ilx1MDUzN1x1MDU4NFx1MDU3ZFx1MDU3Zlx1MDU4MFx1MDU2MSBcdTA1ODRcdTA1NzVcdTA1NjFcdTA1NjJcdTA1NjFcdTA1NjIgXHUwNTcwXHUwNTYxXHUwNTdlXHUwNTZiIiwicnUiOiJcdTA0MWFcdTA0NDNcdTA0NDBcdTA0MzhcdTA0M2RcdTA0NGJcdTA0MzkgXHUwNDRkXHUwNDNhXHUwNDQxXHUwNDQyXHUwNDQwXHUwNDMwIFx1MDQzYVx1MDQzNVx1MDQzMVx1MDQzMFx1MDQzMSIsImVuIjoiQ2hpY2tlbiBleHRyYSBrZWJhYiJ9LCJpbWFnZSI6Ii9tZWRpYS9jYXRlZ29yeS8yZDk4MmVmM2FkMjg2MGNlZTg0MDJiZDA3ODE0YjZjZi5qcGciLCJzbHVnIjoiY2hpY2tlbi1leHRyYS1rZWJhYiJ9XSwiNTYyIjpbeyJxdWFudGl0eSI6NCwicHJvZHVjdElkIjo1NjIsInByaWNlIjoyMjAwLCJuYW1lIjp7Imh5IjoiXHUwNTNkXHUwNTc4XHUwNTgwXHUwNTc4XHUwNTdlXHUwNTYxXHUwNTZlIFx1MDU2Zlx1MDU2MVx1MDU4MFx1MDU3Zlx1MDU3OFx1MDU4Nlx1MDU2Ylx1MDU2YyBcdTA1N2RcdTA1NjFcdTA1NmNcdTA1NzhcdTA1N2UiLCJydSI6Ilx1MDQxYVx1MDQzMFx1MDQ0MFx1MDQ0Mlx1MDQzZVx1MDQ0NFx1MDQzNVx1MDQzYlx1MDQ0YyBcdTA0NDEgXHUwNDQxXHUwNDMwXHUwNDNiXHUwNDNlXHUwNDNjIiwiZW4iOiJQb3RhdG9lcyB3aXRoIGZhdCJ9LCJpbWFnZSI6Ii9tZWRpYS9jYXRlZ29yeS8wMTZjMzNiNDkyMDRjNmQ3ODAzYWU3MWY3MDYxNGQ5Yy5qcGciLCJzbHVnIjoicG90YXRvZXMtd2l0aC1mYXQifV0sIjU2NiI6W3sicXVhbnRpdHkiOjMsInByb2R1Y3RJZCI6NTY2LCJwcmljZSI6OTAwLCJuYW1lIjp7Imh5IjoiXHUwNTRmXHUwNTc2XHUwNTYxXHUwNTZmXHUwNTYxXHUwNTc2IFx1MDU2Zlx1MDU3OFx1MDU3NFx1MDU3YVx1MDU3OFx1MDU3Zlx1MDU3Nlx1MDU2NVx1MDU4MCwgXHUwNTdmXHUwNTY1XHUwNTdkXHUwNTYxXHUwNTZmXHUwNTYxXHUwNTc2XHUwNTZiLCAwLjI1XHUwNTZjIiwicnUiOiJcdTA0MTRcdTA0M2VcdTA0M2NcdTA0MzBcdTA0NDhcdTA0M2RcdTA0MzhcdTA0MzUgXHUwNDNhXHUwNDNlXHUwNDNjXHUwNDNmXHUwNDNlXHUwNDQyXHUwNDRiLCBcdTA0MzBcdTA0NDFcdTA0NDFcdTA0M2VcdTA0NDBcdTA0NDJcdTA0MzhcdTA0M2NcdTA0MzVcdTA0M2RcdTA0NDIsIDAuMjVcdTA0M2IiLCJlbiI6IkhvbWVtYWRlIGNvbXBvdGVzLCBhc3NvcnRtZW50LCAwLjI1bCJ9LCJpbWFnZSI6Ii9tZWRpYS9jYXRlZ29yeS8xNDgwMjIxODNkNmYzNzAxZWY4YWU4YjIxZTQ4ZDlmNi5qcGciLCJzbHVnIjoiaG9tZW1hZGUtY29tcG90ZXMtYXNzb3J0bWVudC0wLTI1bCJ9XSwiNTcyIjpbeyJxdWFudGl0eSI6MjIsInByb2R1Y3RJZCI6NTcyLCJwcmljZSI6OTkwMCwibmFtZSI6eyJoeSI6Ilx1MDU1Nlx1MDU2MVx1MDU3Nlx1MDU3Zlx1MDU2MSAwLDVcdTA1NmMiLCJydSI6Ilx1MDQyNFx1MDQzMFx1MDQzZFx1MDQ0Mlx1MDQzMCAwLDVcdTA0M2IiLCJlbiI6IkZhbnRhIDAsNWwifSwiaW1hZ2UiOiIvbWVkaWEvY2F0ZWdvcnkvM2RkNzg3Y2NmNGEzOTBkZGU2NjgzYWI1ZTM2MjY3ZjMuanBnIiwic2x1ZyI6ImZhbnRhLTAtNWwifV0sIjU2NSI6W3sicXVhbnRpdHkiOjYsInByb2R1Y3RJZCI6NTY1LCJwcmljZSI6MjcwMCwibmFtZSI6eyJoeSI6Ilx1MDU0NFx1MDU2MVx1MDU2ZVx1MDU3Nlx1MDU2MVx1MDU2Mlx1MDU4MFx1MDU2NFx1MDU3OFx1MDU3NyBcdTA1M2ZcdTA1NjFcdTA1ODBcdTA1NjFcdTA1N2QsIDAuNVx1MDU2YyIsInJ1IjoiXHUwNDFlXHUwNDNhXHUwNDQwXHUwNDNlXHUwNDQ4XHUwNDNhXHUwNDMwIFx1MDQxYVx1MDQzMFx1MDQ0MFx1MDQzMFx1MDQ0MSwgMC41XHUwNDNiIiwiZW4iOiJPa3Jvc2hrYSBLYXJhcywgMC41bCJ9LCJpbWFnZSI6Ii9tZWRpYS9jYXRlZ29yeS8zOTI4OWM2M2ZiMjhiNDZiZTczZTEzY2NlZjVmN2JiZC5qcGciLCJzbHVnIjoib2tyb3Noa2Eta2FyYXMtMC01bCJ9XX19', '2019-03-03 19:30:26.331405'),
('82j4719r22is4gbi7v12nbpeucieeccr', 'MTdlZDZjYWE0YTgxZmVkZDNhMmM1NWE5ZTM1YWE1ZDhmNDBjODVlYTp7Il9sYW5ndWFnZSI6Imh5In0=', '2019-05-01 16:30:15.182133'),
('cty5hcbky2nrpmvtnfx5apazs6gil8e8', 'NTc2MDNhMTlmY2E3NzA0Njc5NTdkMmNmODhjMGZiNjcwOTczODJmYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YWU2Y2ZiOGRlODhkNDkwYTc2MWUyYjM0YTMxMWUxZDAwNGM4MzY5IiwiX2xhbmd1YWdlIjoiaHkifQ==', '2019-04-06 20:23:46.212716'),
('cv9t4cn09uh0z1yir87m49ltv3yd1wrc', 'OGY5ZWU5ZmZiY2MyM2ZjYzczN2ZkZDkwMzVmM2RhZTU4NTVlZGFjODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YWU2Y2ZiOGRlODhkNDkwYTc2MWUyYjM0YTMxMWUxZDAwNGM4MzY5In0=', '2019-04-21 18:32:54.273278'),
('heu1bf92ygo6ft908ucmo9sig8kcbz8z', 'MTkyYjExYmYzZjRmYmZmNjk3ZTRjMDk0NWFmNWMxNzQ1Yzc2YWQ3YTp7ImRhdGFiYXNrZXQiOnsiNjEzIjpbeyJxdWFudGl0eSI6MSwicHJvZHVjdElkIjo2MTMsInByaWNlIjo1OTAwLCJuYW1lIjp7Imh5IjoiXHUwNTU1XHUwNTcyXHUwNTZiIFwiXHUwNTUxXHUwNTYxXHUwNTdjXHUwNTdkXHUwNTZmXHUwNTYxXHUwNTc1XHUwNTYxIFx1MDUzNlx1MDU3OFx1MDU2Y1x1MDU3OFx1MDU3Zlx1MDU2MVx1MDU3NVx1MDU2MVwiLCAwLDVcdTA1NmMiLCJydSI6Ilx1MDQxMlx1MDQzZVx1MDQzNFx1MDQzYVx1MDQzMCBcIlx1MDQyNlx1MDQzMFx1MDQ0MFx1MDQ0MVx1MDQzYVx1MDQzMFx1MDQ0ZiBcdTA0MTdcdTA0M2VcdTA0M2JcdTA0M2VcdTA0NDJcdTA0MzBcdTA0NGZcIiwgMCw1XHUwNDNiIiwiZW4iOiJWb2RrYSBcIlRzYXJza2F5YSBab2xvdGF5YVwiLCAwLjVsIn0sImltYWdlIjoiL21lZGlhL2NhdGVnb3J5LzRhYjI4ZjdkZTc0MjkyZDIwY2YxMDYwNTNhNDg2NjNiLmpwZyIsInNsdWciOiJ2b2RrYS10c2Fyc2theWEtem9sb3RheWEtMC01bCJ9XSwiZGF0YSI6W3sicXVhbnRpdHkiOjEsInByaWNlIjo1OTAwfV19LCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2FlNmNmYjhkZTg4ZDQ5MGE3NjFlMmIzNGEzMTFlMWQwMDRjODM2OSJ9', '2019-04-20 10:11:25.363001'),
('hzkew5og3xvgwn9o81bjtnv2skbd4tn0', 'OGY5ZWU5ZmZiY2MyM2ZjYzczN2ZkZDkwMzVmM2RhZTU4NTVlZGFjODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YWU2Y2ZiOGRlODhkNDkwYTc2MWUyYjM0YTMxMWUxZDAwNGM4MzY5In0=', '2019-02-13 11:26:55.938655'),
('i6y8j0jc218sy5q7ygh415uah8ti3xsu', 'OGY5ZWU5ZmZiY2MyM2ZjYzczN2ZkZDkwMzVmM2RhZTU4NTVlZGFjODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YWU2Y2ZiOGRlODhkNDkwYTc2MWUyYjM0YTMxMWUxZDAwNGM4MzY5In0=', '2019-05-01 16:31:47.952232'),
('im63i08wmteqp53i185kul2e1nc7rtdk', 'NTc2MDNhMTlmY2E3NzA0Njc5NTdkMmNmODhjMGZiNjcwOTczODJmYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YWU2Y2ZiOGRlODhkNDkwYTc2MWUyYjM0YTMxMWUxZDAwNGM4MzY5IiwiX2xhbmd1YWdlIjoiaHkifQ==', '2019-02-12 21:02:00.021814'),
('k4bth13znqmr1mac74nwdf25k81ojt9a', 'NTc2MDNhMTlmY2E3NzA0Njc5NTdkMmNmODhjMGZiNjcwOTczODJmYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YWU2Y2ZiOGRlODhkNDkwYTc2MWUyYjM0YTMxMWUxZDAwNGM4MzY5IiwiX2xhbmd1YWdlIjoiaHkifQ==', '2019-04-06 19:56:35.523019'),
('md1dgcv3g1ktpwq9f4a4lohpukxg5oy0', 'MmVmOTkwMjVlOTZiMmQ0MjU4MjAwY2RmOGU3MGI0ZmQ2ZmJmNjRhNTp7fQ==', '2019-04-06 19:50:40.676740'),
('mf2j2s7c4h8pmf07a7e7godj6eiu5qs9', 'OTg3MWJiNjUyZmI0ZTdiNDQ3MmM0ZGQ5YmU0NzJjOTFmMTg3M2U0Nzp7ImRhdGFiYXNrZXRzZXNzaW9uIjp7IjU1OSI6W3sicXVhbnRpdHkiOjIsInByb2R1Y3RJZCI6NTU5LCJwcmljZSI6MTgwMCwibmFtZSI6eyJoeSI6Ilx1MDUzN1x1MDU4NFx1MDU3ZFx1MDU3Zlx1MDU4MFx1MDU2MSBcdTA1ODRcdTA1NzVcdTA1NjFcdTA1NjJcdTA1NjFcdTA1NjIgXHUwNTcwXHUwNTYxXHUwNTdlXHUwNTZiIiwicnUiOiJcdTA0MWFcdTA0NDNcdTA0NDBcdTA0MzhcdTA0M2RcdTA0NGJcdTA0MzkgXHUwNDRkXHUwNDNhXHUwNDQxXHUwNDQyXHUwNDQwXHUwNDMwIFx1MDQzYVx1MDQzNVx1MDQzMVx1MDQzMFx1MDQzMSIsImVuIjoiQ2hpY2tlbiBleHRyYSBrZWJhYiJ9LCJpbWFnZSI6Ii9tZWRpYS9jYXRlZ29yeS8yZDk4MmVmM2FkMjg2MGNlZTg0MDJiZDA3ODE0YjZjZi5qcGciLCJzbHVnIjoiY2hpY2tlbi1leHRyYS1rZWJhYiJ9XX19', '2019-03-24 16:32:42.695339'),
('mk6eqdzbe7bwp74wa7wjwnpig92yl34a', 'NTc2MDNhMTlmY2E3NzA0Njc5NTdkMmNmODhjMGZiNjcwOTczODJmYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YWU2Y2ZiOGRlODhkNDkwYTc2MWUyYjM0YTMxMWUxZDAwNGM4MzY5IiwiX2xhbmd1YWdlIjoiaHkifQ==', '2019-02-15 21:05:34.280501'),
('n6rmv70nqr2e5jlishrtnfwkzuunxvdj', 'YTcxYzdhZTkzZWVmZmZhYmRiMTEzOTJjZDZjNTZhOWFiNWE5NzQ3MDp7ImRhdGFiYXNrZXQiOnsiNTg1IjpbeyJxdWFudGl0eSI6MywicHJvZHVjdElkIjo1ODUsInByaWNlIjoyOTAwLCJuYW1lIjp7Imh5IjoiXHUwNTMxXHUwNTgwXHUwNTY1XHUwNTc2XHUwNTZiIiwicnUiOiJcdTA0MTBcdTA0NDBcdTA0MzVcdTA0M2RcdTA0MzgiLCJlbiI6IkFyZW5pIn0sImltYWdlIjoiL21lZGlhL2NhdGVnb3J5LzMwNjgxYWZhYjk2NDk4ZmM1YWRiY2MyZGY4MTNhMGMzLmpwZyIsInNsdWciOiJhcmVuaSJ9XSwiZGF0YSI6W3sicXVhbnRpdHkiOjE2LCJwcmljZSI6NDU0NTB9XSwiNTM2IjpbeyJxdWFudGl0eSI6MTIsInByb2R1Y3RJZCI6NTM2LCJwcmljZSI6MjgwMCwibmFtZSI6eyJoeSI6Ilx1MDUzZFx1MDU3OFx1MDU2Nlx1MDU2YiBcdTA1ODNcdTA1NjFcdTA1ODNcdTA1NzhcdTA1ODJcdTA1NmYgXHUwNTZkXHUwNTc4XHUwNTgwXHUwNTc4XHUwNTdlXHUwNTYxXHUwNTZlIiwicnUiOiJcdTA0MjhcdTA0MzBcdTA0NDhcdTA0M2JcdTA0NGJcdTA0M2EgXHUwNDM4XHUwNDM3IFx1MDQ0MVx1MDQzMlx1MDQzOFx1MDQzZFx1MDQzOFx1MDQzZFx1MDQ0YiIsImVuIjoiUG9yayB0ZW5kZXJsb2luIGJhcmJlY3VlIn0sImltYWdlIjoiL21lZGlhL2NhdGVnb3J5L2YyYzE2ZmU3ZGZjOTM2MmRlNTRhNjI2YTI2ZWY1OGFkLmpwZyIsInNsdWciOiJwb3JrLXRlbmRlcmxvaW4tYmFyYmVjdWUifV0sIjUzNCI6W3sicXVhbnRpdHkiOjEsInByb2R1Y3RJZCI6NTM0LCJwcmljZSI6MzE1MCwibmFtZSI6eyJoeSI6Ilx1MDUzZFx1MDU3OFx1MDU2Nlx1MDU2YiBcdTA1NzlcdTA1NjFcdTA1NmNcdTA1NjFcdTA1NzJcdTA1NjFcdTA1N2IiLCJydSI6IktcdTA0M2VcdTA0NDBcdTA0MzVcdTA0MzlcdTA0M2FhIFx1MDQ0MVx1MDQzMlx1MDQzOFx1MDQzZFx1MDQzOFx1MDQzZFx1MDQ0YiIsImVuIjoiUG9yayBsb2luIn0sImltYWdlIjoiL21lZGlhL2NhdGVnb3J5LzViNWExNGY1MGFmNzNkNzEwYWY2MzFmYjMxOWE3MzRlLmpwZyIsInNsdWciOiJwb3JrLWxvaW4ifV19LCJfbGFuZ3VhZ2UiOiJoeSJ9', '2019-04-05 20:36:40.758166'),
('nm2fwn4zd7ainprx2q4nqce5ey1uybfi', 'ZjgxYmQ3YmIyNjdhYTVlNmU4NWQ2YWMwOTk3YmU1ODRmNzUzZDM4Nzp7ImRhdGFiYXNrZXRzZXNzaW9uMiI6eyI3MTIiOlt7InF1YW50aXR5Ijo0LCJwcm9kdWN0SWQiOjcxMiwicHJpY2UiOjM4MDAsIm5hbWUiOnsiaHkiOiJcdTA1NDFcdTA1N2VcdTA1NjFcdTA1NmVcdTA1NjVcdTA1NzIgXHUwNTYyXHUwNTYxXHUwNTdkXHUwNTdmXHUwNTc4XHUwNTgyXHUwNTgwXHUwNTc0XHUwNTYxXHUwNTc1XHUwNTc4XHUwNTdlIiwicnUiOiJcdTA0MmZcdTA0MzhcdTA0NDdcdTA0M2RcdTA0MzhcdTA0NDZcdTA0MzAgXHUwNDQxIFx1MDQzMVx1MDQzMFx1MDQ0MVx1MDQ0Mlx1MDQ0M1x1MDQ0MFx1MDQzY1x1MDQzZVx1MDQzOSIsImVuIjoiRnJpZWQgRWdncyB3aXRoIGJhc3R1cm1hIn0sImltYWdlIjoiL21lZGlhL2NhdGVnb3J5LzkxMzM5M2VhYmRkZDQyYTM4YmIxMmI1ODAxYTQ3NDllLmpwZyIsInNsdWciOiJmcmllZC1lZ2dzLXdpdGgtYmFzdHVybWEifV19LCJkYXRhYmFza2V0c2Vzc2lvbjMiOnsiNzEyIjpbeyJxdWFudGl0eSI6MjYsInByb2R1Y3RJZCI6NzEyLCJwcmljZSI6MjQ3MDAsIm5hbWUiOnsiaHkiOiJcdTA1NDFcdTA1N2VcdTA1NjFcdTA1NmVcdTA1NjVcdTA1NzIgXHUwNTYyXHUwNTYxXHUwNTdkXHUwNTdmXHUwNTc4XHUwNTgyXHUwNTgwXHUwNTc0XHUwNTYxXHUwNTc1XHUwNTc4XHUwNTdlIiwicnUiOiJcdTA0MmZcdTA0MzhcdTA0NDdcdTA0M2RcdTA0MzhcdTA0NDZcdTA0MzAgXHUwNDQxIFx1MDQzMVx1MDQzMFx1MDQ0MVx1MDQ0Mlx1MDQ0M1x1MDQ0MFx1MDQzY1x1MDQzZVx1MDQzOSIsImVuIjoiRnJpZWQgRWdncyB3aXRoIGJhc3R1cm1hIn0sImltYWdlIjoiL21lZGlhL2NhdGVnb3J5LzkxMzM5M2VhYmRkZDQyYTM4YmIxMmI1ODAxYTQ3NDllLmpwZyIsInNsdWciOiJmcmllZC1lZ2dzLXdpdGgtYmFzdHVybWEifV19fQ==', '2019-03-29 18:06:15.790493'),
('qlb0fu9t1goms4ysgi49cvrp5auvwgvs', 'ZjVhZTA2NmY3MGYzYmFiYjYxM2ZjYzM3YmZmYzcyM2Q5YTVlNGJkNTp7ImRhdGFiYXNrZXRzZXNzaW9uIjp7IjYxNyI6W3sicXVhbnRpdHkiOjMsInByb2R1Y3RJZCI6NjE3LCJwcmljZSI6NTcwMDAsIm5hbWUiOnsiaHkiOiJcdTA1NTVcdTA1NzJcdTA1NmIgXCJcdTA1NTZcdTA1NjVcdTA1NzVcdTA1NzdcdTA1NzZcIiwgMCw1XHUwNTZjIiwicnUiOiJcdTA0MTJcdTA0M2VcdTA0MzRcdTA0M2FcdTA0MzAgXCJcdTA0MjRcdTA0MzVcdTA0MzlcdTA0NDhcdTA0M2RcIiwgMCw1XHUwNDNiIiwiZW4iOiJWb2RrYSBcIkZhc2hpb25cIiwgMCw1bCJ9LCJpbWFnZSI6Ii9tZWRpYS9jYXRlZ29yeS83N2RjYjFhYWM4NWEzODhiMzhlZDMxNjZhYTA0NWE3ZS5qcGciLCJzbHVnIjoidm9ka2EtZmFzaGlvbi0wLTVsIn1dLCJkYXRhIjpbeyJxdWFudGl0eSI6MywicHJpY2UiOjU3MDAwfV19fQ==', '2019-03-11 18:49:26.448991'),
('rsfxpg4ipls38c0rlv9nhs83xq3s9zuv', 'MWE0YjViOWIwZWRhMjFhN2E1YTk5NmIzZjhkNDcyZmRmNzE2ZjE4ZDp7Il9sYW5ndWFnZSI6Imh5IiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjdhZTZjZmI4ZGU4OGQ0OTBhNzYxZTJiMzRhMzExZTFkMDA0YzgzNjkiLCJkYXRhYmFza2V0Ijp7IjcwOSI6W3sicXVhbnRpdHkiOjQsInByb2R1Y3RJZCI6NzA5LCJwcmljZSI6NTAwLCJuYW1lIjp7Imh5IjoiXHUwNTQxXHUwNTdlXHUwNTYxXHUwNTZlXHUwNTY1XHUwNTcyIiwicnUiOiJcdTA0MmZcdTA0MzhcdTA0NDdcdTA0M2RcdTA0MzhcdTA0NDZcdTA0MzAiLCJlbiI6IkZyaWVkIEVnZ3MifSwiaW1hZ2UiOiIvbWVkaWEvY2F0ZWdvcnkvOWQ2M2MxNzIzYTJlMjk2OGI3N2ExZjkyMjc2N2ExZGEuanBnIiwic2x1ZyI6ImZyaWVkLWVnZ3MifV0sImRhdGEiOlt7InF1YW50aXR5Ijo0LCJwcmljZSI6MjAwMH1dfX0=', '2019-05-16 13:36:05.907765'),
('sl9z3vfdnuv930m0z0vbpvh8l86ywgm9', 'MDUyMGE1NDM1YjFkN2I0NzZhNzg4ZWExMmMxMWFhZDgyYTVhOTBkZDp7ImRhdGFiYXNrZXQiOnsiNjE2IjpbeyJxdWFudGl0eSI6MjEsInByb2R1Y3RJZCI6NjE2LCJwcmljZSI6NzgwMCwibmFtZSI6eyJoeSI6Ilx1MDU1NVx1MDU3Mlx1MDU2YiBcIlx1MDUzMVx1MDU2Mlx1MDU3ZFx1MDU2MVx1MDU2Y1x1MDU3NVx1MDU3OFx1MDU4Mlx1MDU3ZlwiLCAwLDVcdTA1NmMiLCJydSI6Ilx1MDQxMlx1MDQzZVx1MDQzNFx1MDQzYVx1MDQzMCBcIlx1MDQxMFx1MDQzMVx1MDQ0MVx1MDQzZVx1MDQzYlx1MDQ0ZVx1MDQ0MlwiLCAwLDVcdTA0M2IiLCJlbiI6IlZvZGthIFwiQWJzb2x1dFwiLCAwLDVsIn0sImltYWdlIjoiL21lZGlhL2NhdGVnb3J5LzA2MjJjNWFjMGJjMWU4YTI5Y2ZjOTkxYmZhYWE5ZWYxLmpwZyIsInNsdWciOiJ2b2RrYS1hYnNvbHV0LTAtNWwifV0sImRhdGEiOlt7InF1YW50aXR5IjoyMywicHJpY2UiOjE2OTQ1MH1dLCI1MzQiOlt7InF1YW50aXR5IjoxLCJwcm9kdWN0SWQiOjUzNCwicHJpY2UiOjMxNTAsIm5hbWUiOnsiaHkiOiJcdTA1M2RcdTA1NzhcdTA1NjZcdTA1NmIgXHUwNTc5XHUwNTYxXHUwNTZjXHUwNTYxXHUwNTcyXHUwNTYxXHUwNTdiIiwicnUiOiJLXHUwNDNlXHUwNDQwXHUwNDM1XHUwNDM5XHUwNDNhYSBcdTA0NDFcdTA0MzJcdTA0MzhcdTA0M2RcdTA0MzhcdTA0M2RcdTA0NGIiLCJlbiI6IlBvcmsgbG9pbiJ9LCJpbWFnZSI6Ii9tZWRpYS9jYXRlZ29yeS81YjVhMTRmNTBhZjczZDcxMGFmNjMxZmIzMTlhNzM0ZS5qcGciLCJzbHVnIjoicG9yay1sb2luIn1dLCI1MzUiOlt7InF1YW50aXR5IjoxLCJwcm9kdWN0SWQiOjUzNSwicHJpY2UiOjI1MDAsIm5hbWUiOnsiaHkiOiJcdTA1M2RcdTA1NzhcdTA1NjZcdTA1NmIgXHUwNTZmXHUwNTc4XHUwNTcyXHUwNTZiXHUwNTZmXHUwNTc2XHUwNTY1XHUwNTgwIiwicnUiOiJcdTA0MjhcdTA0MzBcdTA0NDhcdTA0M2JcdTA0NGJcdTA0M2EgXHUwNDM4XHUwNDM3IFx1MDQ0MFx1MDQzNVx1MDQzMVx1MDQ0MFx1MDQ0Ylx1MDQ0OFx1MDQzYVx1MDQzOCBcdTA0NDFcdTA0MzJcdTA0MzhcdTA0M2RcdTA0MzhcdTA0M2RcdTA0NGIiLCJlbiI6IlBvcmsgcmlicyJ9LCJpbWFnZSI6Ii9tZWRpYS9jYXRlZ29yeS8zMWY5YjhjMWMzMmMyYmQ2N2JiN2M0MTQzZGEzNDIyOS5qcGciLCJzbHVnIjoicG9yay1yaWJzIn1dfX0=', '2019-03-30 19:11:48.444313'),
('t64fgqa2927n7ok49wha34k81keulg09', 'OGY5ZWU5ZmZiY2MyM2ZjYzczN2ZkZDkwMzVmM2RhZTU4NTVlZGFjODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YWU2Y2ZiOGRlODhkNDkwYTc2MWUyYjM0YTMxMWUxZDAwNGM4MzY5In0=', '2019-02-15 13:01:39.443536'),
('t84s9l72yjqp07ib6gc7y0gr8ev0adfy', 'MWQ0MzVlNDFmNTU1ODgyNzNhY2FkYTdkZGJkMTY2YjljZGZhZDkxOTp7Il9sYW5ndWFnZSI6ImVuIiwiZGF0YWJhc2tldCI6eyJkYXRhIjpbeyJxdWFudGl0eSI6MTYsInByaWNlIjoyMDEwMH1dLCI1NjYiOlt7InF1YW50aXR5Ijo0LCJwcm9kdWN0SWQiOjU2NiwicHJpY2UiOjMwMCwibmFtZSI6eyJoeSI6Ilx1MDU0Zlx1MDU3Nlx1MDU2MVx1MDU2Zlx1MDU2MVx1MDU3NiBcdTA1NmZcdTA1NzhcdTA1NzRcdTA1N2FcdTA1NzhcdTA1N2ZcdTA1NzZcdTA1NjVcdTA1ODAsIFx1MDU3Zlx1MDU2NVx1MDU3ZFx1MDU2MVx1MDU2Zlx1MDU2MVx1MDU3Nlx1MDU2YiwgMC4yNVx1MDU2YyIsInJ1IjoiXHUwNDE0XHUwNDNlXHUwNDNjXHUwNDMwXHUwNDQ4XHUwNDNkXHUwNDM4XHUwNDM1IFx1MDQzYVx1MDQzZVx1MDQzY1x1MDQzZlx1MDQzZVx1MDQ0Mlx1MDQ0YiwgXHUwNDMwXHUwNDQxXHUwNDQxXHUwNDNlXHUwNDQwXHUwNDQyXHUwNDM4XHUwNDNjXHUwNDM1XHUwNDNkXHUwNDQyLCAwLjI1XHUwNDNiIiwiZW4iOiJIb21lbWFkZSBjb21wb3RlcywgYXNzb3J0bWVudCwgMC4yNWwifSwiaW1hZ2UiOiIvbWVkaWEvY2F0ZWdvcnkvMTQ4MDIyMTgzZDZmMzcwMWVmOGFlOGIyMWU0OGQ5ZjYuanBnIiwic2x1ZyI6ImhvbWVtYWRlLWNvbXBvdGVzLWFzc29ydG1lbnQtMC0yNWwifV0sIjUzOCI6W3sicXVhbnRpdHkiOjMsInByb2R1Y3RJZCI6NTM4LCJwcmljZSI6MTA1MCwibmFtZSI6eyJoeSI6Ilx1MDU0Zlx1MDU2MVx1MDU3ZVx1MDU2MVx1MDU4MFx1MDU2YiBcdTA1N2RcdTA1ODBcdTA1N2ZcdTA1NmIgXHUwNTZkXHUwNTc4XHUwNTgwXHUwNTc4XHUwNTdlXHUwNTYxXHUwNTZlIiwicnUiOiJcdTA0MjhcdTA0MzBcdTA0NDhcdTA0M2JcdTA0NGJcdTA0M2EgXHUwNDM4XHUwNDM3IFx1MDQzM1x1MDQzZVx1MDQzMlx1MDQ0Zlx1MDQzNlx1MDQ0Y1x1MDQzNVx1MDQzM1x1MDQzZSBcdTA0NDFcdTA0MzVcdTA0NDBcdTA0MzRcdTA0NDZcdTA0MzAiLCJlbiI6IkJlZWYgaGVhcnQgYmFyYmVjdWUifSwiaW1hZ2UiOiIvbWVkaWEvY2F0ZWdvcnkvY2NlYjU4NjA3MTQ3N2ZjYWQ3MTM2MjU4Y2QxMGQ0YjguanBnIiwic2x1ZyI6ImJlZWYtaGVhcnQtYmFyYmVjdWUifV0sIjYzMiI6W3sicXVhbnRpdHkiOjksInByb2R1Y3RJZCI6NjMyLCJwcmljZSI6MTc1MCwibmFtZSI6eyJoeSI6Ilx1MDU0YVx1MDU2Ylx1MDU4MVx1MDU4MVx1MDU2MSBcdTA1NDlcdTA1NmJcdTA1NmZcdTA1NzYgXHUwNTQ0XHUwNTYxXHUwNTdjXHUwNTZjXHUwNTY1XHUwNTc1LCAyNFx1MDU3ZFx1MDU3NCIsInJ1IjoiXHUwNDFmXHUwNDM4XHUwNDQ2XHUwNDQ2XHUwNDMwIFx1MDQyN1x1MDQzOFx1MDQzYVx1MDQzZCBcdTA0MWNcdTA0MzBcdTA0NDBcdTA0M2JcdTA0MzVcdTA0MzksIDI0XHUwNDQxXHUwNDNjIiwiZW4iOiJQaXp6YSBDaGlja2VuIE1hcmxleSwgMjRjbSJ9LCJpbWFnZSI6Ii9tZWRpYS9jYXRlZ29yeS9mMWZlZDhiMDgxYzQyYzUyYzI1YWVlYzAzYzc0NDc2YS5qcGciLCJzbHVnIjoicGl6emEtY2hpY2tlbi1tYXJsZXktMjRjbSJ9XX0sIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YWU2Y2ZiOGRlODhkNDkwYTc2MWUyYjM0YTMxMWUxZDAwNGM4MzY5In0=', '2019-04-17 18:20:57.811888'),
('uhppt60xbbrxa165gradhzhodl7tyd91', 'MTdlZDZjYWE0YTgxZmVkZDNhMmM1NWE5ZTM1YWE1ZDhmNDBjODVlYTp7Il9sYW5ndWFnZSI6Imh5In0=', '2019-04-06 07:43:42.215059'),
('ujuh0eqz2ngtmy5tluawv8bt3j6k49ej', 'OGY5ZWU5ZmZiY2MyM2ZjYzczN2ZkZDkwMzVmM2RhZTU4NTVlZGFjODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YWU2Y2ZiOGRlODhkNDkwYTc2MWUyYjM0YTMxMWUxZDAwNGM4MzY5In0=', '2019-04-16 18:04:26.559743'),
('ulx9co7l0iuul97ma4q1y5wigan74ufi', 'ZTIzNmZiN2M2YWRkZGM5MTZhY2M0OWNmNThhMGVhNjFhNGY4YTQ0YTp7Il9sYW5ndWFnZSI6ImVuIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjdhZTZjZmI4ZGU4OGQ0OTBhNzYxZTJiMzRhMzExZTFkMDA0YzgzNjkifQ==', '2019-04-13 10:38:43.579512'),
('x8i91fxclycrri38q8a2pm0v6pcukcfa', 'OGY5ZWU5ZmZiY2MyM2ZjYzczN2ZkZDkwMzVmM2RhZTU4NTVlZGFjODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YWU2Y2ZiOGRlODhkNDkwYTc2MWUyYjM0YTMxMWUxZDAwNGM4MzY5In0=', '2019-02-12 11:08:00.587452'),
('z2i1wrdptlv084w0weep4ic24h2nil8y', 'MTdhODU5OTNiMDFmYmY5YTA2NjMyZDUxNmE0YWU5NTE4ZTYxYWU0MTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YWU2Y2ZiOGRlODhkNDkwYTc2MWUyYjM0YTMxMWUxZDAwNGM4MzY5IiwiZGF0YWJhc2tldCI6eyI2MTciOlt7InF1YW50aXR5IjoxLCJwcm9kdWN0SWQiOjYxNywicHJpY2UiOjE5MDAwLCJuYW1lIjp7Imh5IjoiXHUwNTU1XHUwNTcyXHUwNTZiIFwiXHUwNTU2XHUwNTY1XHUwNTc1XHUwNTc3XHUwNTc2XCIsIDAsNVx1MDU2YyIsInJ1IjoiXHUwNDEyXHUwNDNlXHUwNDM0XHUwNDNhXHUwNDMwIFwiXHUwNDI0XHUwNDM1XHUwNDM5XHUwNDQ4XHUwNDNkXCIsIDAsNVx1MDQzYiIsImVuIjoiVm9ka2EgXCJGYXNoaW9uXCIsIDAsNWwifSwiaW1hZ2UiOiIvbWVkaWEvY2F0ZWdvcnkvNzdkY2IxYWFjODVhMzg4YjM4ZWQzMTY2YWEwNDVhN2UuanBnIiwic2x1ZyI6InZvZGthLWZhc2hpb24tMC01bCJ9XSwiZGF0YSI6W3sicXVhbnRpdHkiOjIsInByaWNlIjoyMDc1MH1dLCI2MzIiOlt7InF1YW50aXR5IjoxLCJwcm9kdWN0SWQiOjYzMiwicHJpY2UiOjE3NTAsIm5hbWUiOnsiaHkiOiJcdTA1NGFcdTA1NmJcdTA1ODFcdTA1ODFcdTA1NjEgXHUwNTQ5XHUwNTZiXHUwNTZmXHUwNTc2IFx1MDU0NFx1MDU2MVx1MDU3Y1x1MDU2Y1x1MDU2NVx1MDU3NSwgMjRcdTA1N2RcdTA1NzQiLCJydSI6Ilx1MDQxZlx1MDQzOFx1MDQ0Nlx1MDQ0Nlx1MDQzMCBcdTA0MjdcdTA0MzhcdTA0M2FcdTA0M2QgXHUwNDFjXHUwNDMwXHUwNDQwXHUwNDNiXHUwNDM1XHUwNDM5LCAyNFx1MDQ0MVx1MDQzYyIsImVuIjoiUGl6emEgQ2hpY2tlbiBNYXJsZXksIDI0Y20ifSwiaW1hZ2UiOiIvbWVkaWEvY2F0ZWdvcnkvZjFmZWQ4YjA4MWM0MmM1MmMyNWFlZWMwM2M3NDQ3NmEuanBnIiwic2x1ZyI6InBpenphLWNoaWNrZW4tbWFybGV5LTI0Y20ifV19fQ==', '2019-04-08 18:04:18.083568'),
('ze2cxlffy1bv4z808gic22xan3jrgeqe', 'NTc2MDNhMTlmY2E3NzA0Njc5NTdkMmNmODhjMGZiNjcwOTczODJmYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YWU2Y2ZiOGRlODhkNDkwYTc2MWUyYjM0YTMxMWUxZDAwNGM4MzY5IiwiX2xhbmd1YWdlIjoiaHkifQ==', '2019-02-13 07:33:15.543169');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `path` varchar(100) NOT NULL,
  `object_id` int(11) NOT NULL,
  `type` enum('post','category','about') NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `rank` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `path`, `object_id`, `type`, `created_at`, `updated_at`, `rank`) VALUES
(534, '/media/category/5b5a14f50af73d710af631fb319a734e.jpg', 534, 'post', '2019-02-01', '2019-02-01', 0),
(535, '/media/category/31f9b8c1c32c2bd67bb7c4143da34229.jpg', 535, 'post', '2019-02-01', '2019-02-01', 0),
(536, '/media/category/f2c16fe7dfc9362de54a626a26ef58ad.jpg', 536, 'post', '2019-02-01', '2019-02-01', 0),
(537, '/media/category/45f9799e62aa6d2ca302f93f4b1a135d.jpg', 537, 'post', '2019-02-01', '2019-02-01', 0),
(538, '/media/category/cceb586071477fcad7136258cd10d4b8.jpg', 538, 'post', '2019-02-01', '2019-02-01', 0),
(539, '/media/category/1e160353fcb7941cf1118af3d792b0d3.jpg', 539, 'post', '2019-02-01', '2019-02-01', 0),
(540, '/media/category/a45c42af11fc037578075d4645e1fb57.jpg', 540, 'post', '2019-02-01', '2019-02-01', 0),
(541, '/media/category/7d771a9981ca6d33d0114ee92ec58224.jpg', 541, 'post', '2019-02-01', '2019-02-01', 0),
(542, '/media/category/671eae0f799b84e31cd95a89a7b03959.jpg', 542, 'post', '2019-02-01', '2019-02-01', 0),
(543, '/media/category/e2f9188bb99bd0365b2d50f8d674408a.jpg', 543, 'post', '2019-02-01', '2019-02-01', 0),
(544, '/media/category/9c534a7b0ff76d5830444cfaee73a73d.jpg', 544, 'post', '2019-02-01', '2019-02-01', 0),
(545, '/media/category/c9ed0fb29a75bddb2dae848a745c8b47.jpg', 545, 'post', '2019-02-01', '2019-02-01', 0),
(546, '/media/category/ed9057aca4f235aaeb5847cdeea3830b.jpg', 546, 'post', '2019-02-01', '2019-02-01', 0),
(547, '/media/category/581da671675cf2e5ee69c63ac6799823.jpg', 547, 'post', '2019-02-01', '2019-02-01', 0),
(548, '/media/category/fbacd45c1af4ae4c5c49d1fff6b35ff7.jpg', 548, 'post', '2019-02-01', '2019-02-01', 0),
(549, '/media/category/64d0f50f88e009e20b65653cdedeeaf1.jpg', 549, 'post', '2019-02-01', '2019-02-01', 0),
(550, '/media/category/985776b2a758508ea8136da4cf24419a.jpg', 550, 'post', '2019-02-01', '2019-02-01', 0),
(551, '/media/category/b48249cced5e62251ff681e1a77b5601.jpg', 551, 'post', '2019-02-01', '2019-02-01', 0),
(552, '/media/category/11b2f16e4a01fa599c85e0fdf7432601.jpg', 552, 'post', '2019-02-01', '2019-02-01', 0),
(553, '/media/category/29db241624f0e8dc015aa1ff14d8918d.jpg', 553, 'post', '2019-02-01', '2019-02-01', 0),
(554, '/media/category/d3013f22d0f8a849b39c7cfc80abb6a2.jpg', 554, 'post', '2019-02-01', '2019-02-01', 0),
(555, '/media/category/a27517f60cc17dd9fe43ca9e6711ffdc.jpg', 555, 'post', '2019-02-01', '2019-02-01', 0),
(556, '/media/category/38beacf5ee2e97c35846c535d07340b6.jpg', 556, 'post', '2019-02-01', '2019-02-01', 0),
(557, '/media/category/ff1c5082b5bffb6f130580c1bf691bae.jpg', 557, 'post', '2019-02-01', '2019-02-01', 0),
(558, '/media/category/3a85caa285a291597b37b42ec8325e32.jpg', 558, 'post', '2019-02-01', '2019-02-01', 0),
(559, '/media/category/2d982ef3ad2860cee8402bd07814b6cf.jpg', 559, 'post', '2019-02-01', '2019-02-01', 0),
(560, '/media/category/16263558d47614bc06e926bfd4f42a48.jpg', 560, 'post', '2019-02-01', '2019-02-01', 0),
(561, '/media/category/e7ba01403fe080c0142839050017e2cc.jpg', 561, 'post', '2019-02-01', '2019-02-01', 0),
(562, '/media/category/016c33b49204c6d7803ae71f70614d9c.jpg', 562, 'post', '2019-02-01', '2019-02-01', 0),
(563, '/media/category/b239295952c7a0e1ef15a5e5cb5d9b5e.jpg', 563, 'post', '2019-02-01', '2019-02-01', 0),
(564, '/media/category/cd807c732a6a51ca84516606166e8d99.jpg', 564, 'post', '2019-02-01', '2019-02-01', 0),
(565, '/media/category/39289c63fb28b46be73e13ccef5f7bbd.jpg', 565, 'post', '2019-02-01', '2019-02-01', 0),
(566, '/media/category/148022183d6f3701ef8ae8b21e48d9f6.jpg', 566, 'post', '2019-02-01', '2019-02-01', 0),
(567, '/media/category/d0eea03d5590f442f7e53459bbda2f3f.jpg', 567, 'post', '2019-02-01', '2019-02-01', 0),
(568, '/media/category/947d92ad9f326c4c8a0c34eb6ccf6e84.jpg', 568, 'post', '2019-02-01', '2019-02-01', 0),
(569, '/media/category/23fb605192cfb871e5d6206292ba7312.jpg', 569, 'post', '2019-02-01', '2019-02-01', 0),
(570, '/media/category/17ccb08c30fccc5ccf23238c6f93a910.jpg', 570, 'post', '2019-02-01', '2019-02-01', 0),
(571, '/media/category/d82c1509e62ef792df8f6464e0daafc2.jpg', 571, 'post', '2019-02-01', '2019-02-01', 0),
(572, '/media/category/3dd787ccf4a390dde6683ab5e36267f3.jpg', 572, 'post', '2019-02-01', '2019-02-01', 0),
(573, '/media/category/01021dac89d83f309f2d24e43b010ed7.jpg', 573, 'post', '2019-02-01', '2019-02-01', 0),
(574, '/media/category/1e02c85f450aecc407638710bf61c6df.jpg', 574, 'post', '2019-02-01', '2019-02-01', 0),
(575, '/media/category/faa40b193f3b16af818396439d71d341.jpg', 575, 'post', '2019-02-01', '2019-02-01', 0),
(576, '/media/category/0c984d74f2b99dcb44b6a34c8233fef5.jpg', 576, 'post', '2019-02-01', '2019-02-01', 0),
(577, '/media/category/75870973ba50b9c5464caa009136febf.jpg', 577, 'post', '2019-02-01', '2019-02-01', 0),
(578, '/media/category/0e028f3e323372cbb45727dc88188767.jpg', 578, 'post', '2019-02-01', '2019-02-01', 0),
(579, '/media/category/a8dad4c6c87db8229ccac1f755456db8.jpg', 579, 'post', '2019-02-01', '2019-02-01', 0),
(580, '/media/category/ab2f5e3fbb5550e9a242c6adca28cbc1.jpg', 580, 'post', '2019-02-01', '2019-02-01', 0),
(581, '/media/category/90c14bbd03cdd0f481753842e307ed7f.jpg', 581, 'post', '2019-02-01', '2019-02-01', 0),
(582, '/media/category/f38cf10009b16d58384806247f3519cd.jpg', 582, 'post', '2019-02-01', '2019-02-01', 0),
(583, '/media/category/7dfb0f7bd764010fc32368aff65ecefb.jpg', 583, 'post', '2019-02-01', '2019-02-01', 0),
(584, '/media/category/13222d2ddfa46cc571b8906f2f1417a6.jpg', 584, 'post', '2019-02-01', '2019-02-01', 0),
(585, '/media/category/30681afab96498fc5adbcc2df813a0c3.jpg', 585, 'post', '2019-02-01', '2019-02-01', 0),
(586, '/media/category/f9026b38bb906fda8c611139a5ed5247.jpg', 586, 'post', '2019-02-01', '2019-02-01', 0),
(587, '/media/category/db070484ce26528aabc9c86368831888.jpg', 587, 'post', '2019-02-01', '2019-02-01', 0),
(588, '/media/category/815406ecaa41d6190a58970cc999eab4.jpg', 588, 'post', '2019-02-01', '2019-02-01', 0),
(589, '/media/category/09c67acb4b41842e71f20126b993d9c8.jpg', 589, 'post', '2019-02-01', '2019-02-01', 0),
(590, '/media/category/b0a1732adab4d2d391aab3263fbcff4b.jpg', 590, 'post', '2019-02-01', '2019-02-01', 0),
(591, '/media/category/706589b7dfd0d29c8950ebd1b0d88b76.jpg', 591, 'post', '2019-02-01', '2019-02-01', 0),
(592, '/media/category/9296b8bc882a563b261c00e930881312.jpg', 592, 'post', '2019-02-01', '2019-02-01', 0),
(593, '/media/category/e629ac0f78bbb5603bebb2794b727254.jpg', 593, 'post', '2019-02-01', '2019-02-01', 0),
(594, '/media/category/12f695ab9b7152964ddb2c6723b8ddc0.jpg', 594, 'post', '2019-02-01', '2019-02-01', 0),
(595, '/media/category/cc71fc8d951fca029cd0f25e26ce268e.jpg', 595, 'post', '2019-02-01', '2019-02-01', 0),
(596, '/media/category/b9b4fdeeaac22ce54e6950649314d409.jpg', 596, 'post', '2019-02-01', '2019-02-01', 0),
(597, '/media/category/b81ba87f337bb75a9512417d539f5bec.jpg', 597, 'post', '2019-02-01', '2019-02-01', 0),
(598, '/media/category/1a41d9c9ce9e3d7c3e3a5799d628c2a9.jpg', 598, 'post', '2019-02-01', '2019-02-01', 0),
(599, '/media/category/c158b2a59277d875cf0cdc18cb325070.jpg', 599, 'post', '2019-02-01', '2019-02-01', 0),
(600, '/media/category/a035ca1ef3ccfe37dc4768c7e583e7b2.jpg', 600, 'post', '2019-02-01', '2019-02-01', 0),
(601, '/media/category/2908d09672bbc51f0402023766b496e9.jpg', 601, 'post', '2019-02-01', '2019-02-01', 0),
(602, '/media/category/7dde5ebe9e244488eee8fab3110a5bf8.jpg', 602, 'post', '2019-02-01', '2019-02-01', 0),
(603, '/media/category/5d3512f6d75ade5aa53df282d35da796.jpg', 603, 'post', '2019-02-01', '2019-02-01', 0),
(604, '/media/category/969da668a7819b5011485c9e2ff83588.jpg', 604, 'post', '2019-02-01', '2019-02-01', 0),
(605, '/media/category/eb66290a7721e15208124fb2a2352666.jpg', 605, 'post', '2019-02-01', '2019-02-01', 0),
(606, '/media/category/7b464b55fe7d0024072fad06e6150c0e.jpg', 606, 'post', '2019-02-01', '2019-02-01', 0),
(607, '/media/category/fd26b30a06b509026ce92e523de6c465.jpg', 607, 'post', '2019-02-01', '2019-02-01', 0),
(608, '/media/category/b3ba879db75cf4a1bc72e4954becaf8a.jpg', 608, 'post', '2019-02-01', '2019-02-01', 0),
(609, '/media/category/609c11b11769736a0d1aa3f629131d7a.jpg', 609, 'post', '2019-02-01', '2019-02-01', 0),
(610, '/media/category/24df397a24111b1ed401e8690c7d0ea9.jpg', 610, 'post', '2019-02-01', '2019-02-01', 0),
(611, '/media/category/344960c010c8d56919eb90f6e76044bf.jpg', 611, 'post', '2019-02-01', '2019-02-01', 0),
(612, '/media/category/3860bb77eb73052daf1ac8ba6eee117b.jpg', 612, 'post', '2019-02-01', '2019-02-01', 0),
(613, '/media/category/4ab28f7de74292d20cf106053a48663b.jpg', 613, 'post', '2019-02-01', '2019-02-01', 0),
(614, '/media/category/400e928e68c81a8e67d754fd3cc95725.jpg', 614, 'post', '2019-02-01', '2019-02-01', 0),
(615, '/media/category/8614b81f8642b90f7ae3ac2cf1f17c40.jpg', 615, 'post', '2019-02-01', '2019-02-01', 0),
(616, '/media/category/0622c5ac0bc1e8a29cfc991bfaaa9ef1.jpg', 616, 'post', '2019-02-01', '2019-02-01', 0),
(617, '/media/category/77dcb1aac85a388b38ed3166aa045a7e.jpg', 617, 'post', '2019-02-01', '2019-02-01', 0),
(618, '/media/category/60c96c7207263e98476f2384e7f09e2c.jpg', 618, 'post', '2019-02-01', '2019-02-01', 0),
(619, '/media/category/0d47109754f606b45844301d062ea5d3.jpg', 619, 'post', '2019-02-01', '2019-02-01', 0),
(620, '/media/category/f3ea4882aa43443ec933d09498fd4346.jpg', 620, 'post', '2019-02-01', '2019-02-01', 0),
(621, '/media/category/618036d1a99b3fb044132ba0d7be2c28.jpg', 621, 'post', '2019-02-01', '2019-02-01', 0),
(622, '/media/category/149bcffc13e3c9a73d1276d61ee4a6b9.jpg', 622, 'post', '2019-02-01', '2019-02-01', 0),
(623, '/media/category/b9068e05d93ec9431bb6e984b75151eb.jpg', 623, 'post', '2019-02-01', '2019-02-01', 0),
(624, '/media/category/b6902c1e070531ecca99b96580c8f68c.jpg', 624, 'post', '2019-02-01', '2019-02-01', 0),
(625, '/media/category/b8a560bdbb7bd70acb21e0d7155f0f2b.jpg', 625, 'post', '2019-02-01', '2019-02-01', 0),
(626, '/media/category/3ff51254f9aedd24af2eeea1b19b13d1.jpg', 626, 'post', '2019-02-01', '2019-02-01', 0),
(627, '/media/category/8acaac0aa53a77916d1f030879d1f6b5.jpg', 627, 'post', '2019-02-01', '2019-02-01', 0),
(628, '/media/category/3410f5e5ebdef5aedc78afe2fb0b4db0.jpg', 628, 'post', '2019-02-01', '2019-02-01', 0),
(629, '/media/category/415e438aedee0a9e02ed500464340240.jpg', 629, 'post', '2019-02-01', '2019-02-01', 0),
(630, '/media/category/ff16ec97ac78d6fb737a8b3de2075fe0.jpg', 630, 'post', '2019-02-01', '2019-02-01', 0),
(631, '/media/category/f2cf5bb0420f317f2cf25ca2fe0c3ef8.jpg', 631, 'post', '2019-02-01', '2019-02-01', 0),
(632, '/media/category/f1fed8b081c42c52c25aeec03c74476a.jpg', 632, 'post', '2019-02-01', '2019-02-01', 0),
(633, '/media/category/4f9679670fe38875dd3c17a7d425044f.jpg', 633, 'post', '2019-02-01', '2019-02-01', 0),
(634, '/media/category/14ad51eaf5687639ff7fd4e10f6a4f16.jpg', 634, 'post', '2019-02-01', '2019-02-01', 0),
(635, '/media/category/6e41ad211248b82fcd628de1d7e3f9ed.jpg', 635, 'post', '2019-02-01', '2019-02-01', 0),
(636, '/media/category/3366a9ebcb7eb621f8444410cfd5b289.jpg', 636, 'post', '2019-02-01', '2019-02-01', 0),
(637, '/media/category/f116a45530f052125a25d45f94baa7d9.jpg', 637, 'post', '2019-02-01', '2019-02-01', 0),
(638, '/media/category/38a8bb1722a55d66ffdff2f89c13314c.jpg', 638, 'post', '2019-02-01', '2019-02-01', 0),
(639, '/media/category/815cd7b72c74d910cd690a1426ad2374.jpg', 639, 'post', '2019-02-01', '2019-02-01', 0),
(640, '/media/category/4cb2e3f272743d333dc1c341012542cf.jpg', 640, 'post', '2019-02-01', '2019-02-01', 0),
(641, '/media/category/a5a37c7a4487f26354b0061dcfc855de.jpg', 641, 'post', '2019-02-01', '2019-02-01', 0),
(642, '/media/category/2ac448d0953587a3564a7e1c71480fab.jpg', 642, 'post', '2019-02-01', '2019-02-01', 0),
(643, '/media/category/782073b4d0635fccd336bcddaf09da9b.jpg', 643, 'post', '2019-02-01', '2019-02-01', 0),
(644, '/media/category/d721c29826cec643524c2f36ae8ce874.jpg', 644, 'post', '2019-02-01', '2019-02-01', 0),
(645, '/media/category/76b29dd4ee1fe0af6ba7d2d00ce75d4d.jpg', 645, 'post', '2019-02-01', '2019-02-01', 0),
(646, '/media/category/bd6957d94235eb76b5da78c567cb625e.jpg', 646, 'post', '2019-02-01', '2019-02-01', 0),
(647, '/media/category/beb994639119721593ee091c52343247.jpg', 647, 'post', '2019-02-01', '2019-02-01', 0),
(648, '/media/category/a401c530ad1b472b6004f5d8a017e883.jpg', 648, 'post', '2019-02-01', '2019-02-01', 0),
(649, '/media/category/cc11e68be6696dcfc59c882641200ae5.jpg', 649, 'post', '2019-02-01', '2019-02-01', 0),
(650, '/media/category/8e5ac95385ccd28edf462fc5ea54d177.jpg', 650, 'post', '2019-02-01', '2019-02-01', 0),
(651, '/media/category/d4838cc6a1de08783059b0a9dcfa9028.jpg', 651, 'post', '2019-02-01', '2019-02-01', 0),
(652, '/media/category/adc0fff73e1ef4cabadbf33fa833d155.jpg', 652, 'post', '2019-02-01', '2019-02-01', 0),
(653, '/media/category/14dd078fec5133bccd962856676a451a.jpg', 653, 'post', '2019-02-01', '2019-02-01', 0),
(654, '/media/category/390d9dfa7e1bd34067dafaed6dd37fe9.jpg', 654, 'post', '2019-02-01', '2019-02-01', 0),
(655, '/media/category/364bcde47de6c5d63c2b4765da0a6860.jpg', 655, 'post', '2019-02-01', '2019-02-01', 0),
(656, '/media/category/563458fd55f06f80e01f6725be0dee99.jpg', 656, 'post', '2019-02-01', '2019-02-01', 0),
(657, '/media/category/8357f4b9496967520fd3f1a67a5afa77.jpg', 657, 'post', '2019-02-01', '2019-02-01', 0),
(658, '/media/category/ddbcb79f92c98c55376d44f7c2941e71.jpg', 658, 'post', '2019-02-01', '2019-02-01', 0),
(659, '/media/category/ad4383c97fafe953440dbf0a6ebc6ab3.jpg', 659, 'post', '2019-02-01', '2019-02-01', 0),
(660, '/media/category/d90028db81c09a28767f110e3a487833.jpg', 660, 'post', '2019-02-01', '2019-02-01', 0),
(661, '/media/category/b8c67dc79b32a99ebc799a2c6b85582d.jpg', 661, 'post', '2019-02-01', '2019-02-01', 0),
(662, '/media/category/ed7be5a6ba999eb55a05df7810752b47.jpg', 662, 'post', '2019-02-01', '2019-02-01', 0),
(663, '/media/category/0e6ce65a8943078473ec4be4f90eee2b.jpg', 663, 'post', '2019-02-01', '2019-02-01', 0),
(664, '/media/category/3670a3016a53936815acc6b68b6cbf22.jpg', 664, 'post', '2019-02-01', '2019-02-01', 0),
(665, '/media/category/47542ed2be3075253b15162d594bdd77.jpg', 665, 'post', '2019-02-01', '2019-02-01', 0),
(666, '/media/category/5efe7f3b90b2fa663ecf177cebab9a31.jpg', 666, 'post', '2019-02-01', '2019-02-01', 0),
(667, '/media/category/8033e470d38f9f75b4250699d85ca7d5.jpg', 667, 'post', '2019-02-01', '2019-02-01', 0),
(668, '/media/category/5266a01b3a15e84133b71f1fc9f7ad3b.jpg', 668, 'post', '2019-02-01', '2019-02-01', 0),
(669, '/media/category/674f08c251853f6215794d508c79bfbc.jpg', 669, 'post', '2019-02-01', '2019-02-01', 0),
(670, '/media/category/88e12dafff40e7ae5b1887e42df77945.jpg', 670, 'post', '2019-02-01', '2019-02-01', 0),
(671, '/media/category/1db99f1022391d557a9a6233309c5c12.jpg', 671, 'post', '2019-02-01', '2019-02-01', 0),
(672, '/media/category/5e2f84c6628324d1211b995a5fed7f8d.jpg', 672, 'post', '2019-02-01', '2019-02-01', 0),
(673, '/media/category/9b9a27ab2691212b562f405aba410143.jpg', 673, 'post', '2019-02-01', '2019-02-01', 0),
(674, '/media/category/737c9972a83818a41d13eed9062702f0.jpg', 674, 'post', '2019-02-01', '2019-02-01', 0),
(675, '/media/category/6fb4bb95b438f30e0ac69723be921070.jpg', 675, 'post', '2019-02-01', '2019-02-01', 0),
(676, '/media/category/c90cb752a27895520a61baf667d0a93f.jpg', 676, 'post', '2019-02-01', '2019-02-01', 0),
(677, '/media/category/b09ec3f3f4f58f0297f53a8e61099849.jpg', 677, 'post', '2019-02-01', '2019-02-01', 0),
(678, '/media/category/665f81eb058f7a0f18f53890a92f0452.jpg', 678, 'post', '2019-02-01', '2019-02-01', 0),
(679, '/media/category/6e6405e455a2879f8e870fdbf5ef550b.jpg', 679, 'post', '2019-02-01', '2019-02-01', 0),
(680, '/media/category/462284d705b25da592a689c2de4a6389.jpg', 680, 'post', '2019-02-01', '2019-02-01', 0),
(681, '/media/category/30fc36e13c32428ba654dc6d86f815c3.jpg', 681, 'post', '2019-02-01', '2019-02-01', 0),
(682, '/media/category/cc6ff4302598154c95e969ed855289b9.jpg', 682, 'post', '2019-02-01', '2019-02-01', 0),
(683, '/media/category/9333add8db7241fa6da172971ca5e093.jpg', 683, 'post', '2019-02-01', '2019-02-01', 0),
(684, '/media/category/819b860f5d35d3e6154ca7d0f419f7c5.jpg', 684, 'post', '2019-02-01', '2019-02-01', 0),
(685, '/media/category/b03d09788bd630ab1388db5db765902c.jpg', 685, 'post', '2019-02-01', '2019-02-01', 0),
(686, '/media/category/1403a3382f74aaa9e70384d82ef01b75.jpg', 686, 'post', '2019-02-01', '2019-02-01', 0),
(687, '/media/category/f0e4d3e76c0d415bd30ba8fb3df12c6d.jpg', 687, 'post', '2019-02-01', '2019-02-01', 0),
(688, '/media/category/215f07f19405d6fda190b3e22a9b830c.jpg', 688, 'post', '2019-02-01', '2019-02-01', 0),
(689, '/media/category/e451c49eb42b049494e9e792dfbe78fa.jpg', 689, 'post', '2019-02-01', '2019-02-01', 0),
(690, '/media/category/b14bda13031c6209fee210d76839dd77.jpg', 690, 'post', '2019-02-01', '2019-02-01', 0),
(691, '/media/category/dd5b28df1d38afe3171b0b836e9eb7e4.jpg', 691, 'post', '2019-02-01', '2019-02-01', 0),
(692, '/media/category/2bde9aba7adaaa92e0fe4a1daa972910.jpg', 692, 'post', '2019-02-01', '2019-02-01', 0),
(693, '/media/category/3a76fd5cfc86c646a012f79db20eaec1.jpg', 693, 'post', '2019-02-01', '2019-02-01', 0),
(694, '/media/category/8d56662916ddc464f804d196248faa6d.jpg', 694, 'post', '2019-02-01', '2019-02-01', 0),
(695, '/media/category/990219d0d70b643c4274b448a5688587.jpg', 695, 'post', '2019-02-01', '2019-02-01', 0),
(696, '/media/category/1069bd28ed786ed8c0ed046d009ae1a4.jpg', 696, 'post', '2019-02-01', '2019-02-01', 0),
(697, '/media/category/d4efb1826a38ce561e8b4bba23aed808.jpg', 697, 'post', '2019-02-01', '2019-02-01', 0),
(698, '/media/category/5e6eafc511c8e5c9dc337efd8a7698d0.jpg', 698, 'post', '2019-02-01', '2019-02-01', 0),
(699, '/media/category/ed1432f014b614a509d3e81ed14b1291.jpg', 699, 'post', '2019-02-01', '2019-02-01', 0),
(700, '/media/category/c5f89ee4b158ac571a03058f55c413e9.jpg', 700, 'post', '2019-02-01', '2019-02-01', 0),
(701, '/media/category/cb583e37207148a146f72c7b362419b3.jpg', 701, 'post', '2019-02-01', '2019-02-01', 0),
(702, '/media/category/1b231f1258682712d45a3ab49383230b.jpg', 702, 'post', '2019-02-01', '2019-02-01', 0),
(703, '/media/category/bfe7043c9696df21f7cf8c461d88d82d.jpg', 703, 'post', '2019-02-01', '2019-02-01', 0),
(704, '/media/category/674fa51791b2f3cb6e500fa756c88878.jpg', 704, 'post', '2019-02-01', '2019-02-01', 0),
(705, '/media/category/532906b9429a7b65fde23535406e0b0d.jpg', 705, 'post', '2019-02-01', '2019-02-01', 0),
(706, '/media/category/57a89f716a0ade5bf39dd86f8e432e8c.jpg', 706, 'post', '2019-02-01', '2019-02-01', 0),
(707, '/media/category/51fc318de526515d01cc9774508e7b9d.jpg', 707, 'post', '2019-02-01', '2019-02-01', 0),
(708, '/media/category/4f31ab507e51edff12a34bd8a5d29f3a.jpg', 708, 'post', '2019-02-01', '2019-02-01', 0),
(709, '/media/category/9d63c1723a2e2968b77a1f922767a1da.jpg', 709, 'post', '2019-02-01', '2019-02-01', 0),
(710, '/media/category/fe0b4af6ba7377c670cb0660cf13be39.jpg', 710, 'post', '2019-02-01', '2019-02-01', 0),
(711, '/media/category/fa0a759ee77ed6d3ec31a746c4506b43.jpg', 711, 'post', '2019-02-01', '2019-02-01', 0),
(712, '/media/category/913393eabddd42a38bb12b5801a4749e.jpg', 712, 'post', '2019-02-01', '2019-02-01', 0),
(713, '/media/category/7f985be0496af116f9557a8c8992b0d3.jpg', 713, 'post', '2019-02-01', '2019-02-01', 0),
(714, '/media/category/21ec428b6dec9b55b6343c7153092c4e.jpg', 714, 'post', '2019-02-01', '2019-02-01', 0),
(715, '/media/category/9907eb701a1e3c7e6491a146f7bc518c.jpg', 715, 'post', '2019-02-01', '2019-02-01', 0),
(716, '/media/category/26716f747f88044bf67b7fa895a9f734.jpg', 716, 'post', '2019-02-01', '2019-02-01', 0),
(717, '/media/category/4ed558cc2768e1cad9a13da117d95318.jpg', 717, 'post', '2019-02-01', '2019-02-01', 0),
(718, '/media/category/d2df8c6329d856f872e377519e8b8916.jpg', 718, 'post', '2019-02-01', '2019-02-01', 0),
(719, '/media/category/b29ce667e115ea50b38a732453b66204.jpg', 719, 'post', '2019-02-01', '2019-02-01', 0),
(720, '/media/category/561d8e592e5904f3cccbae2c0047e44c.jpg', 720, 'post', '2019-02-01', '2019-02-01', 0),
(721, '/media/category/a612cfde85f7a6469a2a9ead24b16329.jpg', 721, 'post', '2019-02-01', '2019-02-01', 0),
(722, '/media/category/3bd48cef6809e2aea0915f6d4c0feb98.jpg', 722, 'post', '2019-02-01', '2019-02-01', 0),
(723, '/media/category/5938ebb18c1da76384ef9ba831265dc8.jpg', 723, 'post', '2019-02-01', '2019-02-01', 0),
(724, '/media/category/b812a3fdbeb9c0d291250bdd88862516.jpg', 724, 'post', '2019-02-01', '2019-02-01', 0),
(725, '/media/category/575f2373357aa5bc67f1dac1a27a1ad4.jpg', 725, 'post', '2019-02-01', '2019-02-01', 0),
(726, '/media/category/9d35a50c6dae2dc7f851cfff679c71b7.jpg', 726, 'post', '2019-02-01', '2019-02-01', 0),
(727, '/media/category/7e1633b13fe29ff234363eb9a5464a23.jpg', 727, 'post', '2019-02-01', '2019-02-01', 0),
(728, '/media/category/5777fbbcc5a496254d3fb1db77148831.jpg', 728, 'post', '2019-02-01', '2019-02-01', 0),
(729, '/media/category/3ac3b6e6afdf669f9b64a8dc463fcc9d.jpg', 729, 'post', '2019-02-01', '2019-02-01', 0),
(730, '/media/category/278bc91d86ea644e2ae173e97f0bdfa9.jpg', 730, 'post', '2019-02-01', '2019-02-01', 0),
(731, '/media/category/b35872ece9c38c8f6fd99ba836dc5939.jpg', 731, 'post', '2019-02-01', '2019-02-01', 0),
(732, '/media/category/0ef8042f63ea234a5f71deb8186941cc.jpg', 732, 'post', '2019-02-01', '2019-02-01', 0),
(733, '/media/category/9d8486015281388fdafbfa6dc9dac7c8.jpg', 733, 'post', '2019-02-01', '2019-02-01', 0),
(734, '/media/category/e39c5e7973f739aca642150a03b96693.jpg', 734, 'post', '2019-02-01', '2019-02-01', 0),
(735, '/media/category/c567d1c86734485ae4034260d0260c37.jpg', 735, 'post', '2019-02-01', '2019-02-01', 0),
(736, '/media/category/7f33dadbdb29fb2a7d0cc4e217316473.jpg', 736, 'post', '2019-02-01', '2019-02-01', 0),
(737, '/media/category/0d986d79af866a1466a89aa2587bf942.jpg', 737, 'post', '2019-02-01', '2019-02-01', 0),
(738, '/media/category/4285b44cf4fc485fe59e661b1ec27b5e.jpg', 738, 'post', '2019-02-01', '2019-02-01', 0),
(739, '/media/category/613a3d6a65d8311f7860f62a52c06868.jpg', 739, 'post', '2019-02-01', '2019-02-01', 0),
(740, '/media/category/f2dbb8c3368b5517f14fe859a0a0fff4.jpg', 740, 'post', '2019-02-01', '2019-02-01', 0),
(741, '/media/category/b932b068b5f4f116d7bede5d179f16d5.jpg', 741, 'post', '2019-02-01', '2019-02-01', 0),
(742, '/media/category/c9fd2259849b866d3adcef2abb5ca3d9.jpg', 742, 'post', '2019-02-01', '2019-02-01', 0),
(743, '/media/category/724bd75a48b09b429dc82f896b6bc63f.jpg', 743, 'post', '2019-02-01', '2019-02-01', 0),
(744, '/media/category/22406908ef02d24588c544108fcf53f8.jpg', 744, 'post', '2019-02-01', '2019-02-01', 0),
(745, '/media/category/b80e53490967a0548edb61e985863a88.jpg', 745, 'post', '2019-02-01', '2019-02-01', 0),
(746, '/media/category/7b0d3cf7cdeb29321d48c4a19d06154b.jpg', 746, 'post', '2019-02-01', '2019-02-01', 0),
(747, '/media/category/0b5f8c81e9243e70ea15710b680df1cf.jpg', 747, 'post', '2019-02-01', '2019-02-01', 0),
(748, '/media/category/1c2469da1fe1a3c2b5953ae098ea39b5.jpg', 748, 'post', '2019-02-01', '2019-02-01', 0),
(749, '/media/category/ab5f1028538f84c096ab0fd995adbf68.jpg', 749, 'post', '2019-02-01', '2019-02-01', 0),
(750, '/media/category/a30ed427c964d4f023d242990f274fdf.jpg', 750, 'post', '2019-02-01', '2019-02-01', 0),
(751, '/media/category/3c69eb007180cbdb026961453fd46ab4.jpg', 751, 'post', '2019-02-01', '2019-02-01', 0),
(752, '/media/category/80ca1fd1bf2b4a06c621812975532bc9.jpg', 752, 'post', '2019-02-01', '2019-02-01', 0),
(753, '/media/category/71f8a6b2646cc4d961d3ead73195bf21.jpg', 753, 'post', '2019-02-01', '2019-02-01', 0),
(754, '/media/category/fdcc26392d0d11f9459ec8bd1d71cadc.jpg', 754, 'post', '2019-02-01', '2019-02-01', 0),
(755, '/media/category/38d5e231352f8cba59dea93a0f26e1ea.jpg', 755, 'post', '2019-02-01', '2019-02-01', 0),
(756, '/media/category/77b7d565b5ae3de6134d99dd0e446653.jpg', 756, 'post', '2019-02-01', '2019-02-01', 0),
(757, '/media/category/f3dc9058c53b16078e39c8f3b0d06a37.jpg', 757, 'post', '2019-02-01', '2019-02-01', 0),
(758, '/media/category/6bbfd6bfc6e988f2111a1796c3da4b70.jpg', 758, 'post', '2019-02-01', '2019-02-01', 0),
(759, '/media/category/701bbc1de8fd22d9e22c9fc3b86e8bd0.jpg', 759, 'post', '2019-02-01', '2019-02-01', 0),
(760, '/media/category/1489190e362d7f2566bdfd6f8a2de4b6.jpg', 760, 'post', '2019-02-01', '2019-02-01', 0),
(761, '/media/category/7088e2e8927517c004199e608ed80f30.jpg', 761, 'post', '2019-02-01', '2019-02-01', 0),
(762, '/media/category/cd0e8f58e08f2d2b021e57b5ce92f49e.jpg', 762, 'post', '2019-02-01', '2019-02-01', 0),
(763, '/media/category/3e511b10b7d51fb733ad5668ccd37d29.jpg', 763, 'post', '2019-02-01', '2019-02-01', 0),
(764, '/media/category/b8d334b85e9462fce4b5b1431324ffef.jpg', 764, 'post', '2019-02-01', '2019-02-01', 0),
(765, '/media/category/5131f786d0e9331b823ed32628f6ccd5.jpg', 765, 'post', '2019-02-01', '2019-02-01', 0),
(766, '/media/category/a7b5252e36c739e979b2fdc78b5baee3.jpg', 767, 'post', '2019-02-01', '2019-02-01', 0),
(767, '/media/about/f5d5c668ab178cbf.jpg', 9, 'about', '2019-02-03', '2019-02-03', 0),
(768, '/media/about/605af1ef7e1d7c38.jpg', 9, 'about', '2019-02-03', '2019-02-03', 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `status` enum('new','pending','pay') NOT NULL,
  `show` tinyint(1) NOT NULL,
  `total` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `payment_type` enum('bank','cash') NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `status`, `show`, `total`, `qty`, `payment_type`, `user_id`) VALUES
(85, 'new', 1, 12600, 2, 'cash', 1),
(86, 'pending', 1, 4600, 3, 'cash', 1),
(87, 'pending', 1, 19000, 1, 'cash', 1);

-- --------------------------------------------------------

--
-- Table structure for table `order_billing`
--

CREATE TABLE `order_billing` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `phone` varchar(128) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `diff_address` tinyint(1) DEFAULT NULL,
  `order_note` longtext,
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_billing`
--

INSERT INTO `order_billing` (`id`, `first_name`, `last_name`, `email`, `phone`, `district_id`, `address`, `diff_address`, `order_note`, `order_id`) VALUES
(31, 'david2', 'davo121992', 'davidmkrtchyan1992@mail.ru', '+37477850709', NULL, '', 0, '', 85),
(32, 'david2', 'davo121992', 'davidmkrtchyan1992@mail.ru', '+37477850709', NULL, '', 0, '', 86),
(33, 'david2', 'davo121992', 'davidmkrtchyan1992@mail.ru', '+37477850709', NULL, '', 0, '', 87);

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_products`
--

INSERT INTO `order_products` (`id`, `price`, `qty`, `order_id`, `post_id`) VALUES
(166, 6300, 2, 85, 614),
(167, 1350, 1, 86, 619),
(168, 1750, 1, 86, 632),
(169, 1500, 1, 86, 626),
(170, 19000, 1, 87, 617);

-- --------------------------------------------------------

--
-- Table structure for table `personal_post`
--

CREATE TABLE `personal_post` (
  `id` int(11) NOT NULL,
  `hourly_fee` varchar(255) DEFAULT NULL,
  `phone_number` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `content_en` longtext,
  `publish` date NOT NULL,
  `status` tinyint(1) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `price` decimal(12,2) DEFAULT NULL,
  `content_hy` longtext,
  `content_ru` longtext,
  `title_en` varchar(250) DEFAULT NULL,
  `title_hy` varchar(250) DEFAULT NULL,
  `title_ru` varchar(250) DEFAULT NULL,
  `order` int(10) UNSIGNED DEFAULT NULL,
  `popular` tinyint(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `content_en`, `publish`, `status`, `slug`, `category_id`, `user_id`, `price`, `content_hy`, `content_ru`, `title_en`, `title_hy`, `title_ru`, `order`, `popular`) VALUES
(534, '3 pcs, onion, greens, lavash', '2019-02-01', 1, 'pork-loin', 34, 1, '3150.00', '3 կտոր, սոխ, կանաչի, լավաշ', '3 куска, лук, зелень, лаваш', 'Pork loin', 'Խոզի չալաղաջ', 'Kорейкa свинины', 34, 0),
(535, '4 pcs, onion, greens, lavash', '2019-02-01', 1, 'pork-ribs', 34, 1, '2500.00', '4 կտոր, սոխ, կանաչի, լավաշ', '4 куска, лук, зелень, лаваш', 'Pork ribs', 'Խոզի կողիկներ', 'Шашлык из ребрышки свинины', 35, 0),
(536, '5 pcs, onion, greens, lavash', '2019-02-01', 1, 'pork-tenderloin-barbecue', 34, 1, '2800.00', '5 կտոր, սոխ, կանաչի, լավաշ', '5 кусков, лук, зелень, лаваш', 'Pork tenderloin barbecue', 'Խոզի փափուկ խորոված', 'Шашлык из свинины', 36, 0),
(537, 'Beef tenderloin - 5 pcs, onion, greens, lavash', '2019-02-01', 1, 'beef-tenderloin-barbecue', 34, 1, '3300.00', 'Տավարի /սուկի/ - 5 կտոր, սոխ, կանաչի, լավաշ', 'Корейка говядины - 5 кусков, лук, зелень, лаваш', 'Beef tenderloin barbecue', 'Տավարի խորոված', 'Шашлык из корейки говядины', 37, 0),
(538, '6-7 pcs, lavash, onion, greens, if served in lavash - ketchup and mayonnaise', '2019-02-01', 1, 'beef-heart-barbecue', 34, 1, '1050.00', '6-7 կտոր, լավաշ, սոխ, կանաչի, բրդուճի մեջ նաև` կետչուպ ու մայոնեզ', '6-7 кусков, лаваш, лук, зелень, в завернутом лаваше - с кетчупом и майонезом', 'Beef heart barbecue', 'Տավարի սրտի խորոված', 'Шашлык из говяжьего сердца', 38, 0),
(539, '6-7 pcs, lavash, onion, greens, if served in lavash - ketchup and mayonnaise', '2019-02-01', 1, 'beef-lung-barbecue', 34, 1, '1000.00', '6-7 կտոր, լավաշ, սոխ, կանաչի, բրդուճի մեջ նաև` կետչուպ ու մայոնեզ', '6-7 кусков, лаваш, лук, зелень, завернутый в лаваш - кетчуп и майонез', 'Beef lung barbecue', 'Տավարի թոքի խորոված', 'Шашлык из говяжьих легких', 39, 0),
(540, '4-5 pcs, lavash, onion, greens', '2019-02-01', 1, 'lamb-loin-barbecue', 34, 1, '3150.00', '4-5 կտոր, լավաշ, սոխ, կանաչի', '4-5 кусков, лаваш, лук, зелень', 'Lamb loin barbecue', 'Գառան չալաղաջի խորոված', 'Шашлык из корейки баранины', 40, 0),
(541, 'Lamb parda in natural membrane, lavash, onion, greens, if served in lavash - ketchup, mayonnaise', '2019-02-01', 1, 'lamb-parda', 34, 1, '1250.00', 'Գառան թոք, սիրտ, փայծաղ՝ աղիքային փառի մեջ փաթաթած, լավաշ, սոխ, կանաչի, բրդուճի դեպքում նաև կետչուպ', 'Бараньи потроха в натуральной оболочке, лаваш, лук, зелень, завернутый в лаваш - кетчуп, майонез', 'Lamb parda', 'Գառան փարդա', 'Барашья парда', 41, 0),
(542, '4 small pcs of lavash, onion, greens, white sauce', '2019-02-01', 1, 'grill', 34, 1, '3300.00', '4 փոքր լավաշ, սոխ, կանաչի, սպիտակ սոուս', '4 маленьких лаваша, лук, зелень, белый соус', 'Grill', 'Գրիլ', 'Гриль', 42, 0),
(543, '4 pcs, onion, greens, lavash', '2019-02-01', 1, 'chicken-wing-barbecue', 34, 1, '1600.00', '4 կտոր, սոխ, կանաչի, լավաշ', '4 куска, лук, зелень, лаваш', 'Chicken Wing barbecue', 'Հավի թևիկների խորոված', 'Шашлык из куриных крылышек', 43, 0),
(544, '5 pcs, onion, greens, lavash', '2019-02-01', 1, 'chicken-breast-barbecue', 34, 1, '1700.00', '5 կտոր, սոխ, կանաչի, լավաշ', '5 кусков, лук, зелень, лаваш', 'Chicken breast barbecue', 'Հավի կրծքամսի խորոված', 'Шашлык из грудки курицы', 44, 0),
(545, '6-7 pcs, onion, greens, lavash', '2019-02-01', 1, 'chicken-thigh-barbecue', 34, 1, '1550.00', 'թևի վերևի հատված, 6 - 7 կտոր, սոխ, կանաչի, լավաշ', '6-7 куска, лук, зелень, лаваш', 'Chicken thigh barbecue', 'Հավի բդիկի խորոված', 'Шашлык из куриных окорочков', 45, 0),
(546, '1 chicken, divided in 2 parts, 5-6 pcs, roasted potatoes, onion, greens, lavash', '2019-02-01', 1, 'chicken-barbecue-with-potatoes', 34, 1, '1900.00', '1 ճուտ՝ 2 մասի բաժանված, 5-6 կտոր, պլեճ կարտոֆիլ, սոխ, կանաչի, լավաշ', '1 цыпленок, разделенный на 2 части, 5-6 кусков запеченный картофель, лук, зелень, лаваш', 'Chicken barbecue with potatoes', 'Ճտի և կարտոֆիլի խորոված', 'Шашлык из цыпленка с картофелем', 46, 0),
(547, '6-7 pcs, lavash, 1 lemon', '2019-02-01', 1, 'trout-whole', 34, 1, '4900.00', '6-7 կտոր, լավաշ, 1 կիտրոն', '6-7 кусков, лаваш, 1 лимон', 'Trout (whole)', 'Իշխանի խորոված, ամբողջական', 'Форель (целая)', 47, 0),
(548, '4-5 pcs, lavash, half of a lemon', '2019-02-01', 1, 'trout-1-skewer', 34, 1, '2100.00', '4-5 կտոր, լավաշ, կես կիտրոն', '4-5 кусков, лаваш, половинка лимона', 'Trout (1 skewer)', 'Իշխանի խորոված, 1 շիշ', 'Форель (1 шампур)', 48, 0),
(549, '9-11 pcs, lavasհ, 1 lemon', '2019-02-01', 1, 'sterlet-whole', 34, 1, '11900.00', '9-11 կտոր, լավաշ, 1 հատ կիտրոն', '9-11 кусков, лаваш,1 лимон', 'Sterlet (whole)', 'Ստերլետ խորոված, ամբողջական', 'Стерлядь (целая)', 49, 0),
(550, '4-5 pcs, lavash, half of a lemon', '2019-02-01', 1, 'sterlet-1-skewer', 34, 1, '3150.00', '4-5 կտոր, լավաշ, կես կիտրոն', '4-5 кусков, лаваш, половинка лимона', 'Sterlet (1 skewer)', 'Ստերլետի խորոված, 1 շիշ', 'Стерлядь (1 шампур)', 50, 0),
(551, 'Lavash, onion, greens; if served in lavash - ketchup, mayonnaise', '2019-02-01', 1, 'pork-iqibir', 34, 1, '1050.00', 'Լավաշ, սոխ, կանաչի, բրդուճի մեջ նաև` կետչուպ ու մայոնեզ', 'Лаваш, лук, зелень; в завернутом виде - кетчуп и майонез', 'Pork Iqibir', 'Խոզի իքիբիր', 'Икибир из свинины', 51, 0),
(552, 'Lavash, onion, greens; if served in lavash - ketchup, mayonnaise', '2019-02-01', 1, 'chicken-iqibir', 34, 1, '850.00', 'Լավաշ, սոխ, կանաչի,բրդուճի մեջ նաև կետչուպ ու մայոնեզ', 'Лаваш, лук, зелень; в завернутом виде - кетчуп и майонез', 'Chicken Iqibir', 'Հավի իքիբիր', 'Икибир из курицы', 52, 0),
(553, 'Lavash, onion, greens; if served in lavash - ketchup', '2019-02-01', 1, 'lamb-iqibir', 34, 1, '1200.00', 'Լավաշ, սոխ, կանաչի, բրդուճի մեջ նաև` կետչուպ', 'Лаваш, лук, зелень, в завернутом виде - кетчуп', 'Lamb Iqibir', 'Գառան Իքիբիր', 'Икибир из баранины', 53, 0),
(554, 'Grounded chicken with cheddar, lavash, onion, greens; if served in lavash - ketchup and mayonnaise', '2019-02-01', 1, 'kebab-karas', 34, 1, '950.00', 'Հավի ֆարշի մեջ հոլանդական պանիր, լավաշ, սոխ, կանաչի, բրդուճի մեջ նաև՝ կետչուպ ու մայոնեզ', 'Фарш из куриного мяса с голландским сыром, лаваш, лук, зелень; завернутое в лаваш - кетчуп и майонез', 'Kebab \"Karas\"', 'Քյաբաբ Կարաս', 'Кебаб \"Карас\"', 54, 0),
(555, 'Lavash, onion, greens; if served in lavash - ketchup, mayonnaise', '2019-02-01', 1, 'beef-kebab', 34, 1, '850.00', 'Լավաշ, սոխ, կանաչի, բրդուճի մեջ նաև` կետչուպ ու մայոնեզ', 'Лаваш, лук, зелень, в завернутом лаваше - кетчуп, майонез', 'Beef kebab', 'Տավարի քյաբաբ', 'Говяжий кебаб', 55, 0),
(556, 'Lavash, onion, greens; if served in lavash - ketchup, mayonnaise', '2019-02-01', 1, 'chicken-kebab', 34, 1, '800.00', 'Լավաշ, սոխ, կանաչի, բրդուճի մեջ նաև կետչուպ ու մայոնեզ', 'Лаваш, лук, зелень, в завернутом лаваше так же - кетчуп, майонез', 'Chicken kebab', 'Հավի քյաբաբ', 'Куриный кебаб', 56, 0),
(557, '<p>Served in 35 sm bread; onion, greens, ketchup, mayonnaise, tomato, french fries, hot green pepper</p>', '2019-02-01', 1, 'extra-kebab-karas', 34, 1, '1100.00', '<p>Մատուցվում է 35սմ հացով՝ սոխ, կանաչի, կետչուպ, մայոնեզ, լոլիկ, ֆրի, թարմ կծու բիբար</p>', '<p>Подается в 35 см хлебе; лук, зелень, кетчуп, майонез, помидоры, картофель фри, острый зеленый перец</p>', 'Extra kebab \"Karas\"', 'Էքստրա քյաբաբ Կարաս', 'Экстра кебаб \"Карас\"', 57, 1),
(558, 'Served in 35 sn bread; onion, greens, ketchup, mayonnaise, tomatoes, french fries, hot green pepper', '2019-02-01', 1, 'extra-beef-kebab', 34, 1, '1000.00', 'Մատուցվում է 35սմ հացով՝ սոխ, կանաչի, կետչուպ, մայոնեզ, լոլիկ, ֆրի, թարմ կծու բիբար', 'Подается в 35 см хлебе; лук, зелень, кетчуп, майонез, помидоры, картофель фри, острый зеленый перец', 'Extra beef kebab', 'էքստրա քյաբաբ տավարի', 'Говяжий экстра кебаб', 58, 0),
(559, '<p>Served in 35 sm bread; onion, greens, ketchup, mayonnaise, tomatoes, french fries, green hot pepper</p>', '2019-02-01', 1, 'chicken-extra-kebab', 34, 1, '900.00', '<p>Մատուցվում է 35 սմ հացով՝ սոխ, կանաչի, կետչուպ, մայոնեզ, լոլիկ, ֆրի, թարմ կծու բիբար</p>', '<p>Подается в 35 см хлебе; лук, зелень, кетчуп, майонез, помидоры,картофель фри, зеленый острый перец</p>', 'Chicken extra kebab', 'Էքստրա քյաբաբ հավի', 'Куриный экстра кебаб', 59, 1),
(560, '4-5 champignons, 1 small lavash, greens', '2019-02-01', 1, 'mushrooms-barbeque', 34, 1, '750.00', '4-5 շամպինիոն սունկ, 1 փոքր լավաշ, կանաչի', '4-5 шампиньонов, 1 маленький лаваш, зелень', 'Mushrooms barbeque', 'Խորոված սունկ', 'Шашлык из грибов', 60, 0),
(561, '10 - 12 slices of potato, lavash, onion, greens; if served in lavash - ketchup, greens', '2019-02-01', 1, 'potatoes-plech', 34, 1, '400.00', '10 -12 կտոր բարակ շերտերով կարտոֆիլ, լավաշ, սոխ, կանաչի, բրդուճի մեջ նաև` կետչուպ, կանաչի', '10 - 12 тонких ломтиков картофеля, лаваш, лук, зелень; если завернуто - кетчуп, зелень', 'Potatoes plech', 'Կարտոֆիլ պլեճ', 'Картофель плеч', 61, 0),
(562, '<p>5-6 thick slices of potatoes and fat, lavash, onion, greens; if served in lavash - ketchup and mayonnaise</p>', '2019-02-01', 1, 'potatoes-with-fat', 34, 1, '550.00', '<p>5-6 կտոր հաստ շերտերով կարտոֆիլ՝ մեկումեջ սալի շարվածքով, լավաշ, սոխ, կանաչի, բրդուճի մեջ նաև՝ կետչուպ, մայոնեզ</p>', '<p>5-6 больших ломтей картофеля, чередованных с салом, лаваш, лук, зелень, в завернутом виде - кетчуп и майонез</p>', 'Potatoes with fat', 'Խորոված կարտոֆիլ սալով', 'Картофель с салом', 62, 1),
(563, 'Tan Karas 0,25l', '2019-02-01', 1, 'tan-karas-0-25l', 35, 1, '250.00', 'Թան Կարաս 0,25լ', 'Тан Карас 0,25л', 'Tan Karas 0,25l', 'Թան Կարաս 0,25լ', 'Тан Карас 0,25л', 35, 0),
(564, 'Tan Karas 0,5l', '2019-02-01', 1, 'tan-karas-0-5l', 35, 1, '350.00', 'Թան Կարաս 0,5լ', 'Тан Карас 0,5л', 'Tan Karas 0,5l', 'Թան Կարաս 0,5լ', 'Тан Карас 0,5л', 36, 0),
(565, 'Okroshka Karas, 0.5l', '2019-02-01', 1, 'okroshka-karas-0-5l', 35, 1, '450.00', 'Մածնաբրդոշ Կարաս, 0.5լ', 'Окрошка Карас, 0.5л', 'Okroshka Karas, 0.5l', 'Մածնաբրդոշ Կարաս, 0.5լ', 'Окрошка Карас, 0.5л', 37, 0),
(566, 'Homemade compotes, assortment, 0.25l', '2019-02-01', 1, 'homemade-compotes-assortment-0-25l', 35, 1, '300.00', 'Տնական կոմպոտներ, տեսականի, 0.25լ', 'Домашние компоты, ассортимент, 0.25л', 'Homemade compotes, assortment, 0.25l', 'Տնական կոմպոտներ, տեսականի, 0.25լ', 'Домашние компоты, ассортимент, 0.25л', 38, 0),
(567, '1', '2019-02-01', 1, 'sprite-0-25l', 35, 1, '1.00', '1', '1', 'Sprite 0.25l', 'Սփրայթ 0.25լ', 'Спрайт 0.25л', 39, 0),
(568, '.', '2019-02-01', 1, 'coca-cola-0-25l', 35, 1, '1.00', '.', '.', 'Coca-cola 0.25l', 'Կոկա-Կոլա 0.25լ', 'Кока-кола 0.25л', 40, 0),
(569, ':', '2019-02-01', 1, 'fanta-0-25l', 35, 1, '1.00', ':', ':', 'Fanta 0.25l', 'Ֆանտա 0.25լ', 'Фанта 0.25л', 41, 0),
(570, '.', '2019-02-01', 1, 'sprite-0-5l', 35, 1, '450.00', '.', '.', 'Sprite 0,5l', 'Սփրայթ 0,5լ', 'Спрайт 0,5л', 42, 0),
(571, '.', '2019-02-01', 1, 'coca-cola-0-5l', 35, 1, '450.00', '.', '.', 'Coca-Cola 0,5l', 'Կոկա-կոլա 0,5լ', 'Кока-кола 0,5л', 43, 0),
(572, '.', '2019-02-01', 1, 'fanta-0-5l', 35, 1, '450.00', '.', '.', 'Fanta 0,5l', 'Ֆանտա 0,5լ', 'Фанта 0,5л', 44, 0),
(573, '.', '2019-02-01', 1, 'sprite-1l', 35, 1, '550.00', '.', '.', 'Sprite 1l', 'Սփրայթ 1լ', 'Спрайт 1л', 45, 0),
(574, '.', '2019-02-01', 1, 'coca-cola-1l', 35, 1, '550.00', '.', '.', 'Coca-Cola 1l', 'Կոկա-կոլա 1լ', 'Кока-кола 1л', 46, 0),
(575, '.', '2019-02-01', 1, 'fanta-1l', 35, 1, '550.00', '.', '.', 'Fanta 1l', 'Ֆանտա 1լ', 'Фанта 1л', 47, 0),
(576, '0.5l', '2019-02-01', 1, 'water-ararat', 35, 1, '250.00', '0.5լ', '0.5л', 'Water Ararat', 'Ջուր Արարատ', 'Вода Арарат', 48, 0),
(577, '0.5l', '2019-02-01', 1, 'mineral-water-ararat-0-5l', 35, 1, '250.00', '0.5լ', '0.5л', 'Mineral water Ararat 0.5l', 'Հանքային ջուր Արարատ 0,5լ', 'Минеральная вода Арарат 0,5л', 49, 0),
(578, '1l', '2019-02-01', 1, 'mineral-water-ararat-1l', 35, 1, '350.00', '1լ', '1л', 'Mineral water Ararat 1l', 'Հանքային ջուր Արարատ 1լ', 'Минеральная вода Арарат 1л', 50, 0),
(579, '0.25l', '2019-02-01', 1, 'natural-juice-dobriy-0-25l', 35, 1, '250.00', '0.25լ', '0.25л', 'Natural juice Dobriy 0,25l', 'Դոբրի բնական հյութ 0.25լ', 'Натуральный сок Добрый 0,25л', 51, 0),
(580, 'Juice assortment Dobry 0,33l', '2019-02-01', 1, 'natural-juice-dobriy-0-33l', 35, 1, '350.00', 'Դոբրիյ հյութերի տեսականի 0,33լ', 'Ассортимент соков Добрый 0,33л', 'Natural juice Dobriy 0,33l', 'Դոբրի բնական հյութ 0,33լ', 'Натуральный сок Добрый 0,33л', 52, 0),
(581, '1l', '2019-02-01', 1, 'natural-juice-dobriy-1l', 35, 1, '800.00', '1լ', '1л', 'Natural juice Dobriy 1l', 'Դոբրի բնական հյութ 1լ', 'Натуральный сок Добрый 1л', 53, 0),
(582, '0.45l', '2019-02-01', 1, 'natural-juice-pulpy', 35, 1, '500.00', '0.45լ', '0.45л', 'Natural juice Pulpy', 'Բնական հյութ Pulpy', 'Натуральный сок Pulpy', 54, 0),
(583, 'Iced tea Fuzetea 0.45 l', '2019-02-01', 1, 'iced-tea', 35, 1, '500.00', 'Սառը թեյ Fuzetea 0.45լ', 'Холодный чай Fuzetea 0.45 л', 'Iced tea', 'Սառը թեյ', 'Холодный чай', 55, 0),
(584, '.', '2019-02-01', 1, 'iced-tea-nana', 35, 1, '600.00', '.', '.', 'Iced tea nana', 'Սառը թեյ նանա', 'Холодный чай нана', 56, 0),
(585, 'Areni', '2019-02-01', 1, 'areni', 36, 1, '2900.00', 'Արենի', 'Арени', 'Areni', 'Արենի', 'Арени', 36, 0),
(586, 'Vernashen', '2019-02-01', 1, 'vernashen', 36, 1, '2900.00', 'Վերնաշեն', 'Вернашен', 'Vernashen', 'Վերնաշեն', 'Вернашен', 37, 0),
(587, 'Armas white', '2019-02-01', 1, 'armas-white', 36, 1, '5300.00', 'Արմաս սպիտակ', 'Армас белое', 'Armas white', 'Արմաս սպիտակ', 'Армас белое', 38, 0),
(588, 'Karas white wine 0,75l', '2019-02-01', 1, 'karas-white-wine', 36, 1, '5500.00', 'Կարաս սպիտակ գինի 0,75լ', 'Карас белое вино 0,75л', 'Karas white wine', 'Կարաս սպիտակ գինի', 'Карас белое вино', 39, 0),
(589, 'Haghtanak', '2019-02-01', 1, 'haghtanak', 36, 1, '6000.00', 'Հաղթանակ', 'Ахтанак', 'Haghtanak', 'Հաղթանակ', 'Ахтанак', 40, 0),
(590, 'Armas red', '2019-02-01', 1, 'armas-red', 36, 1, '6700.00', 'Արմաս կարմիր', 'Армас красное', 'Armas red', 'Արմաս կարմիր', 'Армас красное', 41, 0),
(591, 'Kataro white', '2019-02-01', 1, 'kataro-white', 36, 1, '6900.00', 'Կատարո սպիտակ', 'Катаро белое', 'Kataro white', 'Կատարո սպիտակ', 'Катаро белое', 42, 0),
(592, 'Karas red wine 0.75l', '2019-02-01', 1, 'karas-red-wine', 36, 1, '6900.00', 'Կարաս կարմիր գինի 0.75լ', 'Карас красное вино 0.75л', 'Karas red wine', 'Կարաս կարմիր գինի', 'Карас красное вино', 43, 0),
(593, 'Qotot red', '2019-02-01', 1, 'qotot-red', 36, 1, '7400.00', 'Քոթոթ կարմիր', 'Котот красное', 'Qotot red', 'Քոթոթ կարմիր', 'Котот красное', 44, 0),
(594, 'Koor red', '2019-02-01', 1, 'koor-red', 36, 1, '7600.00', 'Կուռ կարմիր', 'Кур красное', 'Koor red', 'Կուռ կարմիր', 'Кур красное', 45, 0),
(595, 'Kataro red', '2019-02-01', 1, 'kataro-red', 36, 1, '8400.00', 'Կատարո կարմիր', 'Катаро красное', 'Kataro red', 'Կատարո կարմիր', 'Катаро красное', 46, 0),
(596, 'Armenian champagne 0.35', '2019-02-01', 1, 'armenian-champagne-0-35', 36, 1, '2000.00', 'Հայկական Շամպայն 0.35', 'Армянское шампанское 0.35', 'Armenian champagne 0.35', 'Հայկական Շամպայն 0.35', 'Армянское шампанское 0.35', 47, 0),
(597, 'Armenian Champagne 0.75', '2019-02-01', 1, 'armenian-champagne-0-75', 36, 1, '3000.00', 'Հայկական Շամպայն 0.75', 'Армянское Шампанское 0.75', 'Armenian Champagne 0.75', 'Հայկական Շամպայն 0.75', 'Армянское Шампанское 0.75', 48, 0),
(598, 'Karas champagne', '2019-02-01', 1, 'karas-champagne', 36, 1, '6000.00', 'Կարաս Շամպայն', 'Шампанское Карас', 'Karas champagne', 'Կարաս Շամպայն', 'Шампанское Карас', 49, 0),
(599, 'Beer \"Ararat\", 0,5l', '2019-02-01', 1, 'beer-ararat-0-5l', 36, 1, '600.00', 'Գարեջուր \"Արարատ\", 0,5լ', 'Пиво \"Арарат\", 0,5л', 'Beer \"Ararat\", 0,5l', 'Գարեջուր \"Արարատ\", 0,5լ', 'Пиво \"Арарат\", 0,5л', 50, 0),
(600, 'Beer \"Kilikia\", 0,5l', '2019-02-01', 1, 'beer-kilikia-0-5l', 36, 1, '600.00', 'Գարեջուր \"Կիլիկիա\", 0,5լ', 'Пиво \"Киликия\", 0,5л', 'Beer \"Kilikia\", 0,5l', 'Գարեջուր \"Կիլիկիա\", 0,5լ', 'Пиво \"Киликия\", 0,5л', 51, 0),
(601, 'Beer \"Gyumri\", 0,5l', '2019-02-01', 1, 'beer-gyumri-0-5l', 36, 1, '600.00', 'Գարեջուր \"Գյումրի\", 0,5լ', 'Пиво \"Гюмри\" (0,5)', 'Beer \"Gyumri\", 0,5l', 'Գարեջուր \"Գյումրի\", 0,5լ', 'Пиво \"Гюмри\" (0,5)', 52, 0),
(602, '0.33l', '2019-02-01', 1, 'stella-artois', 36, 1, '650.00', '0.33լ', '0.33л', 'Stella artois', 'Ստելլա արտոիս', 'Стелла артоис', 53, 0),
(603, 'Beer \"Aleksandrapol\", 0,5l', '2019-02-01', 1, 'beer-aleksandrapol-0-5l', 36, 1, '800.00', 'Գարեջուր \"Ալեքսանդրապոլ\", 0,5լ', 'Пиво \"Александраполь\", 0,5л', 'Beer \"Aleksandrapol\", 0,5l', 'Գարեջուր \"Ալեքսանդրապոլ\", 0,5լ', 'Пиво \"Александраполь\", 0,5л', 54, 0),
(604, 'Beer \"Heinken\", 0,33l', '2019-02-01', 1, 'beer-heinken-0-33l', 36, 1, '850.00', 'Գարեջուր \"Հեյնիկեն\", 0,33լ', 'Пиво \"Хайникен\", 0,33л', 'Beer \"Heinken\", 0,33l', 'Գարեջուր \"Հեյնիկեն\", 0,33լ', 'Пиво \"Хайникен\", 0,33л', 55, 0),
(605, 'Vodka \"Gradus\", 0,25l', '2019-02-01', 1, 'vodka-gradus-0-25l', 36, 1, '1500.00', 'Օղի \"Գրադուս\", 0,25լ', 'Водка \"Градус\", 0,25л', 'Vodka \"Gradus\", 0,25l', 'Օղի \"Գրադուս\", 0,25լ', 'Водка \"Градус\", 0,25л', 56, 0),
(606, 'Vodka \"Moskovskie Novosti\", 0,25l', '2019-02-01', 1, 'vodka-moskovskie-novosti-0-25l', 36, 1, '1600.00', 'Օղի \"Մոսկովսկիե Նովոստի\", 0,25լ', 'Водка \"Московские Новости\", 0,25л', 'Vodka \"Moskovskie Novosti\", 0,25l', 'Օղի \"Մոսկովսկիե Նովոստի\", 0,25լ', 'Водка \"Московские Новости\", 0,25л', 57, 0),
(607, 'Vodka \"Moskovskie Novosti\", 0,5l', '2019-02-01', 1, 'vodka-moskovskie-novosti-0-5l', 36, 1, '1950.00', 'Օղի \"Մոսկովսկիե Նովոստի\", 0,5լ', 'Водка \"Московские Новости\", 0,5л', 'Vodka \"Moskovskie Novosti\", 0,5l', 'Օղի \"Մոսկովսկիե Նովոստի\", 0,5լ', 'Водка \"Московские Новости\", 0,5л', 58, 0),
(608, 'Vodka \"Celsius\" 0.5l', '2019-02-01', 1, 'vodka-celsius-0-5l', 36, 1, '1950.00', 'Օղի \"Ցելսիյ\", 0,5լ', 'Водка \"Цельсий\", 0,5л', 'Vodka \"Celsius\" 0.5l', 'Օղի \"Ցելսիյ\", 0,5լ', 'Водка \"Цельсий\", 0,5л', 59, 0),
(609, 'Vodka \"Gradus\", 0,5l', '2019-02-01', 1, 'vodka-gradus-0-5l', 36, 1, '2900.00', 'Օղի \"Գրադուս\", 0,5լ', 'Водка \"Градус\", 0,5л', 'Vodka \"Gradus\", 0,5l', 'Օղի \"Գրադուս\", 0,5լ', 'Водка \"Градус\", 0,5л', 60, 0),
(610, 'Vodka \"Finsky\", 0,5l', '2019-02-01', 1, 'vodka-finsky-0-5l', 36, 1, '3950.00', 'Օղի \"Ֆինսկի\", 0,5լ', 'Водка \"Фински\", 0,5л', 'Vodka \"Finsky\", 0,5l', 'Օղի \"Ֆինսկի\", 0,5լ', 'Водка \"Фински\", 0,5л', 61, 0),
(611, 'Vodka \"Iney Original\"', '2019-02-01', 1, 'vodka-iney-original', 36, 1, '4200.00', 'Օղի «Ինեյ Օրիգինալ»', 'Водка \"Иней Оригинал\"', 'Vodka \"Iney Original\"', 'Օղի «Ինեյ Օրիգինալ»', 'Водка \"Иней Оригинал\"', 62, 0),
(612, 'Vodka \"Tsarskaya Originalnaya\", 0,5l', '2019-02-01', 1, 'vodka-tsarskaya-originalnaya-0-5l', 36, 1, '4200.00', 'Օղի \"Ցառսկայա Օրիգինալնայա\", 0,5լ', 'Водка \"Царская Оригинальная\", 0,5л', 'Vodka \"Tsarskaya Originalnaya\", 0,5l', 'Օղի \"Ցառսկայա Օրիգինալնայա\", 0,5լ', 'Водка \"Царская Оригинальная\", 0,5л', 63, 0),
(613, 'Vodka \"Tsarskaya Zolotaya\", 0.5l', '2019-02-01', 1, 'vodka-tsarskaya-zolotaya-0-5l', 36, 1, '5900.00', 'Օղի \"Ցառսկայա Զոլոտայա\", 0,5լ', 'Водка \"Царская Золотая\", 0,5л', 'Vodka \"Tsarskaya Zolotaya\", 0.5l', 'Օղի \"Ցառսկայա Զոլոտայա\", 0,5լ', 'Водка \"Царская Золотая\", 0,5л', 64, 0),
(614, 'Vodka \"Koskenkorva\", 0,5l', '2019-02-01', 1, 'vodka-koskenkorva-0-5l', 36, 1, '6300.00', 'Օղի \"Կոսկենկորվա\", 0,5լ', 'Водка \"Коскенкорва\", 0,5л', 'Vodka \"Koskenkorva\", 0,5l', 'Օղի \"Կոսկենկորվա\", 0,5լ', 'Водка \"Коскенкорва\", 0,5л', 65, 0),
(615, 'Vodka \"Finlandia\", 0,5l', '2019-02-01', 1, 'vodka-finlandia-0-5l', 36, 1, '6900.00', 'Օղի \"Ֆինլանդիա\", 0,5լ', 'Водка \"Финляндия\", 0,5л', 'Vodka \"Finlandia\", 0,5l', 'Օղի \"Ֆինլանդիա\", 0,5լ', 'Водка \"Финляндия\", 0,5л', 66, 0),
(616, 'Vodka \"Absolut\", 0,5l', '2019-02-01', 1, 'vodka-absolut-0-5l', 36, 1, '7800.00', 'Օղի \"Աբսալյուտ\", 0,5լ', 'Водка \"Абсолют\", 0,5л', 'Vodka \"Absolut\", 0,5l', 'Օղի \"Աբսալյուտ\", 0,5լ', 'Водка \"Абсолют\", 0,5л', 67, 0),
(617, 'Vodka \"Fashion\", 0,5l', '2019-02-01', 1, 'vodka-fashion-0-5l', 36, 1, '19000.00', 'Օղի \"Ֆեյշն\", 0,5լ', 'Водка \"Фейшн\", 0,5л', 'Vodka \"Fashion\", 0,5l', 'Օղի \"Ֆեյշն\", 0,5լ', 'Водка \"Фейшн\", 0,5л', 68, 0),
(618, 'Vodka \"Ohanyan\"', '2019-02-01', 1, 'vodka-ohanyan', 36, 1, '8500.00', 'Օղի \"Օհանյան\"', 'Водка \"Оганян\"', 'Vodka \"Ohanyan\"', 'Օղի \"Օհանյան\"', 'Водка \"Оганян\"', 69, 0),
(619, 'Main dish, garnish, salad and drink for your choice + 2 pieces of bread\n*available: 09։00-18։00', '2019-02-01', 1, 'lunch-1350', 37, 1, '1350.00', 'Հիմնական ուտեստ, խավարտ, աղցան, ըմպելիք` Ձեր ընտրությամ + 2 կտոր հացիկ\n* հասանելի է՝ 09:00 - 18:00', 'Основное блюдо, гарнир, салат и напиток на выбор + 2 куска хлеба\n*доступно- 09։00-18։00', 'Lunch 1350', 'Լանչ 1350', 'Ланч 1350', 37, 0),
(620, '2 pcs. of bread, tan, garnish, salad and main dish on your choice  *available: 09։00-18։00', '2019-02-01', 1, 'lunch-950', 37, 1, '950.00', '2 կտոր հաց, թան, խավարտ, աղցան և հիմնական ուտեստ ձեր ընտրությամբ  *հասանելի է՝ 09։00-18։00', '2 куска хлеба, тан, гарнир, салат и основное блюдо на ваш выбор  *доступно- 09։00-18։00', 'Lunch 950', 'Լանչ 950', 'Ланч 950', 38, 0),
(621, 'soup, garnish, salad on your choice + 2 pieces of bread, half lavash  *available: 09։00-18։00', '2019-02-01', 1, 'lunch-with-soup-1350', 37, 1, '1350.00', 'ապուր, խավարտ, աղցան Ձեր ընտրությամբ + 2 կտոր հացիկ, կես լավաշ * հասանելի է՝ 09։00-18։00', 'суп, гарнир, салат на Ваш выбор + 2 куска хлеба, половина лаваша  *доступно- 09։00-18։00', 'Lunch with soup 1350', 'Լանչ ապուրով 1350', 'Ланч с супом 1350', 39, 0),
(622, 'soup, garnish, salad on your choice + 2 pieces of bread, half lavash  *available: 09։00-18։00', '2019-02-01', 1, 'lunch-with-soup-950', 37, 1, '950.00', 'ապուր, խավարտ, աղցան Ձեր ընտրությամբ + 2 կտոր հացիկ, կես լավաշ  * հասանելի է՝ 09։00-18։00', 'суп, гарнир, салат на Ваш выбор + 2 куска хлеба, половина лаваша  *доступно- 09։00-18։00', 'Lunch with soup 950', 'Լանչ ապուրով 950', 'Ланч с супом  950', 40, 0),
(623, 'Composed of 4 parts: 1. Cheese, Pepperoni, hot pepper, sauce. 2. Cheese, ham, tomatoes, bell peppers, sauce. 3. Rokfor, Lori and Mozzarella cheese, sauce. 4. Cheese, chicken breast, mushrooms, corn, sauce', '2019-02-01', 1, 'pizza-karas-30cm', 38, 1, '2900.00', '4 տարբեր պիցցաների հավաքածու. 1. Պեպերոնի, կծու պղպեղ, պանիր, սոուս 2. Խոզապուխտ, լոլիկ, պանիր, բուլղ. պղպեղ, սոուս 3. Պանիր ռոքֆոր, լոռի, մոցառելլա, սոուս 4. Հավի կրծքամիս, սունկ, եգիպտացորեն,պանիր, սոուս', 'Состоит из 4 частей: 1. Сыр, Пепперони, острый перец, соус. 2. Сыр, ветчина, помидоры, болгарский перец, соус.   3. Сыр Рокфор, Лори и Моцарелла, соус. 4. Сыр, куриная грудка, грибы, кукуруза, соус', 'Pizza Karas, 30cm', 'Պիցցա Կարաս, 30սմ', 'Пицца Карас, 30см', 38, 0),
(624, 'Composed of 4 parts: 1. Cheese, Pepperoni, hot pepper, sauce.  2. Cheese, ham, tomatoes, bell peppers, sauce. 3. Rokfor, Lori and Mozzarella cheese, sauce. 4. Cheese, chicken breast, mushrooms, corn, sauce', '2019-02-01', 1, 'pizza-karas-24cm', 38, 1, '1800.00', '4 տարբեր պիցցաների հավաքածու.1. Պեպերոնի, կծու պղպեղ, պանիր, սոուս2. Խոզապուխտ, լոլիկ, պանիր, բուլղ. պղպեղ, սոուս3. Պանիր ռոքֆոր, լոռի, մոցառելլա, սոուս4. Հավի կրծքամիս, սունկ, եգիպտացորեն,պանիր, սոուս', 'Состоит из 4 частей: 1. Сыр, Пепперони, острый перец, соус. 2. Сыр, ветчина, помидоры, болгарский перец, соус.3. Сыр Рокфор, Лори и Моцарелла, соус. 4. Сыр, куриная грудка, грибы, кукуруза, соус', 'Pizza Karas, 24cm', 'Պիցցա Կարաս, 24սմ', 'Пицца Карас, 24см', 39, 0),
(625, 'Сhicken breast, ham, mushrooms, cheese, tomatoes, olives, sauce', '2019-02-01', 1, 'pizza-assorti-30cm', 38, 1, '2350.00', 'Հավի կրծքամիս, խոզապուխտ, լոլիկ, սունկ, պանիր, ձիթապտուղ, սոուս', 'Куриная грудка, ветчина, грибы, сыр, помидоры, маслины, соус', 'Pizza Assorti, 30cm', 'Պիցցա Ասորտի, 30սմ', 'Пицца Ассорти, 30см', 40, 0),
(626, '<p>Сhicken breast, ham, mushrooms, cheese, tomatoes, olives, sauce</p>', '2019-02-01', 1, 'pizza-assorti-24cm', 38, 1, '1500.00', '<p>Հավի կրծքամիս, խոզապուխտ, լոլիկ, սունկ, պանիր, ձիթապտուղ, սոուս</p>', '<p>Куриная грудка, ветчина, грибы, сыр, помидоры, маслины, соус</p>', 'Pizza Assorti, 24cm', 'Պիցցա Ասորտի, 24սմ', 'Пицца Ассорти, 24см', 41, 0),
(627, 'Pepperoni sausage, cheese, olives, sauce', '2019-02-01', 1, 'pizza-pepperoni-30cm', 38, 1, '2500.00', 'Պեպերոնի երշիկ, պանիր, ձիթապտուղ, սոուս', 'Колбаса пепперони, сыр, оливки, соус', 'Pizza Pepperoni, 30cm', 'Պիցցա Պեպերոնի, 30սմ', 'Пицца Пеперони, 30см', 42, 0),
(628, '<p>Pepperoni sausage, cheese, olives, sauce</p>', '2019-02-01', 1, 'pizza-pepperoni-24cm', 38, 1, '1600.00', '<p>Պեպերոնի երշիկ, պանիր, ձիթապտուղ, սոուս</p>', '<p>Колбаса пепперони, сыр, оливки, соус</p>', 'Pizza Pepperoni, 24cm', 'Պիցցա Պեպերոնի, 24սմ', 'Пицца Пеперони, 24см', 43, 1),
(629, 'Ham, cheese, tomatoes, sauce', '2019-02-01', 1, 'pizza-palermo-30cm', 38, 1, '2100.00', 'Խոզապուխտ, պանիր, լոլիկ, սոուս', 'Ветчина, сыр, помидоры, соус', 'Pizza Palermo, 30cm', 'Պիցցա Պալերմո, 30սմ', 'Пицца Палермо, 30см', 44, 0),
(630, 'Ham, cheese, tomatoes, sauce', '2019-02-01', 1, 'pizza-palermo-24cm', 38, 1, '1300.00', 'Խոզապուխտ, պանիր, լոլիկ, սոուս', 'Ветчина, сыр, помидоры, соус', 'Pizza Palermo, 24cm', 'Պիցցա Պալերմո, 24սմ', 'Пицца Палермо, 24см', 45, 0),
(631, 'Chicken meat, cheese, tomatoes, peppers, olives', '2019-02-01', 1, 'pizza-chicken-marley-30cm', 38, 1, '2400.00', 'Հավի միս, պանիր, լոլիկ, պղպեղ, ձիթապտուղ', 'Куриное мясо, сыр, помидоры, перец, оливки', 'Pizza Chicken Marley, 30cm', 'Պիցցա Չիկն Մառլեյ, 30սմ', 'Пицца Чикн Марлей, 30см', 46, 0),
(632, '<p>Chicken meat, cheese, tomatoes, peppers, olives</p>', '2019-02-01', 1, 'pizza-chicken-marley-24cm', 38, 1, '1750.00', '<p>Հավի միս, պանիր, լոլիկ, պղպեղ, ձիթապտուղ</p>', '<p>Куриное мясо, сыр, помидоры, перец, оливки</p>', 'Pizza Chicken Marley, 24cm', 'Պիցցա Չիկն Մառլեյ, 24սմ', 'Пицца Чикн Марлей, 24см', 47, 1),
(633, 'Cheese, tomatoes, basil', '2019-02-01', 1, 'pizza-margarita-30cm', 38, 1, '1650.00', 'Պանիր, լոլիկ, ռեհան', 'Сыр, помидоры, базилик', 'Pizza Margarita, 30cm', 'Պիցցա Մարգարիտա, 30սմ', 'Пицца Маргарита, 30см', 48, 0),
(634, 'Cheese, tomatoes, basil', '2019-02-01', 1, 'pizza-margarita-24cm', 38, 1, '1100.00', 'Պանիր, լոլիկ, ռեհան', 'Сыр, помидоры, базилик', 'Pizza Margarita, 24cm', 'Պիցցա Մարգարիտա, 24սմ', 'Пицца Маргарита, 24см', 49, 0),
(635, '1 egg, cheese, butter', '2019-02-01', 1, 'ajarian-khachapuri-1-egg', 39, 1, '900.00', '1 ձու, պանիր, կարագ', '1 яйцо, сыр, сливочное масло', 'Ajarian khachapuri (1 egg)', 'Աջարական 1 ձու', 'Аджарские хачапури (1 яйцо)', 39, 0),
(636, '2 eggs, cheese, butter', '2019-02-01', 1, 'ajarian-khachapuri-2-eggs', 39, 1, '1000.00', '2 ձու, պանիր, կարագ', '2 яйца, сыр, сл. масло', 'Ajarian khachapuri (2 eggs)', 'Աջարական 2 ձու', 'Аджарские хачапури (2 яйца)', 40, 0),
(637, '1 egg, basturma, cheese, butter', '2019-02-01', 1, 'ajarian-khachapuri-with-basturma-1-egg', 39, 1, '1050.00', '1 ձու, բաստուրմա, պանիր, կարագ', '1 яйцо, бастурма, сыр, сл. масло', 'Ajarian khachapuri with basturma (1 egg)', 'Բաստուրմայով աջարական 1 ձու', 'Аджарские хачапури с бастурмой (1 яйцо)', 41, 0),
(638, '2 eggs, basturma, cheese, butter', '2019-02-01', 1, 'ajarian-khachapuri-with-basturma-2-eggs', 39, 1, '1150.00', '2 ձու, բաստուրմա, պանիր, կարագ', '2 яйцa, бастурма, сыр, сл. масло', 'Ajarian khachapuri with basturma (2 eggs)', 'Բաստուրմայով աջարական 2 ձու', 'Аджарские хачапури с бастурмой (2 яйцa)', 42, 0),
(639, '1 egg, ham, cheese, butter', '2019-02-01', 1, 'ajarian-khachapuri-with-ham-1-egg', 39, 1, '1050.00', '1 ձու, խոզապուխտ, պանիր, կարագ', '1 яйцо, ветчина, сыр, сл. масло', 'Ajarian khachapuri with ham (1 egg)', 'Խոզապուխտով աջարական 1 ձու', 'Аджарские хачапури с ветчиной (1 яйцо)', 43, 0),
(640, '2 eggs, ham, cheese, butter', '2019-02-01', 1, 'ajarian-khachapuri-with-ham-2-eggs', 39, 1, '1150.00', '2 ձու, խոզապուխտ, պանիր, կարագ', '2 яйца, ветчина, сыр, сл. масло', 'Ajarian khachapuri with ham ( 2 eggs)', 'Խոզապուխտով աջարական 2 ձու', 'Аджарские хачапури с ветчиной (2 яйца)', 44, 0),
(641, '2 sorts of cheese - lori and mozarella, wrapped in lavash, tomato, greens; baked on fire', '2019-02-01', 1, 'baked-khachapuri', 39, 1, '400.00', 'Լավաշի մեջ 2 տեսակի պանիր՝ լոռի, մոցառելլա, լոլիկ, կանաչի, կրակի վրա խորոված /1 հատ/', 'Завернутые в лаваш сыр лори и моцарелла, помидор и зелень, запеченные на огне', 'Baked khachapuri', 'Խորոված խաչապուրի', 'Печенный хачапури', 45, 0),
(642, 'Closed dough with cheese in it, 30 cm', '2019-02-01', 1, 'imeretin-khachapuri', 39, 1, '0.00', '30 սմ տրամագծով փակ խմոր՝ ներսում պանիր', 'Закрытое тесто с сыром внутри, 30', 'Imeretin khachapuri', 'Իմերիթական', 'Имеретинские хачапури', 46, 0),
(643, 'Puff pastry', '2019-02-01', 1, 'khachapuri-with-potatoes', 39, 1, '250.00', 'Շերտավոր խմորով', 'Слоеное тесто', 'Khachapuri with potatoes', 'Խաչապուրի կարտոֆիլով', 'Хачапури с картофелем', 47, 0),
(644, 'Puff pastry', '2019-02-01', 1, 'khachapuri-with-cheese', 39, 1, '300.00', 'Շերտավոր խմորով', 'Слоеное тесто', 'Khachapuri with cheese', 'Խաչապուրի պանրով', 'Хачапури с сыром', 48, 0),
(645, 'Puff pastry', '2019-02-01', 1, 'khachapuri-with-meat', 39, 1, '350.00', 'Շերտավոր խմորով', 'Слоеное тесто', 'Khachapuri with meat', 'Խաչապուրի մսով', 'Хачапури с мясом', 49, 0),
(646, '.', '2019-02-01', 1, 'pie-with-cabbage', 39, 1, '250.00', '.', '.', 'Pie with cabbage', 'Կարկանդակ կաղամբով', 'Пирожок с капустой', 50, 0),
(647, '.', '2019-02-01', 1, 'pie-with-potatoes', 39, 1, '250.00', '.', '.', 'Pie with potatoes', 'Կարկանդակ կարտոֆիլով', 'Пирожок с картофелем', 51, 0),
(648, '.', '2019-02-01', 1, 'pie-with-sausage', 39, 1, '250.00', '.', '.', 'Pie with sausage', 'Կարկանդակ նրբերշիկով', 'Пирожок с сосиской', 52, 0),
(649, '.', '2019-02-01', 1, 'pie-with-chicken', 39, 1, '300.00', '.', '.', 'Pie with chicken', 'Կարկանդակ հավի մսով', 'Пирожок с курицей', 53, 0),
(650, '.', '2019-02-01', 1, 'pie-with-beef', 39, 1, '300.00', '.', '.', 'Pie with beef', 'Կարկանդակ տավարի մսով', 'Пирожок с говядиной', 54, 0),
(651, '.', '2019-02-01', 1, 'pie-with-cheese', 39, 1, '300.00', '.', '.', 'Pie with cheese', 'Կարկանդակ պանրով', 'Пирожок с сыром', 55, 0),
(652, '.', '2019-02-01', 1, 'pie-chicken-and-mushrooms', 39, 1, '300.00', '.', '.', 'Pie chicken and mushrooms', 'Կարկանդակ հավի մսով և սնկով', 'Пирожок с курицей и грибами', 56, 0),
(653, 'Chicken meat, Dutch cheese, tomato, lettuce, mayonnaise, crusty bread, spices', '2019-02-01', 1, 'karas', 40, 1, '1250.00', 'Հավի կրծքամիս, մառոլ, լոլիկ, հոլանդական պանիր, չորահաց, մայոնեզ, համեմունքներ', 'Куриная грудка, голландский сыр, помидор, листья салата, майонез, сухарики, специи', 'KARAS', 'Կարաս', 'Салат \"Карас\"', 40, 0),
(654, 'Beef, bulg.pepper, mushrooms, corn, cucumber, greens, spices, mayonnaise', '2019-02-01', 1, 'primavera', 40, 1, '1100.00', 'Տավարի միս, բուլղ. պղպեղ, սունկ, եգիպտացորեն, վարունգ, կանաչի, համեմունքներ, մայոնեզ', 'Говяжье мясо, болг. перец, грибы, кукуруза, огурцы, зелень, специи, майонез', 'Primavera', 'Պրիմավերա', 'Примавера', 41, 0),
(655, 'Soft beef, chicken meat, fresh decorative peppers, tomatoes, fresh green onions, spices, soy sauce, garlic', '2019-02-01', 1, 'taybef', 40, 1, '1950.00', 'Տավարի սուկի, հավի կրծքամիս, բուլղ. պղպեղ, լոլիկ, կանաչ սոխ, սոյայի սոուս, սխտոր, համեմունքներ', 'Говяжья вырезка, куриная грудка, свежий болгарский перец, помидоры, свежий зеленый лук, специи, соевый соус, чеснок', 'Taybef', 'Թայբեֆ', 'Тайбеф', 42, 0),
(656, 'Boiled beef, potatoes, carrots, pickles, canned peas, greens, spices and mayonnaise', '2019-02-01', 1, 'olivier-salad', 40, 1, '750.00', 'Գազար, կարտոֆիլ, մարինացված վարունգ, տավարի միս, կանաչ ոլոռ, կանաչի, մայոնեզ, համեմունքներ', 'Говяжье мясо, картофель, морковь, маринованные огурцы, консервированный горох, зелень, майонез, специи', 'Olivier salad', 'Մայրաքաղաքային', 'Салат `Столичный`', 43, 0),
(657, 'Chicken breast, Dutch cheese, Parmesan cheese, tomatoes, lettuce, crusty bread, sauce, spices', '2019-02-01', 1, 'caesar', 40, 1, '1500.00', 'Հավի  կրծքամիս, մառոլ, լոլիկ, պանիր՝ հոլանդական, պարմեզան, սոուս, չորահաց, համեմունքներ', 'Куриная грудка, голландский сыр, сыр Пармезан, помидоры, листья салата, соус, сухарики, специи', 'Caesar', 'Կեսար', 'Цезарь', 44, 0),
(658, 'Chicken breast, corn, nuts, greens, spices and mayonnaise.', '2019-02-01', 1, 'chicken-salad', 40, 1, '1000.00', 'Հավի կրծքամիս, եգիպտացորեն, ընկույզ, կանաչի, մայոնեզ, համեմունքներ', 'Куриная грудка, кукуруза, орех, зелень, майонез, специи', 'Chicken salad', 'Հավով', 'Салат с курицей', 45, 0),
(659, 'Canned green peas, boiled potatoes and beets, pickles, greens, green onions, carrot, spices and sour cream.', '2019-02-01', 1, 'bulgarian-vinegret', 40, 1, '550.00', 'Բազուկ, գազար, կարտոֆիլ, մարինացված վարունգ, կանաչ ոլոռ, կանաչ սոխ, կանաչի, թթվասեր, մայոնեզ, համեմունքներ', 'Консервированный горох, картофель, свекла, маринованные огурцы, зелень, зеленый лук, морковь, сметана, специи', 'Bulgarian Vinegret', 'Բուլղարական վինեգրետ', 'Болгарский винегрет', 46, 0),
(660, 'Fresh tomatoes, cucumbers and green peppers, greens, spices and olive oil', '2019-02-01', 1, 'salad-summer', 40, 1, '850.00', 'Լոլիկ, վարունգ, բիբար, կանաչի, ձեթ, համեմունքներ', 'Свежие помидоры, огурцы и болгарский перец, зелень, оливковое масло, специи.', 'Salad \"Summer\"', 'Ամառային', 'Салат \"Летний\"', 47, 0),
(661, 'Fetta cheese, fresh tomatoes and cucumbers, lettuce, olives, olive oil, spices', '2019-02-01', 1, 'greek', 40, 1, '1350.00', 'Լոլիկ, վարունգ, մառոլ, ձիթապտուղ, ֆետտա պանիր, ձեթ, համեմունքներ', 'Сыр Фетта, свежие помидоры, огурцы, листья салата, оливки, оливковое масло, специи', 'Greek', 'Հունական', 'Греческий', 48, 0),
(662, 'This original salad is made of fresh tomatoes, cucumbers, Parmesan cheese, herbs, lettuce and sour cream.', '2019-02-01', 1, 'grandma-s-salad', 40, 1, '950.00', 'Լոլիկ, վարունգ, մառոլ, պարմեզան պանիր, թթվասեր, համեմունքներ', 'Свежие помидоры, огурцы, сыр Пармезан, листья салата, сметана, специи', 'Grandma\'s salad', 'Աղցան Տատիկի', 'Бабушкин салат', 49, 0),
(663, 'Fried eggplants, stuffed with yoghurt, nuts, spices; 6-7 pcs', '2019-02-01', 1, 'ggplant-rolls', 40, 1, '800.00', 'Տապակած սմբուկ՝ լցոնված մածունով, ընկույզով, համեմունքներով 6-7 հատ', 'Жареные баклажаны, фаршированные мацуном, орехами и специями, 6-7 штук', 'Еggplant rolls', 'Սմբուկով ռուլետ', 'Роллы из баклажанов', 50, 0),
(664, 'Red beans, beets, carrots, pickled cucumbers, corn, greens', '2019-02-01', 1, 'salad-aragats', 40, 1, '700.00', 'Կարմիր լոբի, բազուկ, գազար, մարինացված վարունգ, եգիպտացորեն, կանաչի', 'Красная фасоль, свекла, морковь, маринованные огурцы, кукуруза, зелень', 'Salad Aragats', 'Աղցան Արագած', 'Салат Арагац', 51, 0),
(665, 'Bulgur, tomatoes, greens, green onion, spices', '2019-02-01', 1, 'tabuleh', 40, 1, '700.00', 'Բլղուր, կանաչի, կանաչ սոխ, լոլիկ, համեմունքներ', 'Булгур, помидор, зелень, зеленый лук, специи', 'Tabuleh', 'Թաբուլե', 'Табуле', 52, 0),
(666, 'Сooked beets, nuts, garlic, sour cream, greens, spices', '2019-02-01', 1, 'beet-salad', 40, 1, '500.00', 'Բազուկ, ընկույզ, սխտոր, կանաչի, թթվասեր, համեմունքներ', 'Свекла, орех, чеснок, зелень, сметана, специи', 'Beet Salad', 'Բազուկով աղցան', 'Салаты из свеклы', 53, 0),
(667, 'Strained yogurt, cucumber, dill', '2019-02-01', 1, 'strained-yogurt-salad', 40, 1, '550.00', 'Քամած մածուն, վարունգ, սամիթ', 'Сцеженный мацун, огурец, укроп', 'Strained yogurt salad', 'Քամած մածունով աղցան', 'Салат с сцеженным мацони', 54, 0),
(668, 'Fresh carrots, special spices, garlic and olive oil.', '2019-02-01', 1, 'korean', 40, 1, '400.00', 'Գազար, ձեթ, կորեական համեմունք, սխտոր', 'Cвежая морковь, чеснок, оливковое масло, корейские специи', 'Korean', 'Կորեական', 'Корейский', 55, 0),
(669, 'Fresh cabbage and carrot, greens, spices and olive oil', '2019-02-01', 1, 'coleslaw', 40, 1, '400.00', 'Կաղամբ, գազար, կանաչի, ձեթ, համեմունքներ', 'Свежая капуста и морковь, зелень, оливковое масло, специи', 'Coleslaw', 'Կաղամբով', 'Салат с капустой', 56, 0),
(670, 'Fresh carrots, special spices, garlic and olive oil in 35cm bread', '2019-02-01', 1, 'sandwich-with-korean-salad', 40, 1, '500.00', 'Գազար, ձեթ, կորեական համեմունք, սխտոր՝ 35 սմ հացի մեջ', 'Cвежая морковь, чеснок, оливковое масло, корейские специи в 35 см хлебе', 'Sandwich with korean salad', 'Կորեական աղցանով սենդվիչ', 'Сэндвич с корейским салатом', 57, 0),
(671, 'Boiled beef, potatoes, carrots, pickles, canned peas, greens, spices and mayonnaise in 35 sm bread', '2019-02-01', 1, 'sandwich-with-olivier-salad', 40, 1, '800.00', 'Գազար, կարտոֆիլ, մարինացված վարունգ, տավարի միս, կանաչ ոլոռ, կանաչի, մայոնեզ, համեմունքներ` 35 սմ հացի մեջ', 'Говяжье мясо, картофель, морковь, маринованные огурцы, консервированный горох, зелень, майонез, специи в 35 см хлебе', 'Sandwich with olivier salad', 'Մայրաքաղաքային աղցանով սենդվիչ', 'Сэндвич с столичным салатом', 58, 0),
(672, 'Canned green peas, boiled potatoes and beets, pickles, greens, green onions, carrot, spices and sour cream in 35 sm bread', '2019-02-01', 1, 'sandwich-with-bulgarian-vinegret', 40, 1, '650.00', 'Բազուկ, գազար, կարտոֆիլ, մարինացված վարունգ, կանաչ ոլոռ, կանաչ սոխ, կանաչի, թթվասեր, մայոնեզ, համեմունքներ՝ 35 սմ հացի մեջ', 'Консервированный горох, картофель, свекла, маринованные огурцы, зелень, зеленый лук, морковь, сметана, специи в 35 см хлебе', 'Sandwich with bulgarian vinegret', 'Բուլղարական վինեգրետով սենդվիչ', 'Сэндвич с болгарским винегретом', 59, 0),
(673, 'Chicken breast, corn, nuts, greens, spices and mayonnaise in 35 sm bread', '2019-02-01', 1, 'sandwich-with-chicken-salad', 40, 1, '1100.00', 'Հավի կրծքամիս, եգիպտացորեն, ընկույզ, կանաչի, մայոնեզ, համեմունքներ` 35 սմ հացի մեջ', 'Куриная грудка, кукуруза, орех, зелень, майонез, специи в 35 см хлебе', 'Sandwich with chicken salad', 'Հավի աղցանով սենդվիչ', 'Сэндвич с куриным салатом', 60, 0),
(674, 'Fried eggplants, stuffed with yoghurt, nuts, spices in 35 sm bread', '2019-02-01', 1, 'sandwich-with-eggplants-rolls', 40, 1, '900.00', '35 սմ հացի մեջ՝ տապակած սմբուկ, լցոնված մածունով, ընկույզով, համեմունքներով 6-7 հատ', 'Жареные баклажаны, фаршированные мацуном, орехами и специями в 35 см хлебе', 'Sandwich with eggplants rolls', 'Սենդվիչ սմբուկի ռոլլերով', 'Сэндвич с баклажановыми роллами', 61, 0),
(675, 'Beef meat, ham, sausage, sausages, pickled cucumber, capers, greens, lemon and olives + 2 pcs of bread', '2019-02-01', 1, 'solianka', 41, 1, '1250.00', 'Տավարի միս, նրբերշիկ, խոզապուխտ, երշիկ, մարինացված վարունգ, կապար, ձիթապտուղ, կանաչի, կիտրոն + 2 կտոր հացիկ, թթվասեր', 'Говяжье мясо, ветчина, сосиски, колбаса, маринованные огурцы, томатная паста, лимон, оливки, каперсы, зелень +  ломтика хлеба', 'Solianka', 'Սոլյանկա', 'Солянка', 41, 0),
(676, 'Beef, carrots, beets, potatoes, сabbage, tomato paste, onion, greens + 2 pcs of bread, sour cream', '2019-02-01', 1, 'borsch', 41, 1, '850.00', 'Տավարի միս, գազար, բազուկ, կարտոֆիլ, կաղամբ, տոմատի մածուկ, սոխ, կանաչի+ 2 կտոր հացիկ, թթվասեր', 'Говяжье мясо, морковь, свекла, картофель, капуста, томатная паста, лук, зелень + 2 ломтика хлеба, сметана', 'Borsch', 'Բորշչ', 'Борщ', 42, 0),
(677, 'Chicken, potatoes, carrots, onions, rice, greens, bulgarian peppers + 2 pcs of bread', '2019-02-01', 1, 'chicken-soup', 41, 1, '850.00', 'Հավի միս, կարտոֆիլ, գազար, սոխ, բրինձ, կանաչի, բիբար+2 կտոր հացիկ', 'Куриное мясо, картофель, морковь, лук, рис, зелень, болгарский перец + 2 ломтика хлеба', 'Chicken soup', 'Հավապուր', 'Куриный суп', 43, 0),
(678, 'Mushrooms, potatoes, carrots, onion, bulgarian pepper, greens + 2 pcs of bread', '2019-02-01', 1, 'mushroom-soup', 41, 1, '750.00', 'Սունկ, կարտոֆիլ, գազար, սոխ, բիբար, կանաչի +2 կտոր հացիկ', 'Грибы, картофель, морковь, лук, сладкий перец, зелень + 2 ломтика хлеба', 'Mushroom soup', 'Սնկապուր', 'Грибной суп', 44, 0),
(679, 'Red beans, nuts, greens, onions + 2 pcs of bread', '2019-02-01', 1, 'bean-soup', 41, 1, '700.00', 'Կարմիր լոբի, ընկույզ, սոխ, կանաչի + 2 կտոր հացիկ', 'Красная фасоль, грецкий орех, зелень, лук + 2 ломтика хлеба', 'Bean Soup', 'Կարմիր լոբով', 'Суп с фасолью', 45, 0),
(680, 'Yogurt, wheat, greens and butter + 2 pcs of bread', '2019-02-01', 1, 'spas', 41, 1, '600.00', 'Մածուն, ձավար, կարագ, կանաչի + 2 կտոր հացիկ', 'Мацун, пшеница, сливочное масло, зелень + 2 ломтика хлеба', 'Spas', 'Սպաս', 'Спас', 46, 0),
(681, 'Lentil, potatoes, carrots, onion, greens, spices + 2 slices of bread', '2019-02-01', 1, 'lentil-soup', 41, 1, '500.00', 'Ոսպ, կարտոֆիլ, գազար, սոխ, կանաչի, համեմուքներ+ 2 կտոր հացիկ', 'Чечевица, картофель, морковь, лук, зелень, специи + 2 ломтика хлеба', 'Lentil soup', 'Ոսպով ապուր', 'Суп с чечевицей', 47, 0),
(682, 'Blended beef meat, ham, sausage, sausages, pickled cucumber, greens, capers, lemon and olives, sour cream', '2019-02-01', 1, 'mix-solianka', 41, 1, '1250.00', 'Բլենդերով հարված տավարի միս, նրբերշիկ, խոզապուխտ, երշիկ, մարինացված վարունգ, կապար, ձիթապտուղ, կանաչի, կիտրոն, թթվասեր', 'Говяжье мясо, ветчина, сосиски, колбаса, маринованные огурцы, томатная паста, лимон, оливки, зелень, каперсы, провернутые в блендере, сметана', 'Mix-solianka', 'Միքս-սոլյանկա', 'Микс-солянка', 48, 0),
(683, 'Blended beef, carrots, beets, potatoes, сabbage, tomato paste, onion, greens, sour cream', '2019-02-01', 1, 'mix-borsch', 41, 1, '850.00', 'Բլենդերով հարված տավարի միս, գազար, բազուկ, կարտոֆիլ, կաղամբ, տոմատի մածուկ, սոխ, կանաչի, թթվասեր', 'Говяжье мясо, морковь, свекла, картофель, капуста, томатная паста, лук, зелень, провернутые в блендере, сметана', 'Mix-borsch', 'Միքս-բորշչ', 'Микс-борщ', 49, 0),
(684, 'Blended yogurt, wheat, greens and butter', '2019-02-01', 1, 'mix-spas', 41, 1, '600.00', 'Բլենդերով հարված մածուն, ձավար, կարագ, կանաչի', 'Мацун, пшеница, сливочное масло, зелень, провернутые в  блендере', 'Mix-spas', 'Միքս-սպաս', 'Микс-спас', 50, 0),
(685, 'Blended chicken, potatoes, carrots, onions, rice, greens, bulgarian peppers', '2019-02-01', 1, 'mix-chicken-soup', 41, 1, '850.00', 'Բլենդերով հարված hավի միս, կարտոֆիլ, գազար, սոխ, բրինձ, կանաչի, բիբար', 'Куриное мясо, картофель, морковь, лук, рис, зелень, болгарский перец, провернутые в блендере', 'Mix-chicken soup', 'Միքս-հավապուր', 'Микс-куриный суп', 51, 0),
(686, '3 pcs of dried lavash and 1 soft lavash, garlic and radish,', '2019-02-01', 1, 'khash-without-meat', 41, 1, '1350.00', '3 կտոր չոր լավաշ, 1 կտոր փափուկ լավաշ, բողկ, սխտոր', '3 куска черствого лаваша, 1 кусок мягкого лаваша, чеснок и редька', 'Khash without meat', 'Խաշ առանց մսի', 'Хаш без мяса', 52, 0),
(687, '3 pics of dried lavash, 1 soft lavash, garlic and radish,', '2019-02-01', 1, 'khash-with-meat', 41, 1, '1950.00', '3 կտոր չոր լավաշ, 1 կտոր փափուկ լավաշ, բողկ, սխտոր', '3 куска черствого лаваша, 1 кусок мягкого лаваша, чеснок,  редька.', 'Khash with meat', 'Խաշ մսով', 'Хаш с мясом', 53, 0),
(688, 'veal, sour cream, cream (rezhan), coriander', '2019-02-01', 1, 'karas-khashlama', 42, 1, '4900.00', 'մատղաշ հորթի միս, թթվասեր, կաթի սեր (ռեժան), համեմ', 'телятина, сметана, густые сливки (режан), кориандр', 'Karas khashlama', 'Կարաս խաշլամա', 'Карас хашлама', 42, 0),
(689, 'original dish, which is made of beef tale,zelandais butter, tomato juice and spices', '2019-02-01', 1, 'fried-beef-tail', 42, 1, '2400.00', 'յուրօրինակ ուտեստ, որը պատրաստվում է տավարի\nպոչից, զելանդական կարագով, լոլիկի հյութով եւ\nհամեմունքներով', 'оригинальное блюдо, которое готовится из говяжего хвоста, зеландского масла, томатного сока и специй', 'Fried beef tail', 'Տավարի տապակած պոչ', 'Жареный говяжий ховст', 43, 0),
(690, '8 pcs, yogurt + 2 pcs of bread', '2019-02-01', 1, 'dolma-with-grape-leaves', 42, 1, '1100.00', '8 հատ, մածուն, 2 կտոր հացիկ', '8 шт., мацун, + 2 куска хлеба', 'Dolma with grape leaves', 'Տոլմա թփով', 'Долма из виноградных листьев', 44, 0),
(691, '8 pcs + 2 pcs of bread', '2019-02-01', 1, 'dolma-with-cabbage', 42, 1, '1100.00', '8 հատ, 2 կտոր հացիկ', '8 штук + 2 куска хлеба', 'Dolma with cabbage', 'Տոլմա կաղամբով', 'Долма с капустой', 45, 0),
(692, 'Lamb, tomato, peppers and greens', '2019-02-01', 1, 'lamb-khashlama', 42, 1, '2400.00', 'Գառան միս, սոխ, լոլիկ, կանաչ պղպեղ', 'Мясо ягненка, помидоры, болгарский перец, зелень', 'Lamb khashlama', 'Գառան խաշլամա', 'Хашлама из баранины', 46, 0),
(693, 'Sliced beef heart and lungs, onions, greens, tomato paste', '2019-02-01', 1, 'tjvjeek', 42, 1, '1300.00', 'Տավարի սիրտ, թոք, տոմատ, կանաչի, սոխ, լավաշ', 'Говяжьи потроха (сердце и легкие), лук, томатная паста, зелень', 'Tjvjeek', 'Տժվժիկ', 'Тжвжик', 47, 0),
(694, 'Chicken breast, tomato paste, onions, greens, lavash', '2019-02-01', 1, 'chicken-chakhokhbili', 42, 1, '1350.00', 'Հավի միս, տոմատ, սոխ, կանաչի, լավաշ', 'Куриная грудка, томатная паста, лук, зелень, лаваш', 'Chicken chakhokhbili', 'Հավի չախոխբիլի', 'Чахохбили из курицы', 48, 0),
(695, 'Wheat, chicken, butter + 2 pcs of bread', '2019-02-01', 1, 'harisa', 42, 1, '850.00', 'Հավի միս, ձավար, կարագ + 2 կտոր հացիկ', 'Куриное мясо, пшеница, сливочное масло + 2 куска хлеба', 'Harisa', 'Հարիսա', 'Ариса', 49, 0),
(696, 'Beef, bulgur, eggs, greens, spices, nuts, 1 pcs', '2019-02-01', 1, 'ishli-kufta', 42, 1, '450.00', 'Տավարի միս, բլղուր, ձու, կանաչի, համեմունքներ, ընկույզ, 1 հատ', 'Говядина, булгур, яйца, зелень, специи, орехи, 1 шт', 'Ishli kufta', 'Իշլի քյուֆթա', 'Ишли кюфта', 50, 0),
(697, 'Beef, greens, onion, garlic; min. 5 units for order', '2019-02-01', 1, 'khinkali', 42, 1, '200.00', 'Տավարի միս, կանաչի, սոխ, սխտոր; պատվերներն ընդունվում են 5 հատից սկսած', 'Говядина, зелень, лук, чеснок; мин. кол-во для заказа - 5 штук', 'Khinkali', 'Խինկալի', 'Хинкали', 51, 0),
(698, 'Potato, mozarella, New Zealand butter, milk, ham', '2019-02-01', 1, 'mr-potato', 42, 1, '950.00', 'Կարտոֆիլ, մոցառելլա, կարագ զելանդական, կաթ, խոզապուխտ', 'Картофель, моцарелла, новозеландское сл.масло, молоко, ветчина', 'Mr. Potato', 'Պարոն Կարտոշկա', 'Господин Картошка', 52, 0),
(699, 'Potatoes, marinated hot peppers, oil, lavash', '2019-02-01', 1, 'fried-potatoes', 42, 1, '550.00', 'Կարտոֆիլ, ձեթ, լավաշ, թթու ծիծակ', 'Картофель, маринованный острый перец, пост.масло, лаваш', 'Fried potatoes', 'Կարտոֆիլի տապակա', 'Жареная картошка', 53, 0),
(700, 'potatoes, mushrooms, oil, greens, lavash', '2019-02-01', 1, 'fried-potatoes-with-mushrooms', 42, 1, '950.00', 'կարտոֆիլ, սունկ, ձեթ, կանաչի, լավաշ', 'картофель, грибы, масло, зелень, лаваш', 'Fried potatoes with mushrooms', 'Կարտոֆիլի տապակա սնկով', 'Жареный картофель с грибами', 54, 0),
(701, 'Sausages. potatoes, fresh greens, marinated hot pepper, lavash, bulg. pepper', '2019-02-01', 1, 'temur', 42, 1, '1150.00', 'Կարտոֆիլ, սարդելկա, կանաչ պղպեղ, կանաչի, լավաշ, թթու ծիծակ', 'Сардельки, картофель, свежая зелень, острый маринованный перец, сладкий перец, лаваш', 'Temur', 'Թեմուր', 'Темур', 55, 0),
(702, 'Pork, potatoes, onions, fresh greens, lavash, pickled hot peppers.', '2019-02-01', 1, 'fried-potatoes-with-pork', 42, 1, '1750.00', 'Խոզի միս, կարտոֆիլ, սոխ, կանաչի, լավաշ, թթու ծիծակ', 'Свинина, картофель, лук, свежая зелень, лаваш, маринованный острый перец', 'Fried potatoes with pork', 'Խոզի տապակա կարտոֆիլով', 'Жаркое из свинины с картошкой', 56, 0),
(703, 'Chicken breast, oil,  lavash, cabbage salad', '2019-02-01', 1, 'chicken-languette', 42, 1, '650.00', 'Հավի կրծքամիս, ձեթ, լավաշ, աղցան կաղամբով', 'Куриная грудка, пост. масло, лаваш, салат с капустой', 'Chicken languette', 'Հավի լանգետ', 'Лангет куриный', 57, 0),
(704, 'Chicken breast, dutch cheese, lavash, oil, cabbage salad', '2019-02-01', 1, 'chicken-languette-with-cheese', 42, 1, '800.00', 'Հավի կրծքամիս, հոլանդական պանիր, ձեթ, լավաշ, աղցան կաղամբով', 'Куриная грудка, голландский сыр, пост. масло, лаваш, салат с капустой', 'Chicken languette with cheese', 'Լանգետ հավով և պանրով', 'Лангет с курицей и сыром', 58, 0),
(705, 'Chicken breast, grated potatoes, lavash, cabbage salad', '2019-02-01', 1, 'languette-ministerial', 42, 1, '800.00', 'Հավի կրծքամիս՝ պատված քերիչով անցկացրած կարտոֆիլով, լավաշ, աղցան կաղամբով', 'Куриная грудка, тертый картофель, лаваш, салат с капустой', 'Languette Ministerial', 'Լանգետ նախարարական', 'Лангет Министерский', 59, 0),
(706, 'Beef, lavash, cabbage salad', '2019-02-01', 1, 'beef-languette', 42, 1, '1400.00', 'Տավարի միս, լավաշ, աղցան կաղամբով', 'Говядина, лаваш, салат с капустой', 'Beef Languette', 'Լանգետ տավարի', 'Лангет с говядиной', 60, 0),
(707, 'Small chicken legs, sauteed in spicy sauce, lavash, 2 sauces', '2019-02-01', 1, 'spicy-chicken-legs', 42, 1, '1800.00', 'Փոքր բդիկներ՝ կծու սոուսով տապակված, լավաշ, 2 սոուս', 'Маленькие окорочка, обжаренные в остром соусе, лаваш, 2 соуса', 'Spicy chicken legs', 'Հավի կծու բդիկներ', 'Острые куриные ножки', 61, 0),
(708, '1 cultet, lavash', '2019-02-01', 1, 'chicken-cutlets', 42, 1, '600.00', '1 կոտլետ, լավաշ', '1 котлета, лаваш', 'Chicken cutlets', 'Հավի կոտլետ', 'Куриные котлеты', 62, 0),
(709, '2 eggs, oil, lavash', '2019-02-01', 1, 'fried-eggs', 42, 1, '500.00', '2 ձու, ձեթ, լավաշ', '2 яйца, пост. масло, лаваш', 'Fried Eggs', 'Ձվածեղ', 'Яичница', 63, 0),
(710, '3 eggs, tomatoes, oil, lavash', '2019-02-01', 1, 'fried-eggs-with-tomatoes', 42, 1, '800.00', '3 ձու, լոլիկ, ձեթ, լավաշ', '3 яйца, помидоры, пост. масло, лаваш', 'Fried Eggs with tomatoes', 'Ձվածեղ Լոլիկով', 'Яичница с помидорами', 64, 0),
(711, '3 eggs, peas, oil, lavash', '2019-02-01', 1, 'fried-eggs-with-peas', 42, 1, '850.00', '3 ձու, ոլոռ, ձեթ, լավաշ', '3 яйца, горох, пост. масло, лаваш', 'Fried Eggs with peas', 'Ձվածեղ ոլոռով', 'Яичница с зеленым горошком', 65, 0),
(712, '3 eggs, basturma, oil, lavash', '2019-02-01', 1, 'fried-eggs-with-basturma', 42, 1, '950.00', '3 ձու, բաստուրմա, ձեթ, լավաշ', '3 яйца, бастурма, пост. масло, лаваш', 'Fried Eggs with basturma', 'Ձվածեղ բաստուրմայով', 'Яичница с бастурмой', 66, 0),
(713, '3 eggs, ham, oil, lavash', '2019-02-01', 1, 'fried-eggs-with-ham', 42, 1, '850.00', '3 ձու, խոզապուխտ, ձեթ, լավաշ', '3 яйца, ветчина, пост. масло, лаваш', 'Fried Eggs with ham', 'Ձվածեղ խոզապուխտով', 'Яичница с ветчиной', 67, 0),
(714, 'Potatoes, butter, lavash', '2019-02-01', 1, 'mashed-potatoes', 43, 1, '450.00', 'Կարտոֆիլ, կարագ, լավաշ', 'Картофель, сл. масло, лаваш', 'Mashed potatoes', 'Պյուրե', 'Картофельное пюре', 43, 0),
(715, 'Buckwheat, butter, lavash', '2019-02-01', 1, 'buckwheat', 43, 1, '450.00', 'Հնդկաձավար, կարագ, լավաշ', 'Гречка, сл.масло, лаваш', 'Buckwheat', 'Հնդկաձավար', 'Гречка', 44, 0),
(716, 'Spaghetti, butter, lavash', '2019-02-01', 1, 'spaghetti', 43, 1, '450.00', 'Սպագետտի, կարագ, լավաշ', 'Спагетти, сл. масло, лаваш', 'Spaghetti', 'Սպագետի', 'Спагетти', 45, 0),
(717, 'Rice, butter, lavash', '2019-02-01', 1, 'rice', 43, 1, '450.00', 'Բրինձ, կարագ, լավաշ', 'Рис, сл. масло, лаваш', 'Rice', 'Բրինձ', 'Рис', 46, 0);
INSERT INTO `posts` (`id`, `content_en`, `publish`, `status`, `slug`, `category_id`, `user_id`, `price`, `content_hy`, `content_ru`, `title_en`, `title_hy`, `title_ru`, `order`, `popular`) VALUES
(718, 'Rice, corn, greens, oil, lavash', '2019-02-01', 1, 'rice-with-corn', 43, 1, '500.00', 'Բրինձ, եգիպտացորեն, կանաչի, ձեթ, լավաշ', 'Рис, кукуруза, зелень, пост. масло, лаваш', 'Rice with corn', 'Բրինձ եգիպտացորենով', 'Рис с кукурузой', 47, 0),
(719, 'Potatoes, ketchup', '2019-02-01', 1, 'fries', 43, 1, '400.00', 'Կարտոֆիլ, կետչուպ', 'Картофель, кетчуп', 'Fries', 'Ֆրի', 'Картошка фри', 48, 0),
(720, 'Bread', '2019-02-01', 1, 'bread', 44, 1, '300.00', 'Հաց', 'Хлеб', 'Bread', 'Հաց', 'Хлеб', 44, 0),
(721, '.', '2019-02-01', 1, 'lori-cheese', 44, 1, '700.00', '.', '.', 'Lori cheese', 'Պանիր /լոռի/', 'Сыр /лори/', 45, 0),
(722, 'Roquefort, Dutch cheese, smoked cheese, \"Lori\" cheese', '2019-02-01', 1, 'cheese-assortment', 44, 1, '1900.00', 'Ռոքֆոր, հոլանդական պանիր, ապխտած պանիր, լոռի', 'Рокфор, голландский сыр, копченый сыр, сыр Лори', 'Cheese Assortment', 'Պանրի տեսականի', 'Сырное ассорти', 46, 0),
(723, 'Basturma, soujoukh, Pepperoni, ham', '2019-02-01', 1, 'meat-assortment', 44, 1, '1900.00', 'Բաստուրմա, սուջուխ, պեպերոնի երշիկ, խոզապուխտ', 'Бастурма, суджук, колбаса Пепперони, ветчина', 'Meat assortment', 'Մսի տեսականի', 'Мясное ассорти', 47, 0),
(724, 'Pickles, cabbage, cauliflower, hot green pepper', '2019-02-01', 1, 'pickles', 44, 1, '750.00', 'Վարունգ, կաղամբ, ծաղկակաղամբ, ծիծակ', 'Огурцы, капуста, цветная капуста, острый зеленый перец', 'Pickles', 'Թթու', 'Соленье', 48, 0),
(725, 'Fresh greens (seasonal)', '2019-02-01', 1, 'greens', 44, 1, '600.00', 'Խառը կանաչի /սեզոնային/', 'Зелень свежая (сезонная)', 'Greens', 'Կանաչի', 'Зелень', 49, 0),
(726, 'Strained yogurt, nuts', '2019-02-01', 1, 'strained-yogurt', 44, 1, '500.00', 'Քամած մածուն, ընկույզ', 'Сцеженный мацони, орехи', 'Strained yogurt', 'Քամած մածուն', 'Сцеженным мацони', 50, 0),
(727, '3-4 pork legs', '2019-02-01', 1, 'mozhozh', 44, 1, '800.00', '3-4 հատ խոզի տոտիկ', '3-4 свиные ножки', 'Mozhozh', 'Մոժոժ', 'Можож', 51, 0),
(728, '2 pcs in cabbage, 2 pcs in grape leaves, lavash', '2019-02-01', 1, 'lenten-dolma', 44, 1, '650.00', '2 հատ կաղամբով, 2 հատ թփով, լավաշ', '2 шт. в капусте, 2 шт. в виноградных листьях, лаваш', 'Lenten dolma', 'Պասուց տոլմա', 'Постная долма', 52, 0),
(729, 'Black olives', '2019-02-01', 1, 'olives', 44, 1, '700.00', 'Սև ձիթապտուղ', 'Черные маслины', 'Olives', 'Ձիթապտուղ', 'Оливки', 53, 0),
(730, '1 pcs, boiled or roasted', '2019-02-01', 1, 'corn', 44, 1, '400.00', '1 հատ, խորոված կամ եփած', '1 шт., вареная или печеная', 'Corn', 'Եգիպտացորեն', 'Кукуруза', 54, 0),
(731, 'Roasted hot pepper', '2019-02-01', 1, 'hot-pepper', 44, 1, '300.00', 'Խորոված կծու պղպեղ 1 հատ', 'Печеный острый перец', 'Hot pepper', 'Կծու պղպեղ', 'Острый перец', 55, 0),
(732, 'Lemon', '2019-02-01', 1, 'lemon', 44, 1, '450.00', 'Կիտրոն', 'Лимон', 'Lemon', 'Կիտրոն', 'Лимон', 56, 0),
(733, 'Butter', '2019-02-01', 1, 'butter', 44, 1, '200.00', 'Կարագ', 'Масло', 'Butter', 'Կարագ', 'Масло', 57, 0),
(734, 'Lavash', '2019-02-01', 1, 'lavash', 44, 1, '100.00', 'Lավաշ', 'Лаваш', 'Lavash', 'Lավաշ', 'Лаваш', 58, 0),
(735, 'chicken breast, breadcrumbs, spices\n*served with a sauce', '2019-02-01', 1, 'nugget', 45, 1, '1500.00', 'հավի կրծքամիս, չորահաց, ձեթ, համեմունքներ\n*մատուցվում է սոուսով', 'куриная грудка, сухарики, специи\n*подается с соусом', 'Nugget', 'Նագեթ', 'Нагет', 45, 0),
(736, 'Chicken shaurma: chicken, french fries, tomatoes, hot peppers, lettuce, greens, sauce, mayonnaise, ketchup,loshik.', '2019-02-01', 1, 'chicken-shaurma', 45, 1, '800.00', 'Հավի միս, կծու բիբար, լոլիկ, մառոլ, ֆրի, կանաչի,  կետչուպ, մայոնեզ, սոուս, լոշիկ', 'Куриная шаурма: курица, картофель фри, помидоры, острый перец, листья салата, зелень, соус, майонез, кетчуп ,лошик.', 'Chicken shaurma', 'Հավի շաուրմա', 'Куриная шаурма', 46, 0),
(737, 'Pork meat, spicy peppers, tomatoes, lettuce, fries, greens, ketchup, mayonnaise, sauce, loshik', '2019-02-01', 1, 'pork-shaurma', 45, 1, '900.00', 'Խոզի միս, կծու բիբար, լոլիկ, մառոլ, ֆրի , կանաչի ,կետչուպ, մայոնեզ, սոուս, լոշիկ', 'Свинина, острый перец, помидоры, салатные листья, фри, зелень, кетчуп, майонез, соус, лошик', 'Pork shaurma', 'Խոզի շաուրմա', 'Шаурма свинины', 47, 0),
(738, 'Chicken meat, spicy peppers, tomatoes, lettuce, fries, greens, ketchup, mayonnaise, sauce, Korean salad, 35cm bread', '2019-02-01', 1, 'chicken-mega-shaurma', 45, 1, '1200.00', 'Հավի միս, կծու բիբար, լոլիկ, մառոլ, ֆրի , կանաչի,  կետչուպ, մայոնեզ, սոուս, կորեական աղցան, 35սմ հաց', 'Куриное мясо, острый перец, помидоры, салатные листья, картофель фри, зелень, кетчуп, майонез, соус, корейский салат, 35 см хлеб', 'Chicken mega shaurma', 'Հավի մեգա շաուրմա', 'Куриная мега шаурма', 48, 0),
(739, 'Pork, spicy peppers, tomatoes, lettuce, fries, greens, ketchup, mayonnaise, sauce, Korean salad, 35cm bread', '2019-02-01', 1, 'pork-mega-shaurma', 45, 1, '1300.00', 'Խոզի միս, կծու բիբար, լոլիկ, մառոլ, ֆրի , կանաչի , կետչուպ, մայոնեզ, սոուս,կորեական աղցան, 35սմ հաց', 'Свинина, острый перец, помидоры, салат, фри, зелень, кетчуп, майонез, соус, корейский салат, 35 см хлеб', 'Pork mega shaurma', 'Խոզի մեգա շաուրմա', 'Свиная мега шаурма', 49, 0),
(740, 'Bulgarian Vinegret + chicken shawarma + tan 0,5l', '2019-02-01', 1, 'combo-with-chicken-shawarma', 45, 1, '1300.00', 'Բուլղարական վինեգրետ + հավի շաուրմա + թան 0,5լ', 'Болгарский винегрет + шаурма с курицей + тан 0,5 л', 'Combo with chicken shawarma', 'Կոմբո հավի շաուրմայով', 'Комбо с куриной шаурмой', 50, 0),
(741, 'Strained yogurt salad + pork shawarma + Coca-Cola 0,25 l', '2019-02-01', 1, 'combo-with-pork-shawarma', 45, 1, '1300.00', 'Խոզի շաուրմա, քամած մածունով աղցան, կոլա 0.25լ', 'Салат с сцеженным мацони + шаурма со свининой + Кока-Кола 0,25л', 'Combo with pork shawarma', 'Կոմբո խոզի շաուրմայով', 'Комбо со свининой шаурмой', 51, 0),
(742, 'Chicken cutlet, cheese, lettuce, tomatoes, pickles, ketchup, mayonnaise.', '2019-02-01', 1, 'hamburger-with-chicken-and-cheese', 45, 1, '850.00', 'Հավի կոտլետ, հոլանդական պանիր, լոլիկ, մարինացված վարունգ, կետչուպ, մայոնեզ, մառոլ', 'Куриная котлета, голландский сыр, маринованные огурцы, листья салата, помидор, кетчуп, майонез.', 'Hamburger with chicken and cheese', 'Համբուրգեր հավի մսով ու պանրով', 'Гамбургер с курицей и сыром', 52, 0),
(743, 'Beef cutlet, cheese, lettuce, tomatoes, pickles, ketchup, mayonnaise', '2019-02-01', 1, 'hamburger-with-beef-and-cheese', 45, 1, '1150.00', 'Տավարի կոտլետ, հոլանդական պանիր, լոլիկ, մարինացված վարունգ, կետչուպ, մայոնեզ, մառոլ', 'Говяжья котлета, голландский сыр, листья салата, маринованные огурцы, помидоры, кетчуп, майонез', 'Hamburger with beef and cheese', 'Համբուրգեր տավարի մսով ու պանրով', 'Гамбургер с говядиной и сыром', 53, 0),
(744, 'Chicken hamburger, fries , tan \"Karas\" 0.5l.', '2019-02-01', 1, 'combo-with-chicken-hamburger', 45, 1, '1300.00', 'Հավի  hամբուրգեր, ֆրի, թան ՛՛Կարաս՛՛ 0.5լ', 'Куриный гамбургер, фри, тан «Карас» 0.5л.', 'Combo with chicken hamburger', 'Կոմբո հավի համբուրգերով', 'Комбо с куриным гамбургером', 54, 0),
(745, 'Beef humburger, Coca-Cola 0,25 ll', '2019-02-01', 1, 'combo-with-beef-hamburger', 45, 1, '1500.00', 'Տավարի համբուրգեր, ֆրի, կոլա 0.25լ', 'Гамбургер с говядиной, Кока-Кола 0,25л', 'Combo with beef hamburger', 'Կոմբո տավարի բուրգերով', 'Комбо с говяжьим бургером', 55, 0),
(746, 'Chicken breast, mayonnaise, dutch cheese, pickled cucumber, soy sauce, french fries', '2019-02-01', 1, 'club-sandwich', 45, 1, '950.00', 'Հավի կրծքամիս, մայոնեզ, հոլանդական պանիր, թթու վարունգ, սոյաի սոուս, ֆրի', 'Куриная грудка, майонез, голландский сыр, маринованный огурец, соевый соус, картофель-фри', 'Club sandwich', 'Քլաբ Սենդվիչ', 'Клаб сендвич', 56, 0),
(747, 'Ham, cheese, tomatoes, ketchup, mayonnaise.', '2019-02-01', 1, 'sandwich', 45, 1, '500.00', 'Խոզապուխտ, հոլանդական պանիր, լոլիկ, կետչուպ, մայոնեզ:', 'Ветчина, голландский сыр, помидор, кетчуп, майонез.', 'Sandwich', 'Սենդվիչ', 'Cэндвич', 57, 0),
(748, 'Ginger, Dutch cheese, tomatoes, ketchup, mayonnaise, lettuce, 35cm bread', '2019-02-01', 1, 'mega-sandwich', 45, 1, '850.00', 'Խոզապուխտ, հոլանդական պանիր,լոլիկ, կետչուպ, մայոնեզ, մառոլ, 35սմ հաց', 'Ветчина, сыр, кетчуп, майонез, помидоры, листья салата,35 см хлеб', 'Mega sandwich', 'Մեգա սենդվիչ', 'Мега сэндвич', 58, 0),
(749, 'Sausage, greens, ketchup and mayonnaise, mustard.', '2019-02-01', 1, 'hot-dog', 45, 1, '300.00', 'Նրբերշիկ, կանաչի, կետչուպ և մայոնեզ:', 'Сосиска, зелень, кетчуп и майонез, горчица.', 'Hot dog', 'Հոթ-դոգ', 'Xот-дог', 59, 0),
(750, 'Sausages, ketchup, mayonnaise, greens, tomatoes, cucumber, fries, 35 cm bread', '2019-02-01', 1, 'mega-hot-dog', 45, 1, '600.00', 'Նրբերշիկ, կետչուպ, մայոնեզ, կանաչի, լոլիկ, վարունգ, ֆրի, 35սմ հաց', 'Сосиска, кетчуп, майонез, зелень, помидоры, огурцы, фри, хлеб 35 см', 'Mega hot dog', 'Մեգա հոթ դոգ', 'Мега хот-дог', 60, 0),
(751, '25cm / 1 pcs', '2019-02-01', 1, 'lahmajo-26cm', 45, 1, '250.00', '26սմ տրամագծով /1 հատ', '26 см / 1шт.', 'Lahmajo, 26cm', 'Լահմաջո, 26սմ', 'Ламаджо, 26см', 61, 0),
(752, '25cm / 1 pcs', '2019-02-01', 1, 'lahmajo-with-meat-and-cheese', 45, 1, '450.00', '26սմ տրամագծով /1 հատ', '26 см / 1шт.', 'Lahmajo with meat and cheese', 'Լահմաջո մսով և պանրով', 'Ламаджо с мясом и сыром', 62, 0),
(753, 'Organic fries, sandwich with ham and cheese, natural juice 0.25, toy', '2019-02-01', 1, '-tasty-little-house-kid-s-box', 46, 1, '1200.00', 'Մանղալի ֆրի, խոզապուխտով ու պանրով սենդվիչ, բնական հյութ 0.25լ, խաղալիք', 'Органический картофель фри, сэндвич с сыром и ветчиной, натуральный сок 0,25мл, игрушка', '\"Tasty Little House\" kid\'s box', '\"Համով Տնակ\" մանկական բոքս', 'Детский бокс \"Вкусный Домик\"', 46, 0),
(754, 'Ham sandwich, cheese sandwich, organic fries, juice 0.25', '2019-02-01', 1, 'knowledge-lunch', 46, 1, '750.00', 'Ֆրի, խոզապուխտով սենդվիչ, պանրով սենդվիչ, բնական հյութ 0.25լ', 'Сэндвич с ветчиной, сэндвич с сыром, органический картофель фри, натуральный сок 0,25л', 'Knowledge lunch', 'Գիտելիքների լանչ', 'Ланч знаний', 47, 0),
(755, '.', '2019-02-01', 1, 'soup-macfic', 46, 1, '600.00', '.', '.', 'Soup Macfic', 'Ապուր Մակֆիկ', 'Суп Макфик', 48, 0),
(756, 'Cheese and ham; pizza character is made by tomatoes, olives and bulg. peppers (24 cm)', '2019-02-01', 1, 'kids-pizza-sponge-bob', 46, 1, '1300.00', 'Խոզապուխտ, պանիր, արտաքինը լոլիկով, բիբարով, ձիթապտուղով ձևավորվում է համապատասխան կերպարի տեսքով /24սմ/', 'Ветчина, сыр; внешность персонажа оформляется с помощью помидоров, сладкого перца и маслин (24 см)', 'Kids\' pizza \"Sponge Bob\"', 'Մանկական պիցցա \"Սպանջ Բոբ\"', 'Детская пицца \"Спанч Боб\"', 49, 0),
(757, 'chicken breast, breadcrumbs, spices', '2019-02-01', 1, 'crispy-nuggets', 46, 1, '800.00', 'հավի կրծքամիս, չորահաց, ձեթ, համեմունքներ', 'куриная грудка, сухарики, специи', 'Crispy nuggets', 'Խրթխրթան նագեթս', 'Хрустящие наггетсы', 50, 0),
(758, 'Cheese, sausage, sauce; character is made by tomatoes, bulg.peppers and olives (24 cm)', '2019-02-01', 1, 'kids-pizza-smile', 46, 1, '1100.00', 'Որսորդական երշիկ, պանիր, սոուս; արտաքինը ձևավորվում է լոլիկով, բիբարով, ձիթապտղով` \n համապատասխան կերպարի տեսքով /24սմ/', 'Сыр, охотничьи колбаски, соус; внешний вид персонажа оформляется помидорами, сладким перцем и маслинами (24 см)', 'Kids\' pizza «Smile»', 'Մանկական պիցցա \"Ժպիտ\"', 'Детская пицца \"Улыбка\"', 51, 0),
(759, 'Organic fries prepared on mangal - without additives, served with ketchup.', '2019-02-01', 1, 'fries-on-manghal', 46, 1, '400.00', 'Մանղալի վրա պատրաստված օրգանիկ ֆրի՝ առանց հավելումների, մատուցվում է կետչուպով:', 'Приготовленный на мангале органический картофель фри (без добавок), подается с кетчупом.', 'Fries on manghal', 'Մանղալի ֆրի', 'Картошка фри на мангале', 52, 0),
(760, '\"Eskimo\" dessert', '2019-02-01', 1, '-eskimo-dessert', 47, 1, '600.00', '\"Էսկիմո\" խմորեղեն', 'Десерт \"Эскимо\"', '\"Eskimo\" dessert', '\"Էսկիմո\" խմորեղեն', 'Десерт \"Эскимо\"', 47, 0),
(761, '\"Mikado\" dessert', '2019-02-01', 1, '-mikado-dessert', 47, 1, '600.00', '\"Միկադո\" խմորեղեն', 'Десерт \"Микадо\"', '\"Mikado\" dessert', '\"Միկադո\" խմորեղեն', 'Десерт \"Микадо\"', 48, 0),
(762, 'Season fruits', '2019-02-01', 1, 'fruit-assortment-half', 47, 1, '2000.00', 'Սեզոնային մրգեր', 'Сезонные фрукты', 'Fruit assortment (half)', 'Մրգի տեսականի (կես բաժին)', 'Ассортимент фруктов (половина порции)', 49, 0),
(763, 'Season fruits', '2019-02-01', 1, 'fruit-assortment-full', 47, 1, '3500.00', 'Սեզոնային մրգեր', 'Сезонные фрукты', 'Fruit assortment (full)', 'Մրգի տեսականի (ամբողջական)', 'Ассортимент фруктов (целая порция)', 50, 0),
(764, 'Hot sauce small', '2019-02-01', 1, 'hot-sauce-small', 48, 1, '100.00', 'Կծու սոուս փոքր', 'Острый соус маленький', 'Hot sauce small', 'Կծու սոուս փոքր', 'Острый соус маленький', 48, 0),
(765, 'White sauce', '2019-02-01', 1, 'white-sauce', 48, 1, '100.00', 'Սոուս սպիտակ', 'Соус белый', 'White sauce', 'Սոուս սպիտակ', 'Соус белый', 49, 0),
(766, 'Սոուս կարմիր', '2019-02-01', 1, 'red-sauce', 48, 1, '100.00', 'Սոուս կարմիր', 'Соус красный', 'Red sauce', 'Սոուս կարմիր', 'Соус красный', 50, 0),
(767, '.', '2019-02-01', 1, 'sauce-nasharab', 48, 1, '450.00', '.', '.', 'Sauce nasharab', 'Սոուս նաշարաբ', 'Соус нашараб', 51, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `order` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `image`, `created_at`, `updated_at`, `order`, `status`) VALUES
(1, '/slider/slide1-1.jpg', '2019-02-07', '2019-02-07', 1, 1),
(2, '/slider/slide1-2.jpg', '2019-02-07', '2019-02-07', 2, 1),
(3, '/slider/slide1-3.jpg', '2019-02-07', '2019-02-07', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_billing`
--

CREATE TABLE `users_billing` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `phone` varchar(128) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_billing`
--

INSERT INTO `users_billing` (`id`, `first_name`, `last_name`, `email`, `phone`, `district_id`, `address`, `user_id`) VALUES
(13, 'david2', 'davo121992', 'davidmkrtchyan1992@mail.ru', '+37477850709', 4, 'my address', 1),
(14, NULL, NULL, '', NULL, NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users_profile`
--

CREATE TABLE `users_profile` (
  `id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_profile`
--

INSERT INTO `users_profile` (`id`, `image`, `user_id`) VALUES
(1, 'default.jpg', 1),
(2, 'default.jpg', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `categoryies`
--
ALTER TABLE `categoryies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD UNIQUE KEY `categoryies_slug_parent_id_4efac341_uniq` (`slug`,`parent_id`),
  ADD KEY `categoryies_lft_943bef6b` (`lft`),
  ADD KEY `categoryies_rght_7842c853` (`rght`),
  ADD KEY `categoryies_tree_id_5de46d11` (`tree_id`),
  ADD KEY `categoryies_level_8d1115fb` (`level`),
  ADD KEY `categoryies_parent_id_f55e9c12` (`parent_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contacts_user_id_7a55ae84_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_7e2523fb_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `order_billing`
--
ALTER TABLE `order_billing`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `order_billing_order_id_3ea7f045_uniq` (`order_id`),
  ADD KEY `order_billing_district_id_e82fd990` (`district_id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_products_order_id_b5077dc2_fk_orders_id` (`order_id`),
  ADD KEY `order_products_post_id_40bb784a_fk_posts_id` (`post_id`);

--
-- Indexes for table `personal_post`
--
ALTER TABLE `personal_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `posts_user_id_4291758d` (`user_id`),
  ADD KEY `posts_category_id_dbccff63` (`category_id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_billing`
--
ALTER TABLE `users_billing`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD UNIQUE KEY `users_billing_district_id_60192c44_uniq` (`district_id`);

--
-- Indexes for table `users_profile`
--
ALTER TABLE `users_profile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categoryies`
--
ALTER TABLE `categoryies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=769;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `order_billing`
--
ALTER TABLE `order_billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `personal_post`
--
ALTER TABLE `personal_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=768;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users_billing`
--
ALTER TABLE `users_billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users_profile`
--
ALTER TABLE `users_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `categoryies`
--
ALTER TABLE `categoryies`
  ADD CONSTRAINT `categoryies_parent_id_f55e9c12_fk_categoryies_id` FOREIGN KEY (`parent_id`) REFERENCES `categoryies` (`id`);

--
-- Constraints for table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_user_id_7a55ae84_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_7e2523fb_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `order_billing`
--
ALTER TABLE `order_billing`
  ADD CONSTRAINT `order_billing_order_id_3ea7f045_fk_orders_id` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);

--
-- Constraints for table `order_products`
--
ALTER TABLE `order_products`
  ADD CONSTRAINT `order_products_order_id_b5077dc2_fk_orders_id` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_products_post_id_40bb784a_fk_posts_id` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_category_id_dbccff63_fk_categoryies_id` FOREIGN KEY (`category_id`) REFERENCES `categoryies` (`id`),
  ADD CONSTRAINT `posts_user_id_4291758d_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `users_billing`
--
ALTER TABLE `users_billing`
  ADD CONSTRAINT `users_billing_district_id_60192c44_fk_districts_id` FOREIGN KEY (`district_id`) REFERENCES `districts` (`id`),
  ADD CONSTRAINT `users_billing_user_id_c23f9850_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `users_profile`
--
ALTER TABLE `users_profile`
  ADD CONSTRAINT `users_profile_user_id_2112e78d_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
