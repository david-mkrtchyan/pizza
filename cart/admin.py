from django.contrib import admin
from .models import Order, OrderProduct, OrderBilling
from import_export import resources
from cart.classes import order as orderVariable


class OrderAdmin(admin.ModelAdmin):
    # resource_class = OrderResource

    list_filter = ('status', 'show', 'payment_type')
    search_fields = ['total', 'qty'],
    # autocomplete_fields = ['title_en','title_ru']'title_hy',
    list_display = ('user', 'status', 'payment_type', 'total', 'qty',)

    change_form_template = 'admin/cart/cart/change_form.html'

    def suit_cell_attributes(self, obj, column):
        if obj.show == False:
            return {'class': 'text-warning'}

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}

        extra_context['orderBilling'] = OrderBilling.objects.get(order_id=object_id)
        extra_context['order_products'] = OrderProduct.objects.all().filter(order_id=object_id)

        return super().change_view(
            request, object_id, form_url, extra_context=extra_context,
        )

    def get_form(self, request, obj=None, **kwargs):
        if obj.show == 0:
            obj.show = 1
            if obj.status == orderVariable.statusNew and obj.payment_type == orderVariable.payment_type_cash:
                obj.status = orderVariable.statusPending
            obj.save()
        return super(OrderAdmin, self).get_form(request, obj, **kwargs)

    class Media:
        css = {
            'all': (
                '/media/admin/css/style.css',
                # 'https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css',
            )
        }


admin.site.register(Order, OrderAdmin)

# Register your models here.
