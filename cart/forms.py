from django import forms
from .models import OrderBilling
from phonenumber_field.modelfields import PhoneNumberField
from blog.models import District


class OrderBillingForm(forms.ModelForm):
    email = forms.EmailField(required=True)
    phone = PhoneNumberField()
    # district = forms.IntegerField(required=False)
    address = forms.CharField(max_length=255, required=False)
    diff_address = forms.BooleanField(required=False)
    # es pah@ petqa stugvi nor toxvi
    type = {'bank': 'bank', 'cash': 'cash', }
    payment_type = forms.ChoiceField(required=True,
                                     choices=[(v, v) for i, v in enumerate(type)]
                                     )
    district = forms.ChoiceField(required=False,
                                 choices=[(doc.id, doc.name) for doc in District.objects.order_by('-order').all()])

    class Meta:
        model = OrderBilling
        fields = ['first_name', 'last_name', 'email', 'phone', 'address', 'diff_address', 'order_note', ]
        exclude = ("order","district",)
