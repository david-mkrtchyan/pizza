from cart.models import Order as OrderModel, OrderProduct

session_name = 'databasket'

statusNew = 'new'
statusPending = 'pending'
statusPay = 'pay'

payment_type_bank = 'bank'
payment_type_cash = 'cash'


class Order(object):

    @staticmethod
    def addOrder(request, orderBillingForm):
        try:
            basket = request.session.get(session_name)
            if basket:
                quantity = basket['data'][0]['quantity']
                price = basket['data'][0]['price']
                orderModel = OrderModel(user=request.user, status=statusNew, show=False, total=price, qty=quantity,
                                        payment_type=request.POST['payment_type'])
                orderModel.save()

                for i, k in basket.items():
                    if i != 'data':
                        for data in k:
                            quantity = data['quantity']
                            productId = data['productId']
                            price = data['price']
                            orderProductModel = OrderProduct(order=orderModel, post_id=productId, price=price,
                                                             qty=quantity)
                            orderProductModel.save()

                form = orderBillingForm.save(commit=False)
                form.order = orderModel
                form.save()

                del request.session[session_name]

                return True
        except:
            return False
