from blog.models import Images
from django.template.loader import render_to_string
from django.utils.translation import to_locale, get_language
import json

session_name = 'databasket'


class Cart(object):

    def __init__(self, quantity, post, request):
        self.quantity = quantity
        self.post = post
        self.request = request

    def addCart(self):
        post = self.post
        productId = post.id
        quantity = self.quantity
        request = self.request

        price = post.price
        request.session.set_expiry = 60
        data = request.session.get(session_name, [])
        image = Images.objects.filter(type='post', object_id=post.id).order_by('-rank').first()
        path = ''
        if image:
            path = str(image.path)

        try:
            if data and (f'{productId}' in data):
                data = data[f'{productId}'][0]
                quantityAll = int(data['quantity']) + int(quantity)
                priceAll = int(data['price']) + int(price) * int(quantity)
                dct = dctTotal = {}
                dct.setdefault(productId, []).append({
                    'quantity': int(quantityAll),
                    'productId': int(productId),
                    'price': int(price),
                    'name': {'hy': str(post.title_hy), 'ru': str(post.title_ru), 'en': str(post.title_en)},
                    'image': str(path),
                    'slug': str(post.slug),
                })

                basket = request.session[session_name]

                del basket[f'{productId}']

                dctTotal.setdefault('data', []).append({
                    'quantity': int(basket['data'][0]['quantity']) + int(quantity),
                    'price': int(basket['data'][0]['price']) + int(quantity) * int(price)
                })

                basket.update(dct)
                basket.update(dctTotal)
                request.session[session_name] = basket
            else:
                dct = {}
                dct.setdefault(productId, []).append({
                    'quantity': int(quantity),
                    'productId': int(productId),
                    'price': int(price),
                    'name': {'hy': str(post.title_hy), 'ru': str(post.title_ru), 'en': str(post.title_en)},
                    'image': str(path),
                    'slug': str(post.slug),
                })

                dctTotal = {}

                if session_name not in request.session:
                    request.session[session_name] = {}
                    basket = request.session[session_name]
                    dctTotal.setdefault('data', []).append({
                        'quantity': int(quantity),
                        'price': int(quantity) * int(price)
                    })

                else:
                    basket = request.session[session_name]
                    dctTotal.setdefault('data', []).append({
                        'quantity': int(basket['data'][0]['quantity']) + int(quantity),
                        'price': int(basket['data'][0]['price']) + int(quantity) * int(price)
                    })

                basket.update(dct)
                basket.update(dctTotal)
                request.session[session_name] = basket
            request.session.modified = True
            return True
        except Exception as e:
            return False

    def remove(self):
        data = self.request.session.get(session_name, [])
        productId = self.post.id

        if data and (f'{productId}' in data):
            data = data[f'{productId}'][0]

            quantity = int(data['quantity'])
            price = int(data['price'])

            basket = self.request.session[session_name]

            dctTotal = {}
            dctTotal.setdefault('data', []).append({
                'quantity': int(basket['data'][0]['quantity']) - quantity,
                'price': int(basket['data'][0]['price']) - quantity * price
            })

            del self.request.session[session_name][f'{productId}']

            basket.update(dctTotal)

            self.request.session.modified = True

            return True

        return False

    @property
    def refreshSmallCart(self):
        basket = self.request.session.get(session_name)
        totaldata = basket['data']
        html = render_to_string('cart/template/small_cart.html',
                                {'data': basket, 'totaldata': totaldata, 'count': len(basket) - 1,
                                 'lang': to_locale(get_language())})
        return html

    @staticmethod
    def getSessionObjectForView(basket):
        lang = to_locale(get_language())
        if basket:
            totaldata = basket['data']
            # basket = dict(itertools.islice(basket.items(), 4))
            return {'data': basket, 'totaldata': totaldata, 'count': len(basket), 'lang': lang}
        return {'data': '', 'totaldata': '', 'count': 0, 'lang': lang}

    @staticmethod
    def updateCart(request, data):
        try:
            basket = request.session.get(session_name)
            fromJs = data.encode('utf-8')
            fromJs = json.loads(fromJs)
            totalprice = totalquantity = 0
            for val in fromJs:
                if f'{val["id"]}' in basket:
                    basket[f'{val["id"]}'][0]['quantity'] = val['quantity']
                    totalprice = totalprice + basket[f'{val["id"]}'][0]['quantity'] * basket[f'{val["id"]}'][0]['price']
                    totalquantity = totalquantity + basket[f'{val["id"]}'][0]['quantity']
                else:
                    return False
            basket['data'][0]['quantity'] = totalquantity
            basket['data'][0]['price'] = totalprice
            request.session.modified = True
        except:
            return False

        return basket
