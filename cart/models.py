from django.db import models
from django.contrib.auth.models import User
from django_mysql.models import EnumField
from phonenumber_field.modelfields import PhoneNumberField
from post.models import Post
from blog.models import District as DistrictModel


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=True)
    status = EnumField(choices=['new', 'pending', 'pay'], default=('new'))
    show = models.BooleanField(default=False)
    total = models.IntegerField()
    qty = models.IntegerField()
    payment_type = EnumField(choices=['bank', 'cash'], default=('bank'))

    class Meta:
        db_table = "orders"


class OrderProduct(models.Model):
    order = models.ForeignKey(Order, on_delete=True)
    post = models.ForeignKey(Post, on_delete=True)
    price = models.IntegerField()
    qty = models.IntegerField()

    class Meta:
        db_table = "order_products"


class OrderBilling(models.Model):
    order = models.OneToOneField(Order, on_delete=True)
    first_name = models.CharField(max_length=255, blank=True, null=True, )
    last_name = models.CharField(max_length=255, blank=True, null=True, )
    email = models.EmailField()
    phone = PhoneNumberField(blank=True, null=True)
    district = models.ForeignKey(DistrictModel, on_delete=models.CASCADE, blank=True, null=True)
    address = models.CharField(blank=True, null=True, max_length=255)
    diff_address = models.BooleanField(blank=True, null=True, )
    order_note = models.TextField(max_length=255, blank=True, null=True)

    class Meta:
        db_table = "order_billing"
