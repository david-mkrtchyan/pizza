from django.urls import path
from . import views

urlpatterns = [
    path('add-to-cart', views.addToCart, name='add-to-cart'),
    path('remove-product', views.remove, name='remove-product'),
    path('cart', views.cart, name='cart'),
    path('update-cart', views.updateCart, name='update-cart'),
    path('checkout', views.checkout, name='checkout'),
    path('bank', views.bank, name='bank'),
]
