from django.http import JsonResponse
from post.models import Post
from blog.models import District
from django.utils.translation import ugettext, ungettext, gettext as _
from cart.classes.cart import Cart, session_name
from .classes.order import Order, payment_type_bank
from django.contrib import messages
from django.shortcuts import render, redirect
from .forms import OrderBillingForm
from django.contrib.auth.decorators import login_required


def cart(request):
    basket = request.session.get(session_name)
    obj = Cart.getSessionObjectForView(basket)
    obj['title'] = _('Cart')
    obj['breadcrumb'] = _('Cart')
    return render(request, 'cart/template/cart.html', obj)


def updateCart(request):
    if request.method == 'POST':
        data = request.POST.get('data')
        if data:
            obj = Cart.updateCart(request, data)
            return JsonResponse({'success': True, 'data': obj, 'message': _('Updated successfully')})
    return JsonResponse({'success': False})


@login_required
def checkout(request):
    basket = request.session.get(session_name)
    obj = Cart.getSessionObjectForView(basket)
    districts = District.objects.order_by('-order').all()
    form = OrderBillingForm()
    if request.method == "POST":

        if basket == None:
            messages.success(request, _("Your basket is empty, you don't have any product yet"))
            return redirect('checkout')

        form = OrderBillingForm(request.POST)
        if form.is_valid():
            orderModel = Order.addOrder(request, form)
            if orderModel:
                if request.POST['payment_type'] == payment_type_bank:
                    return redirect('bank')
                # for cash payment we will get message just contact you soon
                messages.success(request, _('Your order was completed,we will contact you soon'))
                return redirect('home')
            messages.success(request, _('Something was wrong'))
    obj['districts'] = districts
    obj['form'] = form
    obj['title'] = _('Checkout')
    obj['breadcrumb'] = _('Checkout')
    return render(request, 'cart/template/checkout.html', obj)


def bank(request):
    return render(request, 'cart/template/bank.html')


def addToCart(request):
    if request.method == 'POST':
        quantity = request.POST.get('quantity')
        productId = request.POST.get('productId')
        post = Post.objects.filter(id=productId).first()
        if post:
            cart = Cart(quantity, post, request)
            cart.addCart()
            html = cart.refreshSmallCart
            return JsonResponse({'success': True, 'data': html, 'message': _('Added to the basket successfully')})

    return JsonResponse({'success': False})


def remove(request):
    if request.method == 'POST':
        productId = request.POST.get('productId')
        post = Post.objects.filter(id=productId).first()
        if post:
            cart = Cart(None, post, request)
            cart.remove()
            html = cart.refreshSmallCart

            return JsonResponse({'success': True, 'data': html, 'message': _('Removed successfully')})

    return JsonResponse({'success': False})
