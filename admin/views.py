from django.shortcuts import render,get_object_or_404,redirect
from blog.models import Images
from django.http import HttpResponseRedirect,JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
import json
from django.views.decorators.csrf import csrf_exempt

@login_required
def removeImage(request,id):
	if request.method  == 'GET' and User.is_superuser and id:
	   image = get_object_or_404(Images, id=id).delete()
	   return JsonResponse({'sucess': True})
	return JsonResponse({'sucess': False})
	#return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

@login_required
def imageSort(request):
	stacks = request.POST.get('stack')
	if request.method == 'POST' and stacks:
	   jsonData = json.loads(stacks) 
	   for key,value in enumerate(jsonData): 
		   image = Images.objects.filter(id=int(value['key'])).update(rank=int(key+1))

	return JsonResponse({'sucess': True})

@login_required
@csrf_exempt
def imageDelete(request):
	id = request.POST.get('key')
	if request.method == 'POST' and id:
	   Images.objects.filter(id=int(id)).delete()
	return JsonResponse({'sucess': True})

