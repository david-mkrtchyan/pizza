from django.urls import path
from . import views

urlpatterns = [
    path('remove-image/<int:id>', views.removeImage, name='remove-image'),
    path('sort-images', views.imageSort, name='sort-images'),
    path('image-delete', views.imageDelete, name='image-delete'),
]
